<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the first needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientBase class (each generated ServiceType class extends this class)
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = [
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://goodlife-test.exerp.com/api/v5/BookingAPI?wsdl',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * ];
 * etc...
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = [
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://goodlife-test.exerp.com/api/v5/BookingAPI?wsdl',
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \Booking\ClassMap::get(),
];
/**
 * Samples for Get ServiceType
 */
$get = new \Booking\ServiceType\Get($options);
/**
 * Sample call for getResources operation/method
 */
if ($get->getResources($center, $activityKey) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getParticipationQRCode operation/method
 */
if ($get->getParticipationQRCode(new \Booking\StructType\CompositeKey()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getMemberSanctionStatus operation/method
 */
if ($get->getMemberSanctionStatus(new \Booking\StructType\GetMemberSanctionStatusParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getClass operation/method
 */
if ($get->getClass(new \Booking\StructType\CompositeKey()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getAvailableSeats operation/method
 */
if ($get->getAvailableSeats(new \Booking\StructType\AvailableSeatsParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getBookingDetails operation/method
 */
if ($get->getBookingDetails(new \Booking\StructType\GetBookingDetailsParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getBookingRestrictions operation/method
 */
if ($get->getBookingRestrictions(new \Booking\StructType\GetBookingRestrictionsParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getAvailableAgeGroups operation/method
 */
if ($get->getAvailableAgeGroups(new \Booking\StructType\GetAvailableAgeGroupsParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getAllAvailableActivities operation/method
 */
if ($get->getAllAvailableActivities(new \Booking\ArrayType\IntArray()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getClassParticipations operation/method
 */
if ($get->getClassParticipations(new \Booking\StructType\CompositeKey()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getActivityGroups operation/method
 */
if ($get->getActivityGroups(new \Booking\StructType\GetActivityGroupsParameter()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getProductsForBooking operation/method
 */
if ($get->getProductsForBooking(new \Booking\StructType\GetProductsForBookingParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getPersonParticipations operation/method
 */
if ($get->getPersonParticipations(new \Booking\StructType\ApiPersonKey(), $fromDate, $toDate, $personalBookingType) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getWeekSchedulePDF operation/method
 */
if ($get->getWeekSchedulePDF(new \Booking\StructType\WeekScheduleParameter()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getAvailableActivities operation/method
 */
if ($get->getAvailableActivities(new \Booking\ArrayType\IntArray()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Samples for Change ServiceType
 */
$change = new \Booking\ServiceType\Change($options);
/**
 * Sample call for changeClassActivity operation/method
 */
if ($change->changeClassActivity(new \Booking\StructType\BookingChangeActivityParameter()) !== false) {
    print_r($change->getResult());
} else {
    print_r($change->getLastError());
}
/**
 * Sample call for changeClassActivitySimple operation/method
 */
if ($change->changeClassActivitySimple(new \Booking\StructType\BookingChangeActivitySimpleParameters()) !== false) {
    print_r($change->getResult());
} else {
    print_r($change->getLastError());
}
/**
 * Samples for Find ServiceType
 */
$find = new \Booking\ServiceType\Find($options);
/**
 * Sample call for findClasses operation/method
 */
if ($find->findClasses(new \Booking\StructType\FindClassesParameters()) !== false) {
    print_r($find->getResult());
} else {
    print_r($find->getLastError());
}
/**
 * Sample call for findPersonParticipations operation/method
 */
if ($find->findPersonParticipations(new \Booking\StructType\ApiPersonKey()) !== false) {
    print_r($find->getResult());
} else {
    print_r($find->getLastError());
}
/**
 * Samples for Create ServiceType
 */
$create = new \Booking\ServiceType\Create($options);
/**
 * Sample call for createParticipationAndSendMessage operation/method
 */
if ($create->createParticipationAndSendMessage(new \Booking\StructType\ApiPersonKey(), new \Booking\StructType\CompositeKey(), $userInterfaceType) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for createClass operation/method
 */
if ($create->createClass(new \Booking\StructType\CreateClassBookingParameter()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for createOrUpdateParticipationTentative operation/method
 */
if ($create->createOrUpdateParticipationTentative(new \Booking\StructType\CreateOrUpdateParticipationTentativeParams()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for createOrUpdateParticipation operation/method
 */
if ($create->createOrUpdateParticipation(new \Booking\StructType\CreateOrUpdateParticipationParams()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Samples for Check ServiceType
 */
$check = new \Booking\ServiceType\Check($options);
/**
 * Sample call for checkPrivilegeForBooking operation/method
 */
if ($check->checkPrivilegeForBooking(new \Booking\StructType\CheckPrivilegeForBookingParameters()) !== false) {
    print_r($check->getResult());
} else {
    print_r($check->getLastError());
}
/**
 * Sample call for checkPrivilegeForActivity operation/method
 */
if ($check->checkPrivilegeForActivity(new \Booking\StructType\CheckPrivilegeForActivityParameters()) !== false) {
    print_r($check->getResult());
} else {
    print_r($check->getLastError());
}
/**
 * Samples for Cancel ServiceType
 */
$cancel = new \Booking\ServiceType\Cancel($options);
/**
 * Sample call for cancelBooking operation/method
 */
if ($cancel->cancelBooking(new \Booking\StructType\CompositeKey(), $notifyParticipants, $notifyStaff, $message) !== false) {
    print_r($cancel->getResult());
} else {
    print_r($cancel->getLastError());
}
/**
 * Sample call for cancelPersonParticipation operation/method
 */
if ($cancel->cancelPersonParticipation(new \Booking\StructType\CompositeKey(), new \Booking\StructType\ApiPersonKey(), $userInterfaceType, $sendCancelMessage) !== false) {
    print_r($cancel->getResult());
} else {
    print_r($cancel->getLastError());
}
/**
 * Samples for Showup ServiceType
 */
$showup = new \Booking\ServiceType\Showup($options);
/**
 * Sample call for showup operation/method
 */
if ($showup->showup(new \Booking\StructType\CompositeKey(), $userInterfaceType) !== false) {
    print_r($showup->getResult());
} else {
    print_r($showup->getLastError());
}
/**
 * Samples for Remove ServiceType
 */
$remove = new \Booking\ServiceType\Remove($options);
/**
 * Sample call for removeBookingRestriction operation/method
 */
if ($remove->removeBookingRestriction(new \Booking\StructType\RemoveBookingRestrictionParameters()) !== false) {
    print_r($remove->getResult());
} else {
    print_r($remove->getLastError());
}
/**
 * Samples for Update ServiceType
 */
$update = new \Booking\ServiceType\Update($options);
/**
 * Sample call for updateClass operation/method
 */
if ($update->updateClass(new \Booking\StructType\UpdateClassBookingParameter()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
