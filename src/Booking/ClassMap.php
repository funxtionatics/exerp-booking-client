<?php

declare(strict_types=1);

namespace Booking;

/**
 * Class which returns the class map definition
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get(): array
    {
        return [
            'resource' => '\\Booking\\StructType\\_resource',
            'seats' => '\\Booking\\StructType\\Seats',
            'compositeKey' => '\\Booking\\StructType\\CompositeKey',
            'seat' => '\\Booking\\StructType\\Seat',
            'errorDetail' => '\\Booking\\StructType\\ErrorDetail',
            'bookingChangeActivityParameter' => '\\Booking\\StructType\\BookingChangeActivityParameter',
            'apiPersonKey' => '\\Booking\\StructType\\ApiPersonKey',
            'bookingChangeActivityResult' => '\\Booking\\StructType\\BookingChangeActivityResult',
            'booking' => '\\Booking\\StructType\\Booking',
            'instructors' => '\\Booking\\StructType\\Instructors',
            'activity' => '\\Booking\\StructType\\Activity',
            'colorGroup' => '\\Booking\\StructType\\ColorGroup',
            'activityGroup' => '\\Booking\\StructType\\ActivityGroup',
            'ageRestriction' => '\\Booking\\StructType\\AgeRestriction',
            'personSimple' => '\\Booking\\StructType\\PersonSimple',
            'personName' => '\\Booking\\StructType\\PersonName',
            'BookingChangeActivityParticipation' => '\\Booking\\StructType\\BookingChangeActivityParticipation',
            'bookingChangeActivitySimpleParameters' => '\\Booking\\StructType\\BookingChangeActivitySimpleParameters',
            'bookingChangeActivitySimpleResult' => '\\Booking\\StructType\\BookingChangeActivitySimpleResult',
            'mimeDocument' => '\\Booking\\StructType\\MimeDocument',
            'findClassesParameters' => '\\Booking\\StructType\\FindClassesParameters',
            'interval' => '\\Booking\\StructType\\Interval',
            'participation' => '\\Booking\\StructType\\Participation',
            'getMemberSanctionStatusParameters' => '\\Booking\\StructType\\GetMemberSanctionStatusParameters',
            'getMemberSanctionStatusResult' => '\\Booking\\StructType\\GetMemberSanctionStatusResult',
            'sanctions' => '\\Booking\\StructType\\Sanctions',
            'memberSanctionStatus' => '\\Booking\\StructType\\MemberSanctionStatus',
            'privilegeSanction' => '\\Booking\\StructType\\PrivilegeSanction',
            'privilegeSanctionRestriction' => '\\Booking\\StructType\\PrivilegeSanctionRestriction',
            'createClassBookingParameter' => '\\Booking\\StructType\\CreateClassBookingParameter',
            'checkPrivilegeForBookingParameters' => '\\Booking\\StructType\\CheckPrivilegeForBookingParameters',
            'apiProductKey' => '\\Booking\\StructType\\ApiProductKey',
            'checkPrivilegeForBookingResult' => '\\Booking\\StructType\\CheckPrivilegeForBookingResult',
            'availableSeatsParameters' => '\\Booking\\StructType\\AvailableSeatsParameters',
            'availableSeatsResult' => '\\Booking\\StructType\\AvailableSeatsResult',
            'getBookingDetailsParameters' => '\\Booking\\StructType\\GetBookingDetailsParameters',
            'bookingDetails' => '\\Booking\\StructType\\BookingDetails',
            'customAttribute' => '\\Booking\\StructType\\CustomAttribute',
            'customAttributeValue' => '\\Booking\\StructType\\CustomAttributeValue',
            'createOrUpdateParticipationTentativeParams' => '\\Booking\\StructType\\CreateOrUpdateParticipationTentativeParams',
            'participants' => '\\Booking\\StructType\\Participants',
            'participant' => '\\Booking\\StructType\\Participant',
            'createOrUpdateParticipationTentativeResult' => '\\Booking\\StructType\\CreateOrUpdateParticipationTentativeResult',
            'participations' => '\\Booking\\StructType\\Participations',
            'getBookingRestrictionsParameters' => '\\Booking\\StructType\\GetBookingRestrictionsParameters',
            'periods' => '\\Booking\\StructType\\Periods',
            'getBookingRestrictionsResult' => '\\Booking\\StructType\\GetBookingRestrictionsResult',
            'bookingRestrictions' => '\\Booking\\StructType\\BookingRestrictions',
            'bookingRestriction' => '\\Booking\\StructType\\BookingRestriction',
            'compositeSubKey' => '\\Booking\\StructType\\CompositeSubKey',
            'getAvailableAgeGroupsParameters' => '\\Booking\\StructType\\GetAvailableAgeGroupsParameters',
            'getAvailableAgeGroupsResult' => '\\Booking\\StructType\\GetAvailableAgeGroupsResult',
            'ageGroups' => '\\Booking\\StructType\\AgeGroups',
            'availableAgeGroup' => '\\Booking\\StructType\\AvailableAgeGroup',
            'ageGroupLimit' => '\\Booking\\StructType\\AgeGroupLimit',
            'availableActivity' => '\\Booking\\StructType\\AvailableActivity',
            'ageGroup' => '\\Booking\\StructType\\AgeGroup',
            'courseConfiguration' => '\\Booking\\StructType\\CourseConfiguration',
            'courseLevel' => '\\Booking\\StructType\\CourseLevel',
            'courseType' => '\\Booking\\StructType\\CourseType',
            'removeBookingRestrictionParameters' => '\\Booking\\StructType\\RemoveBookingRestrictionParameters',
            'removeBookingRestrictionResponse' => '\\Booking\\StructType\\RemoveBookingRestrictionResponse',
            'getActivityGroupsParameter' => '\\Booking\\StructType\\GetActivityGroupsParameter',
            'createOrUpdateParticipationParams' => '\\Booking\\StructType\\CreateOrUpdateParticipationParams',
            'paymentInfo' => '\\Booking\\StructType\\PaymentInfo',
            'creditCardPaymentInfo' => '\\Booking\\StructType\\CreditCardPaymentInfo',
            'createOrUpdateParticipationResult' => '\\Booking\\StructType\\CreateOrUpdateParticipationResult',
            'checkPrivilegeForActivityParameters' => '\\Booking\\StructType\\CheckPrivilegeForActivityParameters',
            'checkPrivilegeForActivityResult' => '\\Booking\\StructType\\CheckPrivilegeForActivityResult',
            'getProductsForBookingParameters' => '\\Booking\\StructType\\GetProductsForBookingParameters',
            'getProductsForBookingResult' => '\\Booking\\StructType\\GetProductsForBookingResult',
            'products' => '\\Booking\\StructType\\Products',
            'bookingProduct' => '\\Booking\\StructType\\BookingProduct',
            'productPrice' => '\\Booking\\StructType\\ProductPrice',
            'updateClassBookingParameter' => '\\Booking\\StructType\\UpdateClassBookingParameter',
            'weekScheduleParameter' => '\\Booking\\StructType\\WeekScheduleParameter',
            'resourceArray' => '\\Booking\\ArrayType\\ResourceArray',
            'bookingArray' => '\\Booking\\ArrayType\\BookingArray',
            'participationArray' => '\\Booking\\ArrayType\\ParticipationArray',
            'availableActivityArray' => '\\Booking\\ArrayType\\AvailableActivityArray',
            'activityGroupArray' => '\\Booking\\ArrayType\\ActivityGroupArray',
            'APIException' => '\\Booking\\StructType\\APIException',
            'intArray' => '\\Booking\\ArrayType\\IntArray',
        ];
    }
}
