<?php

declare(strict_types=1);

namespace Booking\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for productType EnumType
 * @subpackage Enumerations
 */
class ProductType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'GOOD'
     * @return string 'GOOD'
     */
    const VALUE_GOOD = 'GOOD';
    /**
     * Constant for value 'SERVICE'
     * @return string 'SERVICE'
     */
    const VALUE_SERVICE = 'SERVICE';
    /**
     * Constant for value 'CLIPCARD'
     * @return string 'CLIPCARD'
     */
    const VALUE_CLIPCARD = 'CLIPCARD';
    /**
     * Constant for value 'SUBSCRIPTION'
     * @return string 'SUBSCRIPTION'
     */
    const VALUE_SUBSCRIPTION = 'SUBSCRIPTION';
    /**
     * Return allowed values
     * @uses self::VALUE_GOOD
     * @uses self::VALUE_SERVICE
     * @uses self::VALUE_CLIPCARD
     * @uses self::VALUE_SUBSCRIPTION
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_GOOD,
            self::VALUE_SERVICE,
            self::VALUE_CLIPCARD,
            self::VALUE_SUBSCRIPTION,
        ];
    }
}
