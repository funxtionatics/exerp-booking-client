<?php

declare(strict_types=1);

namespace Booking\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for activityType EnumType
 * @subpackage Enumerations
 */
class ActivityType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'GENERAL'
     * @return string 'GENERAL'
     */
    const VALUE_GENERAL = 'GENERAL';
    /**
     * Constant for value 'CLASS'
     * @return string 'CLASS'
     */
    const VALUE_CLASS = 'CLASS';
    /**
     * Constant for value 'RESOURCE_AVAILABILITY'
     * @return string 'RESOURCE_AVAILABILITY'
     */
    const VALUE_RESOURCE_AVAILABILITY = 'RESOURCE_AVAILABILITY';
    /**
     * Constant for value 'RESOURCE_BOOKING'
     * @return string 'RESOURCE_BOOKING'
     */
    const VALUE_RESOURCE_BOOKING = 'RESOURCE_BOOKING';
    /**
     * Constant for value 'STAFF_AVAILABILITY'
     * @return string 'STAFF_AVAILABILITY'
     */
    const VALUE_STAFF_AVAILABILITY = 'STAFF_AVAILABILITY';
    /**
     * Constant for value 'STAFF_BOOKING'
     * @return string 'STAFF_BOOKING'
     */
    const VALUE_STAFF_BOOKING = 'STAFF_BOOKING';
    /**
     * Constant for value 'CHILD_CARE'
     * @return string 'CHILD_CARE'
     */
    const VALUE_CHILD_CARE = 'CHILD_CARE';
    /**
     * Constant for value 'MEETING'
     * @return string 'MEETING'
     */
    const VALUE_MEETING = 'MEETING';
    /**
     * Constant for value 'COURSE'
     * @return string 'COURSE'
     */
    const VALUE_COURSE = 'COURSE';
    /**
     * Constant for value 'TASK'
     * @return string 'TASK'
     */
    const VALUE_TASK = 'TASK';
    /**
     * Constant for value 'CAMP'
     * @return string 'CAMP'
     */
    const VALUE_CAMP = 'CAMP';
    /**
     * Constant for value 'CAMP_ELECTIVE'
     * @return string 'CAMP_ELECTIVE'
     */
    const VALUE_CAMP_ELECTIVE = 'CAMP_ELECTIVE';
    /**
     * Constant for value 'RESOURCE_MAINTENANCE'
     * @return string 'RESOURCE_MAINTENANCE'
     */
    const VALUE_RESOURCE_MAINTENANCE = 'RESOURCE_MAINTENANCE';
    /**
     * Return allowed values
     * @uses self::VALUE_GENERAL
     * @uses self::VALUE_CLASS
     * @uses self::VALUE_RESOURCE_AVAILABILITY
     * @uses self::VALUE_RESOURCE_BOOKING
     * @uses self::VALUE_STAFF_AVAILABILITY
     * @uses self::VALUE_STAFF_BOOKING
     * @uses self::VALUE_CHILD_CARE
     * @uses self::VALUE_MEETING
     * @uses self::VALUE_COURSE
     * @uses self::VALUE_TASK
     * @uses self::VALUE_CAMP
     * @uses self::VALUE_CAMP_ELECTIVE
     * @uses self::VALUE_RESOURCE_MAINTENANCE
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_GENERAL,
            self::VALUE_CLASS,
            self::VALUE_RESOURCE_AVAILABILITY,
            self::VALUE_RESOURCE_BOOKING,
            self::VALUE_STAFF_AVAILABILITY,
            self::VALUE_STAFF_BOOKING,
            self::VALUE_CHILD_CARE,
            self::VALUE_MEETING,
            self::VALUE_COURSE,
            self::VALUE_TASK,
            self::VALUE_CAMP,
            self::VALUE_CAMP_ELECTIVE,
            self::VALUE_RESOURCE_MAINTENANCE,
        ];
    }
}
