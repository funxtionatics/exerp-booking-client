<?php

declare(strict_types=1);

namespace Booking\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for weekDay EnumType
 * @subpackage Enumerations
 */
class WeekDay extends AbstractStructEnumBase
{
    /**
     * Constant for value 'MONDAY'
     * @return string 'MONDAY'
     */
    const VALUE_MONDAY = 'MONDAY';
    /**
     * Constant for value 'TUESDAY'
     * @return string 'TUESDAY'
     */
    const VALUE_TUESDAY = 'TUESDAY';
    /**
     * Constant for value 'WEDNESDAY'
     * @return string 'WEDNESDAY'
     */
    const VALUE_WEDNESDAY = 'WEDNESDAY';
    /**
     * Constant for value 'THURSDAY'
     * @return string 'THURSDAY'
     */
    const VALUE_THURSDAY = 'THURSDAY';
    /**
     * Constant for value 'FRIDAY'
     * @return string 'FRIDAY'
     */
    const VALUE_FRIDAY = 'FRIDAY';
    /**
     * Constant for value 'SATURDAY'
     * @return string 'SATURDAY'
     */
    const VALUE_SATURDAY = 'SATURDAY';
    /**
     * Constant for value 'SUNDAY'
     * @return string 'SUNDAY'
     */
    const VALUE_SUNDAY = 'SUNDAY';
    /**
     * Return allowed values
     * @uses self::VALUE_MONDAY
     * @uses self::VALUE_TUESDAY
     * @uses self::VALUE_WEDNESDAY
     * @uses self::VALUE_THURSDAY
     * @uses self::VALUE_FRIDAY
     * @uses self::VALUE_SATURDAY
     * @uses self::VALUE_SUNDAY
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MONDAY,
            self::VALUE_TUESDAY,
            self::VALUE_WEDNESDAY,
            self::VALUE_THURSDAY,
            self::VALUE_FRIDAY,
            self::VALUE_SATURDAY,
            self::VALUE_SUNDAY,
        ];
    }
}
