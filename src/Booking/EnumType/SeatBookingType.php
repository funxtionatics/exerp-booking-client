<?php

declare(strict_types=1);

namespace Booking\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for seatBookingType EnumType
 * @subpackage Enumerations
 */
class SeatBookingType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'NOT_AVAILABLE'
     * @return string 'NOT_AVAILABLE'
     */
    const VALUE_NOT_AVAILABLE = 'NOT_AVAILABLE';
    /**
     * Constant for value 'MANDATORY'
     * @return string 'MANDATORY'
     */
    const VALUE_MANDATORY = 'MANDATORY';
    /**
     * Constant for value 'OPTIONAL'
     * @return string 'OPTIONAL'
     */
    const VALUE_OPTIONAL = 'OPTIONAL';
    /**
     * Return allowed values
     * @uses self::VALUE_NOT_AVAILABLE
     * @uses self::VALUE_MANDATORY
     * @uses self::VALUE_OPTIONAL
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NOT_AVAILABLE,
            self::VALUE_MANDATORY,
            self::VALUE_OPTIONAL,
        ];
    }
}
