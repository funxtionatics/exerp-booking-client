<?php

declare(strict_types=1);

namespace Booking\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for paymentMethod EnumType
 * @subpackage Enumerations
 */
class PaymentMethod extends AbstractStructEnumBase
{
    /**
     * Constant for value 'CASH_ACCOUNT'
     * @return string 'CASH_ACCOUNT'
     */
    const VALUE_CASH_ACCOUNT = 'CASH_ACCOUNT';
    /**
     * Constant for value 'PAYMENT_ACCOUNT'
     * @return string 'PAYMENT_ACCOUNT'
     */
    const VALUE_PAYMENT_ACCOUNT = 'PAYMENT_ACCOUNT';
    /**
     * Constant for value 'SHOPPING_BASKET'
     * @return string 'SHOPPING_BASKET'
     */
    const VALUE_SHOPPING_BASKET = 'SHOPPING_BASKET';
    /**
     * Constant for value 'INSTALLMENT_PLAN'
     * @return string 'INSTALLMENT_PLAN'
     */
    const VALUE_INSTALLMENT_PLAN = 'INSTALLMENT_PLAN';
    /**
     * Return allowed values
     * @uses self::VALUE_CASH_ACCOUNT
     * @uses self::VALUE_PAYMENT_ACCOUNT
     * @uses self::VALUE_SHOPPING_BASKET
     * @uses self::VALUE_INSTALLMENT_PLAN
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CASH_ACCOUNT,
            self::VALUE_PAYMENT_ACCOUNT,
            self::VALUE_SHOPPING_BASKET,
            self::VALUE_INSTALLMENT_PLAN,
        ];
    }
}
