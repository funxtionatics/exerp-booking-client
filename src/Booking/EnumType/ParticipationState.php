<?php

declare(strict_types=1);

namespace Booking\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for participationState EnumType
 * @subpackage Enumerations
 */
class ParticipationState extends AbstractStructEnumBase
{
    /**
     * Constant for value 'BOOKED'
     * @return string 'BOOKED'
     */
    const VALUE_BOOKED = 'BOOKED';
    /**
     * Constant for value 'OVERBOOKED_WAITINGLIST'
     * @return string 'OVERBOOKED_WAITINGLIST'
     */
    const VALUE_OVERBOOKED_WAITINGLIST = 'OVERBOOKED_WAITINGLIST';
    /**
     * Constant for value 'CANCELLED_BY_CENTER'
     * @return string 'CANCELLED_BY_CENTER'
     */
    const VALUE_CANCELLED_BY_CENTER = 'CANCELLED_BY_CENTER';
    /**
     * Constant for value 'CANCELLED_BY_USER'
     * @return string 'CANCELLED_BY_USER'
     */
    const VALUE_CANCELLED_BY_USER = 'CANCELLED_BY_USER';
    /**
     * Constant for value 'CANCELLED_BY_USER_TOO_LATE'
     * @return string 'CANCELLED_BY_USER_TOO_LATE'
     */
    const VALUE_CANCELLED_BY_USER_TOO_LATE = 'CANCELLED_BY_USER_TOO_LATE';
    /**
     * Constant for value 'OVERBOOKED_SHOWED_UP'
     * @return string 'OVERBOOKED_SHOWED_UP'
     */
    const VALUE_OVERBOOKED_SHOWED_UP = 'OVERBOOKED_SHOWED_UP';
    /**
     * Constant for value 'BOOKED_BUT_LATE'
     * @return string 'BOOKED_BUT_LATE'
     */
    const VALUE_BOOKED_BUT_LATE = 'BOOKED_BUT_LATE';
    /**
     * Constant for value 'PARTICIPATION'
     * @return string 'PARTICIPATION'
     */
    const VALUE_PARTICIPATION = 'PARTICIPATION';
    /**
     * Constant for value 'BOOKED_NO_SHOW_UP'
     * @return string 'BOOKED_NO_SHOW_UP'
     */
    const VALUE_BOOKED_NO_SHOW_UP = 'BOOKED_NO_SHOW_UP';
    /**
     * Constant for value 'SHOWED_UP_NO_PARTICIPATION'
     * @return string 'SHOWED_UP_NO_PARTICIPATION'
     */
    const VALUE_SHOWED_UP_NO_PARTICIPATION = 'SHOWED_UP_NO_PARTICIPATION';
    /**
     * Constant for value 'ATTEND'
     * @return string 'ATTEND'
     */
    const VALUE_ATTEND = 'ATTEND';
    /**
     * Constant for value 'ATTEND_CANCELLED'
     * @return string 'ATTEND_CANCELLED'
     */
    const VALUE_ATTEND_CANCELLED = 'ATTEND_CANCELLED';
    /**
     * Constant for value 'ATTEND_IMPLICIT'
     * @return string 'ATTEND_IMPLICIT'
     */
    const VALUE_ATTEND_IMPLICIT = 'ATTEND_IMPLICIT';
    /**
     * Constant for value 'INSTRUCTOR_ACTIVE'
     * @return string 'INSTRUCTOR_ACTIVE'
     */
    const VALUE_INSTRUCTOR_ACTIVE = 'INSTRUCTOR_ACTIVE';
    /**
     * Constant for value 'INSTRUCTOR_CANCELLED'
     * @return string 'INSTRUCTOR_CANCELLED'
     */
    const VALUE_INSTRUCTOR_CANCELLED = 'INSTRUCTOR_CANCELLED';
    /**
     * Constant for value 'BOOKED_IN_WAITING_LIST'
     * @return string 'BOOKED_IN_WAITING_LIST'
     */
    const VALUE_BOOKED_IN_WAITING_LIST = 'BOOKED_IN_WAITING_LIST';
    /**
     * Constant for value 'TENTATIVE'
     * @return string 'TENTATIVE'
     */
    const VALUE_TENTATIVE = 'TENTATIVE';
    /**
     * Constant for value 'CANCELLED_BY_BOOKING'
     * @return string 'CANCELLED_BY_BOOKING'
     */
    const VALUE_CANCELLED_BY_BOOKING = 'CANCELLED_BY_BOOKING';
    /**
     * Constant for value 'CANCELLED_NO_PRIVILEGE'
     * @return string 'CANCELLED_NO_PRIVILEGE'
     */
    const VALUE_CANCELLED_NO_PRIVILEGE = 'CANCELLED_NO_PRIVILEGE';
    /**
     * Return allowed values
     * @uses self::VALUE_BOOKED
     * @uses self::VALUE_OVERBOOKED_WAITINGLIST
     * @uses self::VALUE_CANCELLED_BY_CENTER
     * @uses self::VALUE_CANCELLED_BY_USER
     * @uses self::VALUE_CANCELLED_BY_USER_TOO_LATE
     * @uses self::VALUE_OVERBOOKED_SHOWED_UP
     * @uses self::VALUE_BOOKED_BUT_LATE
     * @uses self::VALUE_PARTICIPATION
     * @uses self::VALUE_BOOKED_NO_SHOW_UP
     * @uses self::VALUE_SHOWED_UP_NO_PARTICIPATION
     * @uses self::VALUE_ATTEND
     * @uses self::VALUE_ATTEND_CANCELLED
     * @uses self::VALUE_ATTEND_IMPLICIT
     * @uses self::VALUE_INSTRUCTOR_ACTIVE
     * @uses self::VALUE_INSTRUCTOR_CANCELLED
     * @uses self::VALUE_BOOKED_IN_WAITING_LIST
     * @uses self::VALUE_TENTATIVE
     * @uses self::VALUE_CANCELLED_BY_BOOKING
     * @uses self::VALUE_CANCELLED_NO_PRIVILEGE
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_BOOKED,
            self::VALUE_OVERBOOKED_WAITINGLIST,
            self::VALUE_CANCELLED_BY_CENTER,
            self::VALUE_CANCELLED_BY_USER,
            self::VALUE_CANCELLED_BY_USER_TOO_LATE,
            self::VALUE_OVERBOOKED_SHOWED_UP,
            self::VALUE_BOOKED_BUT_LATE,
            self::VALUE_PARTICIPATION,
            self::VALUE_BOOKED_NO_SHOW_UP,
            self::VALUE_SHOWED_UP_NO_PARTICIPATION,
            self::VALUE_ATTEND,
            self::VALUE_ATTEND_CANCELLED,
            self::VALUE_ATTEND_IMPLICIT,
            self::VALUE_INSTRUCTOR_ACTIVE,
            self::VALUE_INSTRUCTOR_CANCELLED,
            self::VALUE_BOOKED_IN_WAITING_LIST,
            self::VALUE_TENTATIVE,
            self::VALUE_CANCELLED_BY_BOOKING,
            self::VALUE_CANCELLED_NO_PRIVILEGE,
        ];
    }
}
