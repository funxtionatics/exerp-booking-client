<?php

declare(strict_types=1);

namespace Booking\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for activityColor EnumType
 * @subpackage Enumerations
 */
class ActivityColor extends AbstractStructEnumBase
{
    /**
     * Constant for value 'WHITE'
     * @return string 'WHITE'
     */
    const VALUE_WHITE = 'WHITE';
    /**
     * Constant for value 'YELLOW'
     * @return string 'YELLOW'
     */
    const VALUE_YELLOW = 'YELLOW';
    /**
     * Constant for value 'GREEN'
     * @return string 'GREEN'
     */
    const VALUE_GREEN = 'GREEN';
    /**
     * Constant for value 'BLUE'
     * @return string 'BLUE'
     */
    const VALUE_BLUE = 'BLUE';
    /**
     * Constant for value 'ORANGE'
     * @return string 'ORANGE'
     */
    const VALUE_ORANGE = 'ORANGE';
    /**
     * Constant for value 'RED'
     * @return string 'RED'
     */
    const VALUE_RED = 'RED';
    /**
     * Return allowed values
     * @uses self::VALUE_WHITE
     * @uses self::VALUE_YELLOW
     * @uses self::VALUE_GREEN
     * @uses self::VALUE_BLUE
     * @uses self::VALUE_ORANGE
     * @uses self::VALUE_RED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_WHITE,
            self::VALUE_YELLOW,
            self::VALUE_GREEN,
            self::VALUE_BLUE,
            self::VALUE_ORANGE,
            self::VALUE_RED,
        ];
    }
}
