<?php

declare(strict_types=1);

namespace Booking\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for ageGroupTimeUnit EnumType
 * @subpackage Enumerations
 */
class AgeGroupTimeUnit extends AbstractStructEnumBase
{
    /**
     * Constant for value 'MONTH'
     * @return string 'MONTH'
     */
    const VALUE_MONTH = 'MONTH';
    /**
     * Constant for value 'YEAR'
     * @return string 'YEAR'
     */
    const VALUE_YEAR = 'YEAR';
    /**
     * Return allowed values
     * @uses self::VALUE_MONTH
     * @uses self::VALUE_YEAR
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MONTH,
            self::VALUE_YEAR,
        ];
    }
}
