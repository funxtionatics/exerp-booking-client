<?php

declare(strict_types=1);

namespace Booking\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for courseScheduleType EnumType
 * @subpackage Enumerations
 */
class CourseScheduleType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'FIXED'
     * @return string 'FIXED'
     */
    const VALUE_FIXED = 'FIXED';
    /**
     * Constant for value 'CONTINUOUS'
     * @return string 'CONTINUOUS'
     */
    const VALUE_CONTINUOUS = 'CONTINUOUS';
    /**
     * Return allowed values
     * @uses self::VALUE_FIXED
     * @uses self::VALUE_CONTINUOUS
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_FIXED,
            self::VALUE_CONTINUOUS,
        ];
    }
}
