<?php

declare(strict_types=1);

namespace Booking\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for personalBookingType EnumType
 * @subpackage Enumerations
 */
class PersonalBookingType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'OLD_BOOKINGS'
     * @return string 'OLD_BOOKINGS'
     */
    const VALUE_OLD_BOOKINGS = 'OLD_BOOKINGS';
    /**
     * Constant for value 'NON_SHOW_UPS'
     * @return string 'NON_SHOW_UPS'
     */
    const VALUE_NON_SHOW_UPS = 'NON_SHOW_UPS';
    /**
     * Constant for value 'SHOW_UPS'
     * @return string 'SHOW_UPS'
     */
    const VALUE_SHOW_UPS = 'SHOW_UPS';
    /**
     * Return allowed values
     * @uses self::VALUE_OLD_BOOKINGS
     * @uses self::VALUE_NON_SHOW_UPS
     * @uses self::VALUE_SHOW_UPS
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_OLD_BOOKINGS,
            self::VALUE_NON_SHOW_UPS,
            self::VALUE_SHOW_UPS,
        ];
    }
}
