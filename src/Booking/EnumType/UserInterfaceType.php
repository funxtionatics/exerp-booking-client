<?php

declare(strict_types=1);

namespace Booking\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for userInterfaceType EnumType
 * @subpackage Enumerations
 */
class UserInterfaceType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'CLIENT'
     * @return string 'CLIENT'
     */
    const VALUE_CLIENT = 'CLIENT';
    /**
     * Constant for value 'WEB'
     * @return string 'WEB'
     */
    const VALUE_WEB = 'WEB';
    /**
     * Constant for value 'KIOSK'
     * @return string 'KIOSK'
     */
    const VALUE_KIOSK = 'KIOSK';
    /**
     * Constant for value 'MOBILE'
     * @return string 'MOBILE'
     */
    const VALUE_MOBILE = 'MOBILE';
    /**
     * Constant for value 'MOBILE_STAFF'
     * @return string 'MOBILE_STAFF'
     */
    const VALUE_MOBILE_STAFF = 'MOBILE_STAFF';
    /**
     * Constant for value 'OTHER'
     * @return string 'OTHER'
     */
    const VALUE_OTHER = 'OTHER';
    /**
     * Return allowed values
     * @uses self::VALUE_CLIENT
     * @uses self::VALUE_WEB
     * @uses self::VALUE_KIOSK
     * @uses self::VALUE_MOBILE
     * @uses self::VALUE_MOBILE_STAFF
     * @uses self::VALUE_OTHER
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CLIENT,
            self::VALUE_WEB,
            self::VALUE_KIOSK,
            self::VALUE_MOBILE,
            self::VALUE_MOBILE_STAFF,
            self::VALUE_OTHER,
        ];
    }
}
