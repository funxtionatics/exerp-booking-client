<?php

declare(strict_types=1);

namespace Booking\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for bookingRestrictionPeriod EnumType
 * @subpackage Enumerations
 */
class BookingRestrictionPeriod extends AbstractStructEnumBase
{
    /**
     * Constant for value 'CURRENT'
     * @return string 'CURRENT'
     */
    const VALUE_CURRENT = 'CURRENT';
    /**
     * Constant for value 'FUTURE'
     * @return string 'FUTURE'
     */
    const VALUE_FUTURE = 'FUTURE';
    /**
     * Constant for value 'OLD'
     * @return string 'OLD'
     */
    const VALUE_OLD = 'OLD';
    /**
     * Return allowed values
     * @uses self::VALUE_CURRENT
     * @uses self::VALUE_FUTURE
     * @uses self::VALUE_OLD
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CURRENT,
            self::VALUE_FUTURE,
            self::VALUE_OLD,
        ];
    }
}
