<?php

declare(strict_types=1);

namespace Booking\ArrayType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for availableActivityArray ArrayType
 * Meta information extracted from the WSDL
 * - final: #all
 * @subpackage Arrays
 */
class AvailableActivityArray extends AbstractStructArrayBase
{
    /**
     * The item
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\AvailableActivity[]
     */
    protected ?array $item = null;
    /**
     * Constructor method for availableActivityArray
     * @uses AvailableActivityArray::setItem()
     * @param \Booking\StructType\AvailableActivity[] $item
     */
    public function __construct(?array $item = null)
    {
        $this
            ->setItem($item);
    }
    /**
     * Get item value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\AvailableActivity[]
     */
    public function getItem(): ?array
    {
        return $this->item ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setItem method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateItemForArrayConstraintFromSetItem(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $availableActivityArrayItemItem) {
            // validation for constraint: itemType
            if (!$availableActivityArrayItemItem instanceof \Booking\StructType\AvailableActivity) {
                $invalidValues[] = is_object($availableActivityArrayItemItem) ? get_class($availableActivityArrayItemItem) : sprintf('%s(%s)', gettype($availableActivityArrayItemItem), var_export($availableActivityArrayItemItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The item property can only contain items of type \Booking\StructType\AvailableActivity, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set item value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\AvailableActivity[] $item
     * @return \Booking\ArrayType\AvailableActivityArray
     */
    public function setItem(?array $item = null): self
    {
        // validation for constraint: array
        if ('' !== ($itemArrayErrorMessage = self::validateItemForArrayConstraintFromSetItem($item))) {
            throw new InvalidArgumentException($itemArrayErrorMessage, __LINE__);
        }
        if (is_null($item) || (is_array($item) && empty($item))) {
            unset($this->item);
        } else {
            $this->item = $item;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \Booking\StructType\AvailableActivity|null
     */
    public function current(): ?\Booking\StructType\AvailableActivity
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \Booking\StructType\AvailableActivity|null
     */
    public function item($index): ?\Booking\StructType\AvailableActivity
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \Booking\StructType\AvailableActivity|null
     */
    public function first(): ?\Booking\StructType\AvailableActivity
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \Booking\StructType\AvailableActivity|null
     */
    public function last(): ?\Booking\StructType\AvailableActivity
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \Booking\StructType\AvailableActivity|null
     */
    public function offsetGet($offset): ?\Booking\StructType\AvailableActivity
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \Booking\StructType\AvailableActivity $item
     * @return \Booking\ArrayType\AvailableActivityArray
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\AvailableActivity) {
            throw new InvalidArgumentException(sprintf('The item property can only contain items of type \Booking\StructType\AvailableActivity, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string item
     */
    public function getAttributeName(): string
    {
        return 'item';
    }
}
