<?php

declare(strict_types=1);

namespace Booking\ArrayType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for activityGroupArray ArrayType
 * Meta information extracted from the WSDL
 * - final: #all
 * @subpackage Arrays
 */
class ActivityGroupArray extends AbstractStructArrayBase
{
    /**
     * The item
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\ActivityGroup[]
     */
    protected ?array $item = null;
    /**
     * Constructor method for activityGroupArray
     * @uses ActivityGroupArray::setItem()
     * @param \Booking\StructType\ActivityGroup[] $item
     */
    public function __construct(?array $item = null)
    {
        $this
            ->setItem($item);
    }
    /**
     * Get item value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\ActivityGroup[]
     */
    public function getItem(): ?array
    {
        return $this->item ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setItem method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateItemForArrayConstraintFromSetItem(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $activityGroupArrayItemItem) {
            // validation for constraint: itemType
            if (!$activityGroupArrayItemItem instanceof \Booking\StructType\ActivityGroup) {
                $invalidValues[] = is_object($activityGroupArrayItemItem) ? get_class($activityGroupArrayItemItem) : sprintf('%s(%s)', gettype($activityGroupArrayItemItem), var_export($activityGroupArrayItemItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The item property can only contain items of type \Booking\StructType\ActivityGroup, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set item value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\ActivityGroup[] $item
     * @return \Booking\ArrayType\ActivityGroupArray
     */
    public function setItem(?array $item = null): self
    {
        // validation for constraint: array
        if ('' !== ($itemArrayErrorMessage = self::validateItemForArrayConstraintFromSetItem($item))) {
            throw new InvalidArgumentException($itemArrayErrorMessage, __LINE__);
        }
        if (is_null($item) || (is_array($item) && empty($item))) {
            unset($this->item);
        } else {
            $this->item = $item;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \Booking\StructType\ActivityGroup|null
     */
    public function current(): ?\Booking\StructType\ActivityGroup
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \Booking\StructType\ActivityGroup|null
     */
    public function item($index): ?\Booking\StructType\ActivityGroup
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \Booking\StructType\ActivityGroup|null
     */
    public function first(): ?\Booking\StructType\ActivityGroup
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \Booking\StructType\ActivityGroup|null
     */
    public function last(): ?\Booking\StructType\ActivityGroup
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \Booking\StructType\ActivityGroup|null
     */
    public function offsetGet($offset): ?\Booking\StructType\ActivityGroup
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \Booking\StructType\ActivityGroup $item
     * @return \Booking\ArrayType\ActivityGroupArray
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\ActivityGroup) {
            throw new InvalidArgumentException(sprintf('The item property can only contain items of type \Booking\StructType\ActivityGroup, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string item
     */
    public function getAttributeName(): string
    {
        return 'item';
    }
}
