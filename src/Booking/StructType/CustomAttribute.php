<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for customAttribute StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class CustomAttribute extends AbstractStructBase
{
    /**
     * The attributeExternalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $attributeExternalId = null;
    /**
     * The attributeName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $attributeName = null;
    /**
     * The values
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\CustomAttributeValue[]
     */
    protected ?array $values = null;
    /**
     * Constructor method for customAttribute
     * @uses CustomAttribute::setAttributeExternalId()
     * @uses CustomAttribute::setAttributeName()
     * @uses CustomAttribute::setValues()
     * @param string $attributeExternalId
     * @param string $attributeName
     * @param \Booking\StructType\CustomAttributeValue[] $values
     */
    public function __construct(?string $attributeExternalId = null, ?string $attributeName = null, ?array $values = null)
    {
        $this
            ->setAttributeExternalId($attributeExternalId)
            ->setAttributeName($attributeName)
            ->setValues($values);
    }
    /**
     * Get attributeExternalId value
     * @return string|null
     */
    public function getAttributeExternalId(): ?string
    {
        return $this->attributeExternalId;
    }
    /**
     * Set attributeExternalId value
     * @param string $attributeExternalId
     * @return \Booking\StructType\CustomAttribute
     */
    public function setAttributeExternalId(?string $attributeExternalId = null): self
    {
        // validation for constraint: string
        if (!is_null($attributeExternalId) && !is_string($attributeExternalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($attributeExternalId, true), gettype($attributeExternalId)), __LINE__);
        }
        $this->attributeExternalId = $attributeExternalId;
        
        return $this;
    }
    /**
     * Get attributeName value
     * @return string|null
     */
    public function getAttributeName(): ?string
    {
        return $this->attributeName;
    }
    /**
     * Set attributeName value
     * @param string $attributeName
     * @return \Booking\StructType\CustomAttribute
     */
    public function setAttributeName(?string $attributeName = null): self
    {
        // validation for constraint: string
        if (!is_null($attributeName) && !is_string($attributeName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($attributeName, true), gettype($attributeName)), __LINE__);
        }
        $this->attributeName = $attributeName;
        
        return $this;
    }
    /**
     * Get values value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\CustomAttributeValue[]
     */
    public function getValues(): ?array
    {
        return $this->values ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setValues method
     * This method is willingly generated in order to preserve the one-line inline validation within the setValues method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateValuesForArrayConstraintFromSetValues(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $customAttributeValuesItem) {
            // validation for constraint: itemType
            if (!$customAttributeValuesItem instanceof \Booking\StructType\CustomAttributeValue) {
                $invalidValues[] = is_object($customAttributeValuesItem) ? get_class($customAttributeValuesItem) : sprintf('%s(%s)', gettype($customAttributeValuesItem), var_export($customAttributeValuesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The values property can only contain items of type \Booking\StructType\CustomAttributeValue, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set values value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\CustomAttributeValue[] $values
     * @return \Booking\StructType\CustomAttribute
     */
    public function setValues(?array $values = null): self
    {
        // validation for constraint: array
        if ('' !== ($valuesArrayErrorMessage = self::validateValuesForArrayConstraintFromSetValues($values))) {
            throw new InvalidArgumentException($valuesArrayErrorMessage, __LINE__);
        }
        if (is_null($values) || (is_array($values) && empty($values))) {
            unset($this->values);
        } else {
            $this->values = $values;
        }
        
        return $this;
    }
    /**
     * Add item to values value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\CustomAttributeValue $item
     * @return \Booking\StructType\CustomAttribute
     */
    public function addToValues(\Booking\StructType\CustomAttributeValue $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\CustomAttributeValue) {
            throw new InvalidArgumentException(sprintf('The values property can only contain items of type \Booking\StructType\CustomAttributeValue, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->values[] = $item;
        
        return $this;
    }
}
