<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for bookingChangeActivitySimpleParameters StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class BookingChangeActivitySimpleParameters extends AbstractStructBase
{
    /**
     * The activityId
     * @var int|null
     */
    protected ?int $activityId = null;
    /**
     * The bookingKey
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $bookingKey = null;
    /**
     * Constructor method for bookingChangeActivitySimpleParameters
     * @uses BookingChangeActivitySimpleParameters::setActivityId()
     * @uses BookingChangeActivitySimpleParameters::setBookingKey()
     * @param int $activityId
     * @param \Booking\StructType\CompositeKey $bookingKey
     */
    public function __construct(?int $activityId = null, ?\Booking\StructType\CompositeKey $bookingKey = null)
    {
        $this
            ->setActivityId($activityId)
            ->setBookingKey($bookingKey);
    }
    /**
     * Get activityId value
     * @return int|null
     */
    public function getActivityId(): ?int
    {
        return $this->activityId;
    }
    /**
     * Set activityId value
     * @param int $activityId
     * @return \Booking\StructType\BookingChangeActivitySimpleParameters
     */
    public function setActivityId(?int $activityId = null): self
    {
        // validation for constraint: int
        if (!is_null($activityId) && !(is_int($activityId) || ctype_digit($activityId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($activityId, true), gettype($activityId)), __LINE__);
        }
        $this->activityId = $activityId;
        
        return $this;
    }
    /**
     * Get bookingKey value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getBookingKey(): ?\Booking\StructType\CompositeKey
    {
        return $this->bookingKey;
    }
    /**
     * Set bookingKey value
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @return \Booking\StructType\BookingChangeActivitySimpleParameters
     */
    public function setBookingKey(?\Booking\StructType\CompositeKey $bookingKey = null): self
    {
        $this->bookingKey = $bookingKey;
        
        return $this;
    }
}
