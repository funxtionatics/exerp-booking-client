<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for checkPrivilegeForActivityParameters StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class CheckPrivilegeForActivityParameters extends AbstractStructBase
{
    /**
     * The centerId
     * @var int|null
     */
    protected ?int $centerId = null;
    /**
     * The personKey
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $personKey = null;
    /**
     * The activityId
     * @var int|null
     */
    protected ?int $activityId = null;
    /**
     * The date
     * @var string|null
     */
    protected ?string $date = null;
    /**
     * The timeInterval
     * @var \Booking\StructType\Interval|null
     */
    protected ?\Booking\StructType\Interval $timeInterval = null;
    /**
     * The includeTentative
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $includeTentative = null;
    /**
     * The productKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ApiProductKey|null
     */
    protected ?\Booking\StructType\ApiProductKey $productKey = null;
    /**
     * Constructor method for checkPrivilegeForActivityParameters
     * @uses CheckPrivilegeForActivityParameters::setCenterId()
     * @uses CheckPrivilegeForActivityParameters::setPersonKey()
     * @uses CheckPrivilegeForActivityParameters::setActivityId()
     * @uses CheckPrivilegeForActivityParameters::setDate()
     * @uses CheckPrivilegeForActivityParameters::setTimeInterval()
     * @uses CheckPrivilegeForActivityParameters::setIncludeTentative()
     * @uses CheckPrivilegeForActivityParameters::setProductKey()
     * @param int $centerId
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @param int $activityId
     * @param string $date
     * @param \Booking\StructType\Interval $timeInterval
     * @param bool $includeTentative
     * @param \Booking\StructType\ApiProductKey $productKey
     */
    public function __construct(?int $centerId = null, ?\Booking\StructType\ApiPersonKey $personKey = null, ?int $activityId = null, ?string $date = null, ?\Booking\StructType\Interval $timeInterval = null, ?bool $includeTentative = null, ?\Booking\StructType\ApiProductKey $productKey = null)
    {
        $this
            ->setCenterId($centerId)
            ->setPersonKey($personKey)
            ->setActivityId($activityId)
            ->setDate($date)
            ->setTimeInterval($timeInterval)
            ->setIncludeTentative($includeTentative)
            ->setProductKey($productKey);
    }
    /**
     * Get centerId value
     * @return int|null
     */
    public function getCenterId(): ?int
    {
        return $this->centerId;
    }
    /**
     * Set centerId value
     * @param int $centerId
     * @return \Booking\StructType\CheckPrivilegeForActivityParameters
     */
    public function setCenterId(?int $centerId = null): self
    {
        // validation for constraint: int
        if (!is_null($centerId) && !(is_int($centerId) || ctype_digit($centerId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($centerId, true), gettype($centerId)), __LINE__);
        }
        $this->centerId = $centerId;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @return \Booking\StructType\CheckPrivilegeForActivityParameters
     */
    public function setPersonKey(?\Booking\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get activityId value
     * @return int|null
     */
    public function getActivityId(): ?int
    {
        return $this->activityId;
    }
    /**
     * Set activityId value
     * @param int $activityId
     * @return \Booking\StructType\CheckPrivilegeForActivityParameters
     */
    public function setActivityId(?int $activityId = null): self
    {
        // validation for constraint: int
        if (!is_null($activityId) && !(is_int($activityId) || ctype_digit($activityId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($activityId, true), gettype($activityId)), __LINE__);
        }
        $this->activityId = $activityId;
        
        return $this;
    }
    /**
     * Get date value
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }
    /**
     * Set date value
     * @param string $date
     * @return \Booking\StructType\CheckPrivilegeForActivityParameters
     */
    public function setDate(?string $date = null): self
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->date = $date;
        
        return $this;
    }
    /**
     * Get timeInterval value
     * @return \Booking\StructType\Interval|null
     */
    public function getTimeInterval(): ?\Booking\StructType\Interval
    {
        return $this->timeInterval;
    }
    /**
     * Set timeInterval value
     * @param \Booking\StructType\Interval $timeInterval
     * @return \Booking\StructType\CheckPrivilegeForActivityParameters
     */
    public function setTimeInterval(?\Booking\StructType\Interval $timeInterval = null): self
    {
        $this->timeInterval = $timeInterval;
        
        return $this;
    }
    /**
     * Get includeTentative value
     * @return bool|null
     */
    public function getIncludeTentative(): ?bool
    {
        return $this->includeTentative;
    }
    /**
     * Set includeTentative value
     * @param bool $includeTentative
     * @return \Booking\StructType\CheckPrivilegeForActivityParameters
     */
    public function setIncludeTentative(?bool $includeTentative = null): self
    {
        // validation for constraint: boolean
        if (!is_null($includeTentative) && !is_bool($includeTentative)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($includeTentative, true), gettype($includeTentative)), __LINE__);
        }
        $this->includeTentative = $includeTentative;
        
        return $this;
    }
    /**
     * Get productKey value
     * @return \Booking\StructType\ApiProductKey|null
     */
    public function getProductKey(): ?\Booking\StructType\ApiProductKey
    {
        return $this->productKey;
    }
    /**
     * Set productKey value
     * @param \Booking\StructType\ApiProductKey $productKey
     * @return \Booking\StructType\CheckPrivilegeForActivityParameters
     */
    public function setProductKey(?\Booking\StructType\ApiProductKey $productKey = null): self
    {
        $this->productKey = $productKey;
        
        return $this;
    }
}
