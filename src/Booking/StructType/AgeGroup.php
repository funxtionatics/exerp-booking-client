<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ageGroup StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AgeGroup extends AbstractStructBase
{
    /**
     * The id
     * @var int|null
     */
    protected ?int $id = null;
    /**
     * The maxAge
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\AgeGroupLimit|null
     */
    protected ?\Booking\StructType\AgeGroupLimit $maxAge = null;
    /**
     * The minAge
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\AgeGroupLimit|null
     */
    protected ?\Booking\StructType\AgeGroupLimit $minAge = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * Constructor method for ageGroup
     * @uses AgeGroup::setId()
     * @uses AgeGroup::setMaxAge()
     * @uses AgeGroup::setMinAge()
     * @uses AgeGroup::setName()
     * @param int $id
     * @param \Booking\StructType\AgeGroupLimit $maxAge
     * @param \Booking\StructType\AgeGroupLimit $minAge
     * @param string $name
     */
    public function __construct(?int $id = null, ?\Booking\StructType\AgeGroupLimit $maxAge = null, ?\Booking\StructType\AgeGroupLimit $minAge = null, ?string $name = null)
    {
        $this
            ->setId($id)
            ->setMaxAge($maxAge)
            ->setMinAge($minAge)
            ->setName($name);
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \Booking\StructType\AgeGroup
     */
    public function setId(?int $id = null): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
    /**
     * Get maxAge value
     * @return \Booking\StructType\AgeGroupLimit|null
     */
    public function getMaxAge(): ?\Booking\StructType\AgeGroupLimit
    {
        return $this->maxAge;
    }
    /**
     * Set maxAge value
     * @param \Booking\StructType\AgeGroupLimit $maxAge
     * @return \Booking\StructType\AgeGroup
     */
    public function setMaxAge(?\Booking\StructType\AgeGroupLimit $maxAge = null): self
    {
        $this->maxAge = $maxAge;
        
        return $this;
    }
    /**
     * Get minAge value
     * @return \Booking\StructType\AgeGroupLimit|null
     */
    public function getMinAge(): ?\Booking\StructType\AgeGroupLimit
    {
        return $this->minAge;
    }
    /**
     * Set minAge value
     * @param \Booking\StructType\AgeGroupLimit $minAge
     * @return \Booking\StructType\AgeGroup
     */
    public function setMinAge(?\Booking\StructType\AgeGroupLimit $minAge = null): self
    {
        $this->minAge = $minAge;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\AgeGroup
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
}
