<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for BookingChangeActivityParticipation StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class BookingChangeActivityParticipation extends AbstractStructBase
{
    /**
     * The key
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $key = null;
    /**
     * The personKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $personKey = null;
    /**
     * The state
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $state = null;
    /**
     * Constructor method for BookingChangeActivityParticipation
     * @uses BookingChangeActivityParticipation::setKey()
     * @uses BookingChangeActivityParticipation::setPersonKey()
     * @uses BookingChangeActivityParticipation::setState()
     * @param \Booking\StructType\CompositeKey $key
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @param string $state
     */
    public function __construct(?\Booking\StructType\CompositeKey $key = null, ?\Booking\StructType\ApiPersonKey $personKey = null, ?string $state = null)
    {
        $this
            ->setKey($key)
            ->setPersonKey($personKey)
            ->setState($state);
    }
    /**
     * Get key value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getKey(): ?\Booking\StructType\CompositeKey
    {
        return $this->key;
    }
    /**
     * Set key value
     * @param \Booking\StructType\CompositeKey $key
     * @return \Booking\StructType\BookingChangeActivityParticipation
     */
    public function setKey(?\Booking\StructType\CompositeKey $key = null): self
    {
        $this->key = $key;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @return \Booking\StructType\BookingChangeActivityParticipation
     */
    public function setPersonKey(?\Booking\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get state value
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }
    /**
     * Set state value
     * @uses \Booking\EnumType\ParticipationState::valueIsValid()
     * @uses \Booking\EnumType\ParticipationState::getValidValues()
     * @throws InvalidArgumentException
     * @param string $state
     * @return \Booking\StructType\BookingChangeActivityParticipation
     */
    public function setState(?string $state = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\ParticipationState::valueIsValid($state)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\ParticipationState', is_array($state) ? implode(', ', $state) : var_export($state, true), implode(', ', \Booking\EnumType\ParticipationState::getValidValues())), __LINE__);
        }
        $this->state = $state;
        
        return $this;
    }
}
