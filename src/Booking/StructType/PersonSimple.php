<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for personSimple StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class PersonSimple extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\PersonName|null
     */
    protected ?\Booking\StructType\PersonName $name = null;
    /**
     * The personKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $personKey = null;
    /**
     * Constructor method for personSimple
     * @uses PersonSimple::setName()
     * @uses PersonSimple::setPersonKey()
     * @param \Booking\StructType\PersonName $name
     * @param \Booking\StructType\ApiPersonKey $personKey
     */
    public function __construct(?\Booking\StructType\PersonName $name = null, ?\Booking\StructType\ApiPersonKey $personKey = null)
    {
        $this
            ->setName($name)
            ->setPersonKey($personKey);
    }
    /**
     * Get name value
     * @return \Booking\StructType\PersonName|null
     */
    public function getName(): ?\Booking\StructType\PersonName
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param \Booking\StructType\PersonName $name
     * @return \Booking\StructType\PersonSimple
     */
    public function setName(?\Booking\StructType\PersonName $name = null): self
    {
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @return \Booking\StructType\PersonSimple
     */
    public function setPersonKey(?\Booking\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
}
