<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for apiProductKey StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class ApiProductKey extends AbstractStructBase
{
    /**
     * The center
     * @var int|null
     */
    protected ?int $center = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The id
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $id = null;
    /**
     * Constructor method for apiProductKey
     * @uses ApiProductKey::setCenter()
     * @uses ApiProductKey::setExternalId()
     * @uses ApiProductKey::setId()
     * @param int $center
     * @param string $externalId
     * @param int $id
     */
    public function __construct(?int $center = null, ?string $externalId = null, ?int $id = null)
    {
        $this
            ->setCenter($center)
            ->setExternalId($externalId)
            ->setId($id);
    }
    /**
     * Get center value
     * @return int|null
     */
    public function getCenter(): ?int
    {
        return $this->center;
    }
    /**
     * Set center value
     * @param int $center
     * @return \Booking\StructType\ApiProductKey
     */
    public function setCenter(?int $center = null): self
    {
        // validation for constraint: int
        if (!is_null($center) && !(is_int($center) || ctype_digit($center))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($center, true), gettype($center)), __LINE__);
        }
        $this->center = $center;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Booking\StructType\ApiProductKey
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \Booking\StructType\ApiProductKey
     */
    public function setId(?int $id = null): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
}
