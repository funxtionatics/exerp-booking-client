<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for creditCardPaymentInfo StructType
 * Meta information extracted from the WSDL
 * - final: extension restriction
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class CreditCardPaymentInfo extends AbstractStructBase
{
    /**
     * The cardDetailsEncrypted
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $cardDetailsEncrypted = null;
    /**
     * The createAgreement
     * @var bool|null
     */
    protected ?bool $createAgreement = null;
    /**
     * The replacePaymentAgreementKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CompositeSubKey|null
     */
    protected ?\Booking\StructType\CompositeSubKey $replacePaymentAgreementKey = null;
    /**
     * Constructor method for creditCardPaymentInfo
     * @uses CreditCardPaymentInfo::setCardDetailsEncrypted()
     * @uses CreditCardPaymentInfo::setCreateAgreement()
     * @uses CreditCardPaymentInfo::setReplacePaymentAgreementKey()
     * @param string $cardDetailsEncrypted
     * @param bool $createAgreement
     * @param \Booking\StructType\CompositeSubKey $replacePaymentAgreementKey
     */
    public function __construct(?string $cardDetailsEncrypted = null, ?bool $createAgreement = null, ?\Booking\StructType\CompositeSubKey $replacePaymentAgreementKey = null)
    {
        $this
            ->setCardDetailsEncrypted($cardDetailsEncrypted)
            ->setCreateAgreement($createAgreement)
            ->setReplacePaymentAgreementKey($replacePaymentAgreementKey);
    }
    /**
     * Get cardDetailsEncrypted value
     * @return string|null
     */
    public function getCardDetailsEncrypted(): ?string
    {
        return $this->cardDetailsEncrypted;
    }
    /**
     * Set cardDetailsEncrypted value
     * @param string $cardDetailsEncrypted
     * @return \Booking\StructType\CreditCardPaymentInfo
     */
    public function setCardDetailsEncrypted(?string $cardDetailsEncrypted = null): self
    {
        // validation for constraint: string
        if (!is_null($cardDetailsEncrypted) && !is_string($cardDetailsEncrypted)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cardDetailsEncrypted, true), gettype($cardDetailsEncrypted)), __LINE__);
        }
        $this->cardDetailsEncrypted = $cardDetailsEncrypted;
        
        return $this;
    }
    /**
     * Get createAgreement value
     * @return bool|null
     */
    public function getCreateAgreement(): ?bool
    {
        return $this->createAgreement;
    }
    /**
     * Set createAgreement value
     * @param bool $createAgreement
     * @return \Booking\StructType\CreditCardPaymentInfo
     */
    public function setCreateAgreement(?bool $createAgreement = null): self
    {
        // validation for constraint: boolean
        if (!is_null($createAgreement) && !is_bool($createAgreement)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($createAgreement, true), gettype($createAgreement)), __LINE__);
        }
        $this->createAgreement = $createAgreement;
        
        return $this;
    }
    /**
     * Get replacePaymentAgreementKey value
     * @return \Booking\StructType\CompositeSubKey|null
     */
    public function getReplacePaymentAgreementKey(): ?\Booking\StructType\CompositeSubKey
    {
        return $this->replacePaymentAgreementKey;
    }
    /**
     * Set replacePaymentAgreementKey value
     * @param \Booking\StructType\CompositeSubKey $replacePaymentAgreementKey
     * @return \Booking\StructType\CreditCardPaymentInfo
     */
    public function setReplacePaymentAgreementKey(?\Booking\StructType\CompositeSubKey $replacePaymentAgreementKey = null): self
    {
        $this->replacePaymentAgreementKey = $replacePaymentAgreementKey;
        
        return $this;
    }
}
