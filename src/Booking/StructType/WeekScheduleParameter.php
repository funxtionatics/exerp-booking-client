<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for weekScheduleParameter StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class WeekScheduleParameter extends AbstractStructBase
{
    /**
     * The activityGroupId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $activityGroupId = null;
    /**
     * The activityId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $activityId = null;
    /**
     * The centerId
     * @var int|null
     */
    protected ?int $centerId = null;
    /**
     * The startDate
     * @var string|null
     */
    protected ?string $startDate = null;
    /**
     * Constructor method for weekScheduleParameter
     * @uses WeekScheduleParameter::setActivityGroupId()
     * @uses WeekScheduleParameter::setActivityId()
     * @uses WeekScheduleParameter::setCenterId()
     * @uses WeekScheduleParameter::setStartDate()
     * @param int $activityGroupId
     * @param int $activityId
     * @param int $centerId
     * @param string $startDate
     */
    public function __construct(?int $activityGroupId = null, ?int $activityId = null, ?int $centerId = null, ?string $startDate = null)
    {
        $this
            ->setActivityGroupId($activityGroupId)
            ->setActivityId($activityId)
            ->setCenterId($centerId)
            ->setStartDate($startDate);
    }
    /**
     * Get activityGroupId value
     * @return int|null
     */
    public function getActivityGroupId(): ?int
    {
        return $this->activityGroupId;
    }
    /**
     * Set activityGroupId value
     * @param int $activityGroupId
     * @return \Booking\StructType\WeekScheduleParameter
     */
    public function setActivityGroupId(?int $activityGroupId = null): self
    {
        // validation for constraint: int
        if (!is_null($activityGroupId) && !(is_int($activityGroupId) || ctype_digit($activityGroupId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($activityGroupId, true), gettype($activityGroupId)), __LINE__);
        }
        $this->activityGroupId = $activityGroupId;
        
        return $this;
    }
    /**
     * Get activityId value
     * @return int|null
     */
    public function getActivityId(): ?int
    {
        return $this->activityId;
    }
    /**
     * Set activityId value
     * @param int $activityId
     * @return \Booking\StructType\WeekScheduleParameter
     */
    public function setActivityId(?int $activityId = null): self
    {
        // validation for constraint: int
        if (!is_null($activityId) && !(is_int($activityId) || ctype_digit($activityId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($activityId, true), gettype($activityId)), __LINE__);
        }
        $this->activityId = $activityId;
        
        return $this;
    }
    /**
     * Get centerId value
     * @return int|null
     */
    public function getCenterId(): ?int
    {
        return $this->centerId;
    }
    /**
     * Set centerId value
     * @param int $centerId
     * @return \Booking\StructType\WeekScheduleParameter
     */
    public function setCenterId(?int $centerId = null): self
    {
        // validation for constraint: int
        if (!is_null($centerId) && !(is_int($centerId) || ctype_digit($centerId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($centerId, true), gettype($centerId)), __LINE__);
        }
        $this->centerId = $centerId;
        
        return $this;
    }
    /**
     * Get startDate value
     * @return string|null
     */
    public function getStartDate(): ?string
    {
        return $this->startDate;
    }
    /**
     * Set startDate value
     * @param string $startDate
     * @return \Booking\StructType\WeekScheduleParameter
     */
    public function setStartDate(?string $startDate = null): self
    {
        // validation for constraint: string
        if (!is_null($startDate) && !is_string($startDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startDate, true), gettype($startDate)), __LINE__);
        }
        $this->startDate = $startDate;
        
        return $this;
    }
}
