<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for products StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Products extends AbstractStructBase
{
    /**
     * The product
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Booking\StructType\BookingProduct[]
     */
    protected ?array $product = null;
    /**
     * Constructor method for products
     * @uses Products::setProduct()
     * @param \Booking\StructType\BookingProduct[] $product
     */
    public function __construct(?array $product = null)
    {
        $this
            ->setProduct($product);
    }
    /**
     * Get product value
     * @return \Booking\StructType\BookingProduct[]
     */
    public function getProduct(): ?array
    {
        return $this->product;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setProduct method
     * This method is willingly generated in order to preserve the one-line inline validation within the setProduct method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateProductForArrayConstraintFromSetProduct(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $productsProductItem) {
            // validation for constraint: itemType
            if (!$productsProductItem instanceof \Booking\StructType\BookingProduct) {
                $invalidValues[] = is_object($productsProductItem) ? get_class($productsProductItem) : sprintf('%s(%s)', gettype($productsProductItem), var_export($productsProductItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The product property can only contain items of type \Booking\StructType\BookingProduct, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set product value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\BookingProduct[] $product
     * @return \Booking\StructType\Products
     */
    public function setProduct(?array $product = null): self
    {
        // validation for constraint: array
        if ('' !== ($productArrayErrorMessage = self::validateProductForArrayConstraintFromSetProduct($product))) {
            throw new InvalidArgumentException($productArrayErrorMessage, __LINE__);
        }
        $this->product = $product;
        
        return $this;
    }
    /**
     * Add item to product value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\BookingProduct $item
     * @return \Booking\StructType\Products
     */
    public function addToProduct(\Booking\StructType\BookingProduct $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\BookingProduct) {
            throw new InvalidArgumentException(sprintf('The product property can only contain items of type \Booking\StructType\BookingProduct, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->product[] = $item;
        
        return $this;
    }
}
