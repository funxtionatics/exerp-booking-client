<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getProductsForBookingParameters StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class GetProductsForBookingParameters extends AbstractStructBase
{
    /**
     * The bookingKey
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $bookingKey = null;
    /**
     * The personKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $personKey = null;
    /**
     * The productType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $productType = null;
    /**
     * The productGroupId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $productGroupId = null;
    /**
     * The onlyAvailableOnWeb
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $onlyAvailableOnWeb = null;
    /**
     * Constructor method for getProductsForBookingParameters
     * @uses GetProductsForBookingParameters::setBookingKey()
     * @uses GetProductsForBookingParameters::setPersonKey()
     * @uses GetProductsForBookingParameters::setProductType()
     * @uses GetProductsForBookingParameters::setProductGroupId()
     * @uses GetProductsForBookingParameters::setOnlyAvailableOnWeb()
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @param string $productType
     * @param int $productGroupId
     * @param bool $onlyAvailableOnWeb
     */
    public function __construct(?\Booking\StructType\CompositeKey $bookingKey = null, ?\Booking\StructType\ApiPersonKey $personKey = null, ?string $productType = null, ?int $productGroupId = null, ?bool $onlyAvailableOnWeb = null)
    {
        $this
            ->setBookingKey($bookingKey)
            ->setPersonKey($personKey)
            ->setProductType($productType)
            ->setProductGroupId($productGroupId)
            ->setOnlyAvailableOnWeb($onlyAvailableOnWeb);
    }
    /**
     * Get bookingKey value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getBookingKey(): ?\Booking\StructType\CompositeKey
    {
        return $this->bookingKey;
    }
    /**
     * Set bookingKey value
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @return \Booking\StructType\GetProductsForBookingParameters
     */
    public function setBookingKey(?\Booking\StructType\CompositeKey $bookingKey = null): self
    {
        $this->bookingKey = $bookingKey;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @return \Booking\StructType\GetProductsForBookingParameters
     */
    public function setPersonKey(?\Booking\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get productType value
     * @return string|null
     */
    public function getProductType(): ?string
    {
        return $this->productType;
    }
    /**
     * Set productType value
     * @uses \Booking\EnumType\ProductType::valueIsValid()
     * @uses \Booking\EnumType\ProductType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $productType
     * @return \Booking\StructType\GetProductsForBookingParameters
     */
    public function setProductType(?string $productType = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\ProductType::valueIsValid($productType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\ProductType', is_array($productType) ? implode(', ', $productType) : var_export($productType, true), implode(', ', \Booking\EnumType\ProductType::getValidValues())), __LINE__);
        }
        $this->productType = $productType;
        
        return $this;
    }
    /**
     * Get productGroupId value
     * @return int|null
     */
    public function getProductGroupId(): ?int
    {
        return $this->productGroupId;
    }
    /**
     * Set productGroupId value
     * @param int $productGroupId
     * @return \Booking\StructType\GetProductsForBookingParameters
     */
    public function setProductGroupId(?int $productGroupId = null): self
    {
        // validation for constraint: int
        if (!is_null($productGroupId) && !(is_int($productGroupId) || ctype_digit($productGroupId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($productGroupId, true), gettype($productGroupId)), __LINE__);
        }
        $this->productGroupId = $productGroupId;
        
        return $this;
    }
    /**
     * Get onlyAvailableOnWeb value
     * @return bool|null
     */
    public function getOnlyAvailableOnWeb(): ?bool
    {
        return $this->onlyAvailableOnWeb;
    }
    /**
     * Set onlyAvailableOnWeb value
     * @param bool $onlyAvailableOnWeb
     * @return \Booking\StructType\GetProductsForBookingParameters
     */
    public function setOnlyAvailableOnWeb(?bool $onlyAvailableOnWeb = null): self
    {
        // validation for constraint: boolean
        if (!is_null($onlyAvailableOnWeb) && !is_bool($onlyAvailableOnWeb)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($onlyAvailableOnWeb, true), gettype($onlyAvailableOnWeb)), __LINE__);
        }
        $this->onlyAvailableOnWeb = $onlyAvailableOnWeb;
        
        return $this;
    }
}
