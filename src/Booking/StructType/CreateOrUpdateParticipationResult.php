<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createOrUpdateParticipationResult StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class CreateOrUpdateParticipationResult extends AbstractStructBase
{
    /**
     * The invoiceKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $invoiceKey = null;
    /**
     * The participations
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\Participations|null
     */
    protected ?\Booking\StructType\Participations $participations = null;
    /**
     * Constructor method for createOrUpdateParticipationResult
     * @uses CreateOrUpdateParticipationResult::setInvoiceKey()
     * @uses CreateOrUpdateParticipationResult::setParticipations()
     * @param \Booking\StructType\CompositeKey $invoiceKey
     * @param \Booking\StructType\Participations $participations
     */
    public function __construct(?\Booking\StructType\CompositeKey $invoiceKey = null, ?\Booking\StructType\Participations $participations = null)
    {
        $this
            ->setInvoiceKey($invoiceKey)
            ->setParticipations($participations);
    }
    /**
     * Get invoiceKey value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getInvoiceKey(): ?\Booking\StructType\CompositeKey
    {
        return $this->invoiceKey;
    }
    /**
     * Set invoiceKey value
     * @param \Booking\StructType\CompositeKey $invoiceKey
     * @return \Booking\StructType\CreateOrUpdateParticipationResult
     */
    public function setInvoiceKey(?\Booking\StructType\CompositeKey $invoiceKey = null): self
    {
        $this->invoiceKey = $invoiceKey;
        
        return $this;
    }
    /**
     * Get participations value
     * @return \Booking\StructType\Participations|null
     */
    public function getParticipations(): ?\Booking\StructType\Participations
    {
        return $this->participations;
    }
    /**
     * Set participations value
     * @param \Booking\StructType\Participations $participations
     * @return \Booking\StructType\CreateOrUpdateParticipationResult
     */
    public function setParticipations(?\Booking\StructType\Participations $participations = null): self
    {
        $this->participations = $participations;
        
        return $this;
    }
}
