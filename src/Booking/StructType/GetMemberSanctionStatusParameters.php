<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getMemberSanctionStatusParameters StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class GetMemberSanctionStatusParameters extends AbstractStructBase
{
    /**
     * The personKey
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $personKey = null;
    /**
     * The fromDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $fromDate = null;
    /**
     * Constructor method for getMemberSanctionStatusParameters
     * @uses GetMemberSanctionStatusParameters::setPersonKey()
     * @uses GetMemberSanctionStatusParameters::setFromDate()
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @param string $fromDate
     */
    public function __construct(?\Booking\StructType\ApiPersonKey $personKey = null, ?string $fromDate = null)
    {
        $this
            ->setPersonKey($personKey)
            ->setFromDate($fromDate);
    }
    /**
     * Get personKey value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @return \Booking\StructType\GetMemberSanctionStatusParameters
     */
    public function setPersonKey(?\Booking\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get fromDate value
     * @return string|null
     */
    public function getFromDate(): ?string
    {
        return $this->fromDate;
    }
    /**
     * Set fromDate value
     * @param string $fromDate
     * @return \Booking\StructType\GetMemberSanctionStatusParameters
     */
    public function setFromDate(?string $fromDate = null): self
    {
        // validation for constraint: string
        if (!is_null($fromDate) && !is_string($fromDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fromDate, true), gettype($fromDate)), __LINE__);
        }
        $this->fromDate = $fromDate;
        
        return $this;
    }
}
