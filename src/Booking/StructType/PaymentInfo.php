<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for paymentInfo StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class PaymentInfo extends AbstractStructBase
{
    /**
     * The amountPaidByCustomer
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $amountPaidByCustomer = null;
    /**
     * The creditCardPaymentInfo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CreditCardPaymentInfo|null
     */
    protected ?\Booking\StructType\CreditCardPaymentInfo $creditCardPaymentInfo = null;
    /**
     * The creditCardTransactionRef
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $creditCardTransactionRef = null;
    /**
     * The installmentPlanConfigExternalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $installmentPlanConfigExternalId = null;
    /**
     * The paymentAgreementKeyForCollection
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CompositeSubKey|null
     */
    protected ?\Booking\StructType\CompositeSubKey $paymentAgreementKeyForCollection = null;
    /**
     * The paymentMethod
     * @var string|null
     */
    protected ?string $paymentMethod = null;
    /**
     * The shoppingBasketCenter
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $shoppingBasketCenter = null;
    /**
     * Constructor method for paymentInfo
     * @uses PaymentInfo::setAmountPaidByCustomer()
     * @uses PaymentInfo::setCreditCardPaymentInfo()
     * @uses PaymentInfo::setCreditCardTransactionRef()
     * @uses PaymentInfo::setInstallmentPlanConfigExternalId()
     * @uses PaymentInfo::setPaymentAgreementKeyForCollection()
     * @uses PaymentInfo::setPaymentMethod()
     * @uses PaymentInfo::setShoppingBasketCenter()
     * @param string $amountPaidByCustomer
     * @param \Booking\StructType\CreditCardPaymentInfo $creditCardPaymentInfo
     * @param string $creditCardTransactionRef
     * @param string $installmentPlanConfigExternalId
     * @param \Booking\StructType\CompositeSubKey $paymentAgreementKeyForCollection
     * @param string $paymentMethod
     * @param int $shoppingBasketCenter
     */
    public function __construct(?string $amountPaidByCustomer = null, ?\Booking\StructType\CreditCardPaymentInfo $creditCardPaymentInfo = null, ?string $creditCardTransactionRef = null, ?string $installmentPlanConfigExternalId = null, ?\Booking\StructType\CompositeSubKey $paymentAgreementKeyForCollection = null, ?string $paymentMethod = null, ?int $shoppingBasketCenter = null)
    {
        $this
            ->setAmountPaidByCustomer($amountPaidByCustomer)
            ->setCreditCardPaymentInfo($creditCardPaymentInfo)
            ->setCreditCardTransactionRef($creditCardTransactionRef)
            ->setInstallmentPlanConfigExternalId($installmentPlanConfigExternalId)
            ->setPaymentAgreementKeyForCollection($paymentAgreementKeyForCollection)
            ->setPaymentMethod($paymentMethod)
            ->setShoppingBasketCenter($shoppingBasketCenter);
    }
    /**
     * Get amountPaidByCustomer value
     * @return string|null
     */
    public function getAmountPaidByCustomer(): ?string
    {
        return $this->amountPaidByCustomer;
    }
    /**
     * Set amountPaidByCustomer value
     * @param string $amountPaidByCustomer
     * @return \Booking\StructType\PaymentInfo
     */
    public function setAmountPaidByCustomer(?string $amountPaidByCustomer = null): self
    {
        // validation for constraint: string
        if (!is_null($amountPaidByCustomer) && !is_string($amountPaidByCustomer)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($amountPaidByCustomer, true), gettype($amountPaidByCustomer)), __LINE__);
        }
        $this->amountPaidByCustomer = $amountPaidByCustomer;
        
        return $this;
    }
    /**
     * Get creditCardPaymentInfo value
     * @return \Booking\StructType\CreditCardPaymentInfo|null
     */
    public function getCreditCardPaymentInfo(): ?\Booking\StructType\CreditCardPaymentInfo
    {
        return $this->creditCardPaymentInfo;
    }
    /**
     * Set creditCardPaymentInfo value
     * @param \Booking\StructType\CreditCardPaymentInfo $creditCardPaymentInfo
     * @return \Booking\StructType\PaymentInfo
     */
    public function setCreditCardPaymentInfo(?\Booking\StructType\CreditCardPaymentInfo $creditCardPaymentInfo = null): self
    {
        $this->creditCardPaymentInfo = $creditCardPaymentInfo;
        
        return $this;
    }
    /**
     * Get creditCardTransactionRef value
     * @return string|null
     */
    public function getCreditCardTransactionRef(): ?string
    {
        return $this->creditCardTransactionRef;
    }
    /**
     * Set creditCardTransactionRef value
     * @param string $creditCardTransactionRef
     * @return \Booking\StructType\PaymentInfo
     */
    public function setCreditCardTransactionRef(?string $creditCardTransactionRef = null): self
    {
        // validation for constraint: string
        if (!is_null($creditCardTransactionRef) && !is_string($creditCardTransactionRef)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($creditCardTransactionRef, true), gettype($creditCardTransactionRef)), __LINE__);
        }
        $this->creditCardTransactionRef = $creditCardTransactionRef;
        
        return $this;
    }
    /**
     * Get installmentPlanConfigExternalId value
     * @return string|null
     */
    public function getInstallmentPlanConfigExternalId(): ?string
    {
        return $this->installmentPlanConfigExternalId;
    }
    /**
     * Set installmentPlanConfigExternalId value
     * @param string $installmentPlanConfigExternalId
     * @return \Booking\StructType\PaymentInfo
     */
    public function setInstallmentPlanConfigExternalId(?string $installmentPlanConfigExternalId = null): self
    {
        // validation for constraint: string
        if (!is_null($installmentPlanConfigExternalId) && !is_string($installmentPlanConfigExternalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($installmentPlanConfigExternalId, true), gettype($installmentPlanConfigExternalId)), __LINE__);
        }
        $this->installmentPlanConfigExternalId = $installmentPlanConfigExternalId;
        
        return $this;
    }
    /**
     * Get paymentAgreementKeyForCollection value
     * @return \Booking\StructType\CompositeSubKey|null
     */
    public function getPaymentAgreementKeyForCollection(): ?\Booking\StructType\CompositeSubKey
    {
        return $this->paymentAgreementKeyForCollection;
    }
    /**
     * Set paymentAgreementKeyForCollection value
     * @param \Booking\StructType\CompositeSubKey $paymentAgreementKeyForCollection
     * @return \Booking\StructType\PaymentInfo
     */
    public function setPaymentAgreementKeyForCollection(?\Booking\StructType\CompositeSubKey $paymentAgreementKeyForCollection = null): self
    {
        $this->paymentAgreementKeyForCollection = $paymentAgreementKeyForCollection;
        
        return $this;
    }
    /**
     * Get paymentMethod value
     * @return string|null
     */
    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }
    /**
     * Set paymentMethod value
     * @uses \Booking\EnumType\PaymentMethod::valueIsValid()
     * @uses \Booking\EnumType\PaymentMethod::getValidValues()
     * @throws InvalidArgumentException
     * @param string $paymentMethod
     * @return \Booking\StructType\PaymentInfo
     */
    public function setPaymentMethod(?string $paymentMethod = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\PaymentMethod::valueIsValid($paymentMethod)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\PaymentMethod', is_array($paymentMethod) ? implode(', ', $paymentMethod) : var_export($paymentMethod, true), implode(', ', \Booking\EnumType\PaymentMethod::getValidValues())), __LINE__);
        }
        $this->paymentMethod = $paymentMethod;
        
        return $this;
    }
    /**
     * Get shoppingBasketCenter value
     * @return int|null
     */
    public function getShoppingBasketCenter(): ?int
    {
        return $this->shoppingBasketCenter;
    }
    /**
     * Set shoppingBasketCenter value
     * @param int $shoppingBasketCenter
     * @return \Booking\StructType\PaymentInfo
     */
    public function setShoppingBasketCenter(?int $shoppingBasketCenter = null): self
    {
        // validation for constraint: int
        if (!is_null($shoppingBasketCenter) && !(is_int($shoppingBasketCenter) || ctype_digit($shoppingBasketCenter))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($shoppingBasketCenter, true), gettype($shoppingBasketCenter)), __LINE__);
        }
        $this->shoppingBasketCenter = $shoppingBasketCenter;
        
        return $this;
    }
}
