<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for instructors StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Instructors extends AbstractStructBase
{
    /**
     * The instructor
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Booking\StructType\PersonSimple[]
     */
    protected ?array $instructor = null;
    /**
     * Constructor method for instructors
     * @uses Instructors::setInstructor()
     * @param \Booking\StructType\PersonSimple[] $instructor
     */
    public function __construct(?array $instructor = null)
    {
        $this
            ->setInstructor($instructor);
    }
    /**
     * Get instructor value
     * @return \Booking\StructType\PersonSimple[]
     */
    public function getInstructor(): ?array
    {
        return $this->instructor;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setInstructor method
     * This method is willingly generated in order to preserve the one-line inline validation within the setInstructor method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateInstructorForArrayConstraintFromSetInstructor(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $instructorsInstructorItem) {
            // validation for constraint: itemType
            if (!$instructorsInstructorItem instanceof \Booking\StructType\PersonSimple) {
                $invalidValues[] = is_object($instructorsInstructorItem) ? get_class($instructorsInstructorItem) : sprintf('%s(%s)', gettype($instructorsInstructorItem), var_export($instructorsInstructorItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The instructor property can only contain items of type \Booking\StructType\PersonSimple, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set instructor value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\PersonSimple[] $instructor
     * @return \Booking\StructType\Instructors
     */
    public function setInstructor(?array $instructor = null): self
    {
        // validation for constraint: array
        if ('' !== ($instructorArrayErrorMessage = self::validateInstructorForArrayConstraintFromSetInstructor($instructor))) {
            throw new InvalidArgumentException($instructorArrayErrorMessage, __LINE__);
        }
        $this->instructor = $instructor;
        
        return $this;
    }
    /**
     * Add item to instructor value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\PersonSimple $item
     * @return \Booking\StructType\Instructors
     */
    public function addToInstructor(\Booking\StructType\PersonSimple $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\PersonSimple) {
            throw new InvalidArgumentException(sprintf('The instructor property can only contain items of type \Booking\StructType\PersonSimple, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->instructor[] = $item;
        
        return $this;
    }
}
