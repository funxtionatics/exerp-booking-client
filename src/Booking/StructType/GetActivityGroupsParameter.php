<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getActivityGroupsParameter StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class GetActivityGroupsParameter extends AbstractStructBase
{
    /**
     * The bookableViaAPI
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $bookableViaAPI = null;
    /**
     * The centerId
     * @var int|null
     */
    protected ?int $centerId = null;
    /**
     * Constructor method for getActivityGroupsParameter
     * @uses GetActivityGroupsParameter::setBookableViaAPI()
     * @uses GetActivityGroupsParameter::setCenterId()
     * @param bool $bookableViaAPI
     * @param int $centerId
     */
    public function __construct(?bool $bookableViaAPI = null, ?int $centerId = null)
    {
        $this
            ->setBookableViaAPI($bookableViaAPI)
            ->setCenterId($centerId);
    }
    /**
     * Get bookableViaAPI value
     * @return bool|null
     */
    public function getBookableViaAPI(): ?bool
    {
        return $this->bookableViaAPI;
    }
    /**
     * Set bookableViaAPI value
     * @param bool $bookableViaAPI
     * @return \Booking\StructType\GetActivityGroupsParameter
     */
    public function setBookableViaAPI(?bool $bookableViaAPI = null): self
    {
        // validation for constraint: boolean
        if (!is_null($bookableViaAPI) && !is_bool($bookableViaAPI)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($bookableViaAPI, true), gettype($bookableViaAPI)), __LINE__);
        }
        $this->bookableViaAPI = $bookableViaAPI;
        
        return $this;
    }
    /**
     * Get centerId value
     * @return int|null
     */
    public function getCenterId(): ?int
    {
        return $this->centerId;
    }
    /**
     * Set centerId value
     * @param int $centerId
     * @return \Booking\StructType\GetActivityGroupsParameter
     */
    public function setCenterId(?int $centerId = null): self
    {
        // validation for constraint: int
        if (!is_null($centerId) && !(is_int($centerId) || ctype_digit($centerId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($centerId, true), gettype($centerId)), __LINE__);
        }
        $this->centerId = $centerId;
        
        return $this;
    }
}
