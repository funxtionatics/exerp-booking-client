<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for activityGroup StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class ActivityGroup extends AbstractStructBase
{
    /**
     * The bookableViaAPI
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $bookableViaAPI = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The id
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $id = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * Constructor method for activityGroup
     * @uses ActivityGroup::setBookableViaAPI()
     * @uses ActivityGroup::setDescription()
     * @uses ActivityGroup::setExternalId()
     * @uses ActivityGroup::setId()
     * @uses ActivityGroup::setName()
     * @param bool $bookableViaAPI
     * @param string $description
     * @param string $externalId
     * @param int $id
     * @param string $name
     */
    public function __construct(?bool $bookableViaAPI = null, ?string $description = null, ?string $externalId = null, ?int $id = null, ?string $name = null)
    {
        $this
            ->setBookableViaAPI($bookableViaAPI)
            ->setDescription($description)
            ->setExternalId($externalId)
            ->setId($id)
            ->setName($name);
    }
    /**
     * Get bookableViaAPI value
     * @return bool|null
     */
    public function getBookableViaAPI(): ?bool
    {
        return $this->bookableViaAPI;
    }
    /**
     * Set bookableViaAPI value
     * @param bool $bookableViaAPI
     * @return \Booking\StructType\ActivityGroup
     */
    public function setBookableViaAPI(?bool $bookableViaAPI = null): self
    {
        // validation for constraint: boolean
        if (!is_null($bookableViaAPI) && !is_bool($bookableViaAPI)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($bookableViaAPI, true), gettype($bookableViaAPI)), __LINE__);
        }
        $this->bookableViaAPI = $bookableViaAPI;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \Booking\StructType\ActivityGroup
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Booking\StructType\ActivityGroup
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \Booking\StructType\ActivityGroup
     */
    public function setId(?int $id = null): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\ActivityGroup
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
}
