<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for removeBookingRestrictionParameters StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class RemoveBookingRestrictionParameters extends AbstractStructBase
{
    /**
     * The bookingRestrictionKeys
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * @var \Booking\StructType\CompositeSubKey[]
     */
    protected ?array $bookingRestrictionKeys = null;
    /**
     * Constructor method for removeBookingRestrictionParameters
     * @uses RemoveBookingRestrictionParameters::setBookingRestrictionKeys()
     * @param \Booking\StructType\CompositeSubKey[] $bookingRestrictionKeys
     */
    public function __construct(?array $bookingRestrictionKeys = null)
    {
        $this
            ->setBookingRestrictionKeys($bookingRestrictionKeys);
    }
    /**
     * Get bookingRestrictionKeys value
     * @return \Booking\StructType\CompositeSubKey[]
     */
    public function getBookingRestrictionKeys(): ?array
    {
        return $this->bookingRestrictionKeys;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setBookingRestrictionKeys method
     * This method is willingly generated in order to preserve the one-line inline validation within the setBookingRestrictionKeys method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateBookingRestrictionKeysForArrayConstraintFromSetBookingRestrictionKeys(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $removeBookingRestrictionParametersBookingRestrictionKeysItem) {
            // validation for constraint: itemType
            if (!$removeBookingRestrictionParametersBookingRestrictionKeysItem instanceof \Booking\StructType\CompositeSubKey) {
                $invalidValues[] = is_object($removeBookingRestrictionParametersBookingRestrictionKeysItem) ? get_class($removeBookingRestrictionParametersBookingRestrictionKeysItem) : sprintf('%s(%s)', gettype($removeBookingRestrictionParametersBookingRestrictionKeysItem), var_export($removeBookingRestrictionParametersBookingRestrictionKeysItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The bookingRestrictionKeys property can only contain items of type \Booking\StructType\CompositeSubKey, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set bookingRestrictionKeys value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\CompositeSubKey[] $bookingRestrictionKeys
     * @return \Booking\StructType\RemoveBookingRestrictionParameters
     */
    public function setBookingRestrictionKeys(?array $bookingRestrictionKeys = null): self
    {
        // validation for constraint: array
        if ('' !== ($bookingRestrictionKeysArrayErrorMessage = self::validateBookingRestrictionKeysForArrayConstraintFromSetBookingRestrictionKeys($bookingRestrictionKeys))) {
            throw new InvalidArgumentException($bookingRestrictionKeysArrayErrorMessage, __LINE__);
        }
        $this->bookingRestrictionKeys = $bookingRestrictionKeys;
        
        return $this;
    }
    /**
     * Add item to bookingRestrictionKeys value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\CompositeSubKey $item
     * @return \Booking\StructType\RemoveBookingRestrictionParameters
     */
    public function addToBookingRestrictionKeys(\Booking\StructType\CompositeSubKey $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\CompositeSubKey) {
            throw new InvalidArgumentException(sprintf('The bookingRestrictionKeys property can only contain items of type \Booking\StructType\CompositeSubKey, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->bookingRestrictionKeys[] = $item;
        
        return $this;
    }
}
