<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for participation StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Participation extends AbstractStructBase
{
    /**
     * The booking
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\Booking|null
     */
    protected ?\Booking\StructType\Booking $booking = null;
    /**
     * The canShowUp
     * @var bool|null
     */
    protected ?bool $canShowUp = null;
    /**
     * The energyConsumptionKcal
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $energyConsumptionKcal = null;
    /**
     * The expiryTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $expiryTime = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The ownerId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $ownerId = null;
    /**
     * The ownerName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ownerName = null;
    /**
     * The participationId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $participationId = null;
    /**
     * The participationListIndex
     * @var int|null
     */
    protected ?int $participationListIndex = null;
    /**
     * The personId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $personId = null;
    /**
     * The personName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $personName = null;
    /**
     * The qrCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\MimeDocument|null
     */
    protected ?\Booking\StructType\MimeDocument $qrCode = null;
    /**
     * The seat
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\Seat|null
     */
    protected ?\Booking\StructType\Seat $seat = null;
    /**
     * The state
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $state = null;
    /**
     * The usedOwnerPrivilege
     * @var bool|null
     */
    protected ?bool $usedOwnerPrivilege = null;
    /**
     * The waitingListIndex
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $waitingListIndex = null;
    /**
     * Constructor method for participation
     * @uses Participation::setBooking()
     * @uses Participation::setCanShowUp()
     * @uses Participation::setEnergyConsumptionKcal()
     * @uses Participation::setExpiryTime()
     * @uses Participation::setExternalId()
     * @uses Participation::setOwnerId()
     * @uses Participation::setOwnerName()
     * @uses Participation::setParticipationId()
     * @uses Participation::setParticipationListIndex()
     * @uses Participation::setPersonId()
     * @uses Participation::setPersonName()
     * @uses Participation::setQrCode()
     * @uses Participation::setSeat()
     * @uses Participation::setState()
     * @uses Participation::setUsedOwnerPrivilege()
     * @uses Participation::setWaitingListIndex()
     * @param \Booking\StructType\Booking $booking
     * @param bool $canShowUp
     * @param float $energyConsumptionKcal
     * @param string $expiryTime
     * @param string $externalId
     * @param \Booking\StructType\ApiPersonKey $ownerId
     * @param string $ownerName
     * @param \Booking\StructType\CompositeKey $participationId
     * @param int $participationListIndex
     * @param \Booking\StructType\ApiPersonKey $personId
     * @param string $personName
     * @param \Booking\StructType\MimeDocument $qrCode
     * @param \Booking\StructType\Seat $seat
     * @param string $state
     * @param bool $usedOwnerPrivilege
     * @param int $waitingListIndex
     */
    public function __construct(?\Booking\StructType\Booking $booking = null, ?bool $canShowUp = null, ?float $energyConsumptionKcal = null, ?string $expiryTime = null, ?string $externalId = null, ?\Booking\StructType\ApiPersonKey $ownerId = null, ?string $ownerName = null, ?\Booking\StructType\CompositeKey $participationId = null, ?int $participationListIndex = null, ?\Booking\StructType\ApiPersonKey $personId = null, ?string $personName = null, ?\Booking\StructType\MimeDocument $qrCode = null, ?\Booking\StructType\Seat $seat = null, ?string $state = null, ?bool $usedOwnerPrivilege = null, ?int $waitingListIndex = null)
    {
        $this
            ->setBooking($booking)
            ->setCanShowUp($canShowUp)
            ->setEnergyConsumptionKcal($energyConsumptionKcal)
            ->setExpiryTime($expiryTime)
            ->setExternalId($externalId)
            ->setOwnerId($ownerId)
            ->setOwnerName($ownerName)
            ->setParticipationId($participationId)
            ->setParticipationListIndex($participationListIndex)
            ->setPersonId($personId)
            ->setPersonName($personName)
            ->setQrCode($qrCode)
            ->setSeat($seat)
            ->setState($state)
            ->setUsedOwnerPrivilege($usedOwnerPrivilege)
            ->setWaitingListIndex($waitingListIndex);
    }
    /**
     * Get booking value
     * @return \Booking\StructType\Booking|null
     */
    public function getBooking(): ?\Booking\StructType\Booking
    {
        return $this->booking;
    }
    /**
     * Set booking value
     * @param \Booking\StructType\Booking $booking
     * @return \Booking\StructType\Participation
     */
    public function setBooking(?\Booking\StructType\Booking $booking = null): self
    {
        $this->booking = $booking;
        
        return $this;
    }
    /**
     * Get canShowUp value
     * @return bool|null
     */
    public function getCanShowUp(): ?bool
    {
        return $this->canShowUp;
    }
    /**
     * Set canShowUp value
     * @param bool $canShowUp
     * @return \Booking\StructType\Participation
     */
    public function setCanShowUp(?bool $canShowUp = null): self
    {
        // validation for constraint: boolean
        if (!is_null($canShowUp) && !is_bool($canShowUp)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($canShowUp, true), gettype($canShowUp)), __LINE__);
        }
        $this->canShowUp = $canShowUp;
        
        return $this;
    }
    /**
     * Get energyConsumptionKcal value
     * @return float|null
     */
    public function getEnergyConsumptionKcal(): ?float
    {
        return $this->energyConsumptionKcal;
    }
    /**
     * Set energyConsumptionKcal value
     * @param float $energyConsumptionKcal
     * @return \Booking\StructType\Participation
     */
    public function setEnergyConsumptionKcal(?float $energyConsumptionKcal = null): self
    {
        // validation for constraint: float
        if (!is_null($energyConsumptionKcal) && !(is_float($energyConsumptionKcal) || is_numeric($energyConsumptionKcal))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($energyConsumptionKcal, true), gettype($energyConsumptionKcal)), __LINE__);
        }
        $this->energyConsumptionKcal = $energyConsumptionKcal;
        
        return $this;
    }
    /**
     * Get expiryTime value
     * @return string|null
     */
    public function getExpiryTime(): ?string
    {
        return $this->expiryTime;
    }
    /**
     * Set expiryTime value
     * @param string $expiryTime
     * @return \Booking\StructType\Participation
     */
    public function setExpiryTime(?string $expiryTime = null): self
    {
        // validation for constraint: string
        if (!is_null($expiryTime) && !is_string($expiryTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expiryTime, true), gettype($expiryTime)), __LINE__);
        }
        $this->expiryTime = $expiryTime;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Booking\StructType\Participation
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get ownerId value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getOwnerId(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->ownerId;
    }
    /**
     * Set ownerId value
     * @param \Booking\StructType\ApiPersonKey $ownerId
     * @return \Booking\StructType\Participation
     */
    public function setOwnerId(?\Booking\StructType\ApiPersonKey $ownerId = null): self
    {
        $this->ownerId = $ownerId;
        
        return $this;
    }
    /**
     * Get ownerName value
     * @return string|null
     */
    public function getOwnerName(): ?string
    {
        return $this->ownerName;
    }
    /**
     * Set ownerName value
     * @param string $ownerName
     * @return \Booking\StructType\Participation
     */
    public function setOwnerName(?string $ownerName = null): self
    {
        // validation for constraint: string
        if (!is_null($ownerName) && !is_string($ownerName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ownerName, true), gettype($ownerName)), __LINE__);
        }
        $this->ownerName = $ownerName;
        
        return $this;
    }
    /**
     * Get participationId value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getParticipationId(): ?\Booking\StructType\CompositeKey
    {
        return $this->participationId;
    }
    /**
     * Set participationId value
     * @param \Booking\StructType\CompositeKey $participationId
     * @return \Booking\StructType\Participation
     */
    public function setParticipationId(?\Booking\StructType\CompositeKey $participationId = null): self
    {
        $this->participationId = $participationId;
        
        return $this;
    }
    /**
     * Get participationListIndex value
     * @return int|null
     */
    public function getParticipationListIndex(): ?int
    {
        return $this->participationListIndex;
    }
    /**
     * Set participationListIndex value
     * @param int $participationListIndex
     * @return \Booking\StructType\Participation
     */
    public function setParticipationListIndex(?int $participationListIndex = null): self
    {
        // validation for constraint: int
        if (!is_null($participationListIndex) && !(is_int($participationListIndex) || ctype_digit($participationListIndex))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($participationListIndex, true), gettype($participationListIndex)), __LINE__);
        }
        $this->participationListIndex = $participationListIndex;
        
        return $this;
    }
    /**
     * Get personId value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getPersonId(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->personId;
    }
    /**
     * Set personId value
     * @param \Booking\StructType\ApiPersonKey $personId
     * @return \Booking\StructType\Participation
     */
    public function setPersonId(?\Booking\StructType\ApiPersonKey $personId = null): self
    {
        $this->personId = $personId;
        
        return $this;
    }
    /**
     * Get personName value
     * @return string|null
     */
    public function getPersonName(): ?string
    {
        return $this->personName;
    }
    /**
     * Set personName value
     * @param string $personName
     * @return \Booking\StructType\Participation
     */
    public function setPersonName(?string $personName = null): self
    {
        // validation for constraint: string
        if (!is_null($personName) && !is_string($personName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($personName, true), gettype($personName)), __LINE__);
        }
        $this->personName = $personName;
        
        return $this;
    }
    /**
     * Get qrCode value
     * @return \Booking\StructType\MimeDocument|null
     */
    public function getQrCode(): ?\Booking\StructType\MimeDocument
    {
        return $this->qrCode;
    }
    /**
     * Set qrCode value
     * @param \Booking\StructType\MimeDocument $qrCode
     * @return \Booking\StructType\Participation
     */
    public function setQrCode(?\Booking\StructType\MimeDocument $qrCode = null): self
    {
        $this->qrCode = $qrCode;
        
        return $this;
    }
    /**
     * Get seat value
     * @return \Booking\StructType\Seat|null
     */
    public function getSeat(): ?\Booking\StructType\Seat
    {
        return $this->seat;
    }
    /**
     * Set seat value
     * @param \Booking\StructType\Seat $seat
     * @return \Booking\StructType\Participation
     */
    public function setSeat(?\Booking\StructType\Seat $seat = null): self
    {
        $this->seat = $seat;
        
        return $this;
    }
    /**
     * Get state value
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }
    /**
     * Set state value
     * @uses \Booking\EnumType\ParticipationState::valueIsValid()
     * @uses \Booking\EnumType\ParticipationState::getValidValues()
     * @throws InvalidArgumentException
     * @param string $state
     * @return \Booking\StructType\Participation
     */
    public function setState(?string $state = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\ParticipationState::valueIsValid($state)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\ParticipationState', is_array($state) ? implode(', ', $state) : var_export($state, true), implode(', ', \Booking\EnumType\ParticipationState::getValidValues())), __LINE__);
        }
        $this->state = $state;
        
        return $this;
    }
    /**
     * Get usedOwnerPrivilege value
     * @return bool|null
     */
    public function getUsedOwnerPrivilege(): ?bool
    {
        return $this->usedOwnerPrivilege;
    }
    /**
     * Set usedOwnerPrivilege value
     * @param bool $usedOwnerPrivilege
     * @return \Booking\StructType\Participation
     */
    public function setUsedOwnerPrivilege(?bool $usedOwnerPrivilege = null): self
    {
        // validation for constraint: boolean
        if (!is_null($usedOwnerPrivilege) && !is_bool($usedOwnerPrivilege)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($usedOwnerPrivilege, true), gettype($usedOwnerPrivilege)), __LINE__);
        }
        $this->usedOwnerPrivilege = $usedOwnerPrivilege;
        
        return $this;
    }
    /**
     * Get waitingListIndex value
     * @return int|null
     */
    public function getWaitingListIndex(): ?int
    {
        return $this->waitingListIndex;
    }
    /**
     * Set waitingListIndex value
     * @param int $waitingListIndex
     * @return \Booking\StructType\Participation
     */
    public function setWaitingListIndex(?int $waitingListIndex = null): self
    {
        // validation for constraint: int
        if (!is_null($waitingListIndex) && !(is_int($waitingListIndex) || ctype_digit($waitingListIndex))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($waitingListIndex, true), gettype($waitingListIndex)), __LINE__);
        }
        $this->waitingListIndex = $waitingListIndex;
        
        return $this;
    }
}
