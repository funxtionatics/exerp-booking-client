<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for colorGroup StructType
 * Meta information extracted from the WSDL
 * - final: extension restriction
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class ColorGroup extends AbstractStructBase
{
    /**
     * The colorGroupId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $colorGroupId = null;
    /**
     * The hexColor
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $hexColor = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * Constructor method for colorGroup
     * @uses ColorGroup::setColorGroupId()
     * @uses ColorGroup::setHexColor()
     * @uses ColorGroup::setName()
     * @param int $colorGroupId
     * @param string $hexColor
     * @param string $name
     */
    public function __construct(?int $colorGroupId = null, ?string $hexColor = null, ?string $name = null)
    {
        $this
            ->setColorGroupId($colorGroupId)
            ->setHexColor($hexColor)
            ->setName($name);
    }
    /**
     * Get colorGroupId value
     * @return int|null
     */
    public function getColorGroupId(): ?int
    {
        return $this->colorGroupId;
    }
    /**
     * Set colorGroupId value
     * @param int $colorGroupId
     * @return \Booking\StructType\ColorGroup
     */
    public function setColorGroupId(?int $colorGroupId = null): self
    {
        // validation for constraint: int
        if (!is_null($colorGroupId) && !(is_int($colorGroupId) || ctype_digit($colorGroupId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($colorGroupId, true), gettype($colorGroupId)), __LINE__);
        }
        $this->colorGroupId = $colorGroupId;
        
        return $this;
    }
    /**
     * Get hexColor value
     * @return string|null
     */
    public function getHexColor(): ?string
    {
        return $this->hexColor;
    }
    /**
     * Set hexColor value
     * @param string $hexColor
     * @return \Booking\StructType\ColorGroup
     */
    public function setHexColor(?string $hexColor = null): self
    {
        // validation for constraint: string
        if (!is_null($hexColor) && !is_string($hexColor)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hexColor, true), gettype($hexColor)), __LINE__);
        }
        $this->hexColor = $hexColor;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\ColorGroup
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
}
