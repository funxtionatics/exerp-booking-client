<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getProductsForBookingResult StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class GetProductsForBookingResult extends AbstractStructBase
{
    /**
     * The products
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\Products|null
     */
    protected ?\Booking\StructType\Products $products = null;
    /**
     * Constructor method for getProductsForBookingResult
     * @uses GetProductsForBookingResult::setProducts()
     * @param \Booking\StructType\Products $products
     */
    public function __construct(?\Booking\StructType\Products $products = null)
    {
        $this
            ->setProducts($products);
    }
    /**
     * Get products value
     * @return \Booking\StructType\Products|null
     */
    public function getProducts(): ?\Booking\StructType\Products
    {
        return $this->products;
    }
    /**
     * Set products value
     * @param \Booking\StructType\Products $products
     * @return \Booking\StructType\GetProductsForBookingResult
     */
    public function setProducts(?\Booking\StructType\Products $products = null): self
    {
        $this->products = $products;
        
        return $this;
    }
}
