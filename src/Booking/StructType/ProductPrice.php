<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for productPrice StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class ProductPrice extends AbstractStructBase
{
    /**
     * The customerPrice
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $customerPrice = null;
    /**
     * The normalPrice
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $normalPrice = null;
    /**
     * The sponsorPrice
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $sponsorPrice = null;
    /**
     * Constructor method for productPrice
     * @uses ProductPrice::setCustomerPrice()
     * @uses ProductPrice::setNormalPrice()
     * @uses ProductPrice::setSponsorPrice()
     * @param string $customerPrice
     * @param string $normalPrice
     * @param string $sponsorPrice
     */
    public function __construct(?string $customerPrice = null, ?string $normalPrice = null, ?string $sponsorPrice = null)
    {
        $this
            ->setCustomerPrice($customerPrice)
            ->setNormalPrice($normalPrice)
            ->setSponsorPrice($sponsorPrice);
    }
    /**
     * Get customerPrice value
     * @return string|null
     */
    public function getCustomerPrice(): ?string
    {
        return $this->customerPrice;
    }
    /**
     * Set customerPrice value
     * @param string $customerPrice
     * @return \Booking\StructType\ProductPrice
     */
    public function setCustomerPrice(?string $customerPrice = null): self
    {
        // validation for constraint: string
        if (!is_null($customerPrice) && !is_string($customerPrice)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($customerPrice, true), gettype($customerPrice)), __LINE__);
        }
        $this->customerPrice = $customerPrice;
        
        return $this;
    }
    /**
     * Get normalPrice value
     * @return string|null
     */
    public function getNormalPrice(): ?string
    {
        return $this->normalPrice;
    }
    /**
     * Set normalPrice value
     * @param string $normalPrice
     * @return \Booking\StructType\ProductPrice
     */
    public function setNormalPrice(?string $normalPrice = null): self
    {
        // validation for constraint: string
        if (!is_null($normalPrice) && !is_string($normalPrice)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($normalPrice, true), gettype($normalPrice)), __LINE__);
        }
        $this->normalPrice = $normalPrice;
        
        return $this;
    }
    /**
     * Get sponsorPrice value
     * @return string|null
     */
    public function getSponsorPrice(): ?string
    {
        return $this->sponsorPrice;
    }
    /**
     * Set sponsorPrice value
     * @param string $sponsorPrice
     * @return \Booking\StructType\ProductPrice
     */
    public function setSponsorPrice(?string $sponsorPrice = null): self
    {
        // validation for constraint: string
        if (!is_null($sponsorPrice) && !is_string($sponsorPrice)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sponsorPrice, true), gettype($sponsorPrice)), __LINE__);
        }
        $this->sponsorPrice = $sponsorPrice;
        
        return $this;
    }
}
