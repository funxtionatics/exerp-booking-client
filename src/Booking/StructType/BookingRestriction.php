<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for bookingRestriction StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class BookingRestriction extends AbstractStructBase
{
    /**
     * The bookingRestrictionKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CompositeSubKey|null
     */
    protected ?\Booking\StructType\CompositeSubKey $bookingRestrictionKey = null;
    /**
     * The userInterfaceType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $userInterfaceType = null;
    /**
     * The dateFrom
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $dateFrom = null;
    /**
     * The dateTo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $dateTo = null;
    /**
     * The accessGroupKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $accessGroupKey = null;
    /**
     * Constructor method for bookingRestriction
     * @uses BookingRestriction::setBookingRestrictionKey()
     * @uses BookingRestriction::setUserInterfaceType()
     * @uses BookingRestriction::setDateFrom()
     * @uses BookingRestriction::setDateTo()
     * @uses BookingRestriction::setAccessGroupKey()
     * @param \Booking\StructType\CompositeSubKey $bookingRestrictionKey
     * @param string $userInterfaceType
     * @param string $dateFrom
     * @param string $dateTo
     * @param int $accessGroupKey
     */
    public function __construct(?\Booking\StructType\CompositeSubKey $bookingRestrictionKey = null, ?string $userInterfaceType = null, ?string $dateFrom = null, ?string $dateTo = null, ?int $accessGroupKey = null)
    {
        $this
            ->setBookingRestrictionKey($bookingRestrictionKey)
            ->setUserInterfaceType($userInterfaceType)
            ->setDateFrom($dateFrom)
            ->setDateTo($dateTo)
            ->setAccessGroupKey($accessGroupKey);
    }
    /**
     * Get bookingRestrictionKey value
     * @return \Booking\StructType\CompositeSubKey|null
     */
    public function getBookingRestrictionKey(): ?\Booking\StructType\CompositeSubKey
    {
        return $this->bookingRestrictionKey;
    }
    /**
     * Set bookingRestrictionKey value
     * @param \Booking\StructType\CompositeSubKey $bookingRestrictionKey
     * @return \Booking\StructType\BookingRestriction
     */
    public function setBookingRestrictionKey(?\Booking\StructType\CompositeSubKey $bookingRestrictionKey = null): self
    {
        $this->bookingRestrictionKey = $bookingRestrictionKey;
        
        return $this;
    }
    /**
     * Get userInterfaceType value
     * @return string|null
     */
    public function getUserInterfaceType(): ?string
    {
        return $this->userInterfaceType;
    }
    /**
     * Set userInterfaceType value
     * @uses \Booking\EnumType\UserInterfaceType::valueIsValid()
     * @uses \Booking\EnumType\UserInterfaceType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $userInterfaceType
     * @return \Booking\StructType\BookingRestriction
     */
    public function setUserInterfaceType(?string $userInterfaceType = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\UserInterfaceType::valueIsValid($userInterfaceType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\UserInterfaceType', is_array($userInterfaceType) ? implode(', ', $userInterfaceType) : var_export($userInterfaceType, true), implode(', ', \Booking\EnumType\UserInterfaceType::getValidValues())), __LINE__);
        }
        $this->userInterfaceType = $userInterfaceType;
        
        return $this;
    }
    /**
     * Get dateFrom value
     * @return string|null
     */
    public function getDateFrom(): ?string
    {
        return $this->dateFrom;
    }
    /**
     * Set dateFrom value
     * @param string $dateFrom
     * @return \Booking\StructType\BookingRestriction
     */
    public function setDateFrom(?string $dateFrom = null): self
    {
        // validation for constraint: string
        if (!is_null($dateFrom) && !is_string($dateFrom)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateFrom, true), gettype($dateFrom)), __LINE__);
        }
        $this->dateFrom = $dateFrom;
        
        return $this;
    }
    /**
     * Get dateTo value
     * @return string|null
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }
    /**
     * Set dateTo value
     * @param string $dateTo
     * @return \Booking\StructType\BookingRestriction
     */
    public function setDateTo(?string $dateTo = null): self
    {
        // validation for constraint: string
        if (!is_null($dateTo) && !is_string($dateTo)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateTo, true), gettype($dateTo)), __LINE__);
        }
        $this->dateTo = $dateTo;
        
        return $this;
    }
    /**
     * Get accessGroupKey value
     * @return int|null
     */
    public function getAccessGroupKey(): ?int
    {
        return $this->accessGroupKey;
    }
    /**
     * Set accessGroupKey value
     * @param int $accessGroupKey
     * @return \Booking\StructType\BookingRestriction
     */
    public function setAccessGroupKey(?int $accessGroupKey = null): self
    {
        // validation for constraint: int
        if (!is_null($accessGroupKey) && !(is_int($accessGroupKey) || ctype_digit($accessGroupKey))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($accessGroupKey, true), gettype($accessGroupKey)), __LINE__);
        }
        $this->accessGroupKey = $accessGroupKey;
        
        return $this;
    }
}
