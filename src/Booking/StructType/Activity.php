<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for activity StructType
 * Meta information extracted from the WSDL
 * - final: extension restriction
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Activity extends AbstractStructBase
{
    /**
     * The activityId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $activityId = null;
    /**
     * The activityType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $activityType = null;
    /**
     * The color
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $color = null;
    /**
     * The colorGroup
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ColorGroup|null
     */
    protected ?\Booking\StructType\ColorGroup $colorGroup = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The energyConsumptionKcalPerHour
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $energyConsumptionKcalPerHour = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The group
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ActivityGroup|null
     */
    protected ?\Booking\StructType\ActivityGroup $group = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The seatBooking
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $seatBooking = null;
    /**
     * The waitingListCapacity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $waitingListCapacity = null;
    /**
     * Constructor method for activity
     * @uses Activity::setActivityId()
     * @uses Activity::setActivityType()
     * @uses Activity::setColor()
     * @uses Activity::setColorGroup()
     * @uses Activity::setDescription()
     * @uses Activity::setEnergyConsumptionKcalPerHour()
     * @uses Activity::setExternalId()
     * @uses Activity::setGroup()
     * @uses Activity::setName()
     * @uses Activity::setSeatBooking()
     * @uses Activity::setWaitingListCapacity()
     * @param int $activityId
     * @param string $activityType
     * @param string $color
     * @param \Booking\StructType\ColorGroup $colorGroup
     * @param string $description
     * @param int $energyConsumptionKcalPerHour
     * @param string $externalId
     * @param \Booking\StructType\ActivityGroup $group
     * @param string $name
     * @param string $seatBooking
     * @param int $waitingListCapacity
     */
    public function __construct(?int $activityId = null, ?string $activityType = null, ?string $color = null, ?\Booking\StructType\ColorGroup $colorGroup = null, ?string $description = null, ?int $energyConsumptionKcalPerHour = null, ?string $externalId = null, ?\Booking\StructType\ActivityGroup $group = null, ?string $name = null, ?string $seatBooking = null, ?int $waitingListCapacity = null)
    {
        $this
            ->setActivityId($activityId)
            ->setActivityType($activityType)
            ->setColor($color)
            ->setColorGroup($colorGroup)
            ->setDescription($description)
            ->setEnergyConsumptionKcalPerHour($energyConsumptionKcalPerHour)
            ->setExternalId($externalId)
            ->setGroup($group)
            ->setName($name)
            ->setSeatBooking($seatBooking)
            ->setWaitingListCapacity($waitingListCapacity);
    }
    /**
     * Get activityId value
     * @return int|null
     */
    public function getActivityId(): ?int
    {
        return $this->activityId;
    }
    /**
     * Set activityId value
     * @param int $activityId
     * @return \Booking\StructType\Activity
     */
    public function setActivityId(?int $activityId = null): self
    {
        // validation for constraint: int
        if (!is_null($activityId) && !(is_int($activityId) || ctype_digit($activityId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($activityId, true), gettype($activityId)), __LINE__);
        }
        $this->activityId = $activityId;
        
        return $this;
    }
    /**
     * Get activityType value
     * @return string|null
     */
    public function getActivityType(): ?string
    {
        return $this->activityType;
    }
    /**
     * Set activityType value
     * @uses \Booking\EnumType\ActivityType::valueIsValid()
     * @uses \Booking\EnumType\ActivityType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $activityType
     * @return \Booking\StructType\Activity
     */
    public function setActivityType(?string $activityType = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\ActivityType::valueIsValid($activityType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\ActivityType', is_array($activityType) ? implode(', ', $activityType) : var_export($activityType, true), implode(', ', \Booking\EnumType\ActivityType::getValidValues())), __LINE__);
        }
        $this->activityType = $activityType;
        
        return $this;
    }
    /**
     * Get color value
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }
    /**
     * Set color value
     * @uses \Booking\EnumType\ActivityColor::valueIsValid()
     * @uses \Booking\EnumType\ActivityColor::getValidValues()
     * @throws InvalidArgumentException
     * @param string $color
     * @return \Booking\StructType\Activity
     */
    public function setColor(?string $color = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\ActivityColor::valueIsValid($color)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\ActivityColor', is_array($color) ? implode(', ', $color) : var_export($color, true), implode(', ', \Booking\EnumType\ActivityColor::getValidValues())), __LINE__);
        }
        $this->color = $color;
        
        return $this;
    }
    /**
     * Get colorGroup value
     * @return \Booking\StructType\ColorGroup|null
     */
    public function getColorGroup(): ?\Booking\StructType\ColorGroup
    {
        return $this->colorGroup;
    }
    /**
     * Set colorGroup value
     * @param \Booking\StructType\ColorGroup $colorGroup
     * @return \Booking\StructType\Activity
     */
    public function setColorGroup(?\Booking\StructType\ColorGroup $colorGroup = null): self
    {
        $this->colorGroup = $colorGroup;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \Booking\StructType\Activity
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get energyConsumptionKcalPerHour value
     * @return int|null
     */
    public function getEnergyConsumptionKcalPerHour(): ?int
    {
        return $this->energyConsumptionKcalPerHour;
    }
    /**
     * Set energyConsumptionKcalPerHour value
     * @param int $energyConsumptionKcalPerHour
     * @return \Booking\StructType\Activity
     */
    public function setEnergyConsumptionKcalPerHour(?int $energyConsumptionKcalPerHour = null): self
    {
        // validation for constraint: int
        if (!is_null($energyConsumptionKcalPerHour) && !(is_int($energyConsumptionKcalPerHour) || ctype_digit($energyConsumptionKcalPerHour))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($energyConsumptionKcalPerHour, true), gettype($energyConsumptionKcalPerHour)), __LINE__);
        }
        $this->energyConsumptionKcalPerHour = $energyConsumptionKcalPerHour;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Booking\StructType\Activity
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get group value
     * @return \Booking\StructType\ActivityGroup|null
     */
    public function getGroup(): ?\Booking\StructType\ActivityGroup
    {
        return $this->group;
    }
    /**
     * Set group value
     * @param \Booking\StructType\ActivityGroup $group
     * @return \Booking\StructType\Activity
     */
    public function setGroup(?\Booking\StructType\ActivityGroup $group = null): self
    {
        $this->group = $group;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\Activity
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get seatBooking value
     * @return string|null
     */
    public function getSeatBooking(): ?string
    {
        return $this->seatBooking;
    }
    /**
     * Set seatBooking value
     * @uses \Booking\EnumType\SeatBookingType::valueIsValid()
     * @uses \Booking\EnumType\SeatBookingType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $seatBooking
     * @return \Booking\StructType\Activity
     */
    public function setSeatBooking(?string $seatBooking = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\SeatBookingType::valueIsValid($seatBooking)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\SeatBookingType', is_array($seatBooking) ? implode(', ', $seatBooking) : var_export($seatBooking, true), implode(', ', \Booking\EnumType\SeatBookingType::getValidValues())), __LINE__);
        }
        $this->seatBooking = $seatBooking;
        
        return $this;
    }
    /**
     * Get waitingListCapacity value
     * @return int|null
     */
    public function getWaitingListCapacity(): ?int
    {
        return $this->waitingListCapacity;
    }
    /**
     * Set waitingListCapacity value
     * @param int $waitingListCapacity
     * @return \Booking\StructType\Activity
     */
    public function setWaitingListCapacity(?int $waitingListCapacity = null): self
    {
        // validation for constraint: int
        if (!is_null($waitingListCapacity) && !(is_int($waitingListCapacity) || ctype_digit($waitingListCapacity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($waitingListCapacity, true), gettype($waitingListCapacity)), __LINE__);
        }
        $this->waitingListCapacity = $waitingListCapacity;
        
        return $this;
    }
}
