<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for removeBookingRestrictionResponse StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class RemoveBookingRestrictionResponse extends AbstractStructBase
{
}
