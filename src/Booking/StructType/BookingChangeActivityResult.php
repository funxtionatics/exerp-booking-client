<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for bookingChangeActivityResult StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class BookingChangeActivityResult extends AbstractStructBase
{
    /**
     * The booking
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\Booking|null
     */
    protected ?\Booking\StructType\Booking $booking = null;
    /**
     * The cancelledParticipations
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\ApiPersonKey[]
     */
    protected ?array $cancelledParticipations = null;
    /**
     * The movedParticipations
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\BookingChangeActivityParticipation[]
     */
    protected ?array $movedParticipations = null;
    /**
     * Constructor method for bookingChangeActivityResult
     * @uses BookingChangeActivityResult::setBooking()
     * @uses BookingChangeActivityResult::setCancelledParticipations()
     * @uses BookingChangeActivityResult::setMovedParticipations()
     * @param \Booking\StructType\Booking $booking
     * @param \Booking\StructType\ApiPersonKey[] $cancelledParticipations
     * @param \Booking\StructType\BookingChangeActivityParticipation[] $movedParticipations
     */
    public function __construct(?\Booking\StructType\Booking $booking = null, ?array $cancelledParticipations = null, ?array $movedParticipations = null)
    {
        $this
            ->setBooking($booking)
            ->setCancelledParticipations($cancelledParticipations)
            ->setMovedParticipations($movedParticipations);
    }
    /**
     * Get booking value
     * @return \Booking\StructType\Booking|null
     */
    public function getBooking(): ?\Booking\StructType\Booking
    {
        return $this->booking;
    }
    /**
     * Set booking value
     * @param \Booking\StructType\Booking $booking
     * @return \Booking\StructType\BookingChangeActivityResult
     */
    public function setBooking(?\Booking\StructType\Booking $booking = null): self
    {
        $this->booking = $booking;
        
        return $this;
    }
    /**
     * Get cancelledParticipations value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\ApiPersonKey[]
     */
    public function getCancelledParticipations(): ?array
    {
        return $this->cancelledParticipations ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setCancelledParticipations method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCancelledParticipations method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCancelledParticipationsForArrayConstraintFromSetCancelledParticipations(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $bookingChangeActivityResultCancelledParticipationsItem) {
            // validation for constraint: itemType
            if (!$bookingChangeActivityResultCancelledParticipationsItem instanceof \Booking\StructType\ApiPersonKey) {
                $invalidValues[] = is_object($bookingChangeActivityResultCancelledParticipationsItem) ? get_class($bookingChangeActivityResultCancelledParticipationsItem) : sprintf('%s(%s)', gettype($bookingChangeActivityResultCancelledParticipationsItem), var_export($bookingChangeActivityResultCancelledParticipationsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The cancelledParticipations property can only contain items of type \Booking\StructType\ApiPersonKey, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set cancelledParticipations value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\ApiPersonKey[] $cancelledParticipations
     * @return \Booking\StructType\BookingChangeActivityResult
     */
    public function setCancelledParticipations(?array $cancelledParticipations = null): self
    {
        // validation for constraint: array
        if ('' !== ($cancelledParticipationsArrayErrorMessage = self::validateCancelledParticipationsForArrayConstraintFromSetCancelledParticipations($cancelledParticipations))) {
            throw new InvalidArgumentException($cancelledParticipationsArrayErrorMessage, __LINE__);
        }
        if (is_null($cancelledParticipations) || (is_array($cancelledParticipations) && empty($cancelledParticipations))) {
            unset($this->cancelledParticipations);
        } else {
            $this->cancelledParticipations = $cancelledParticipations;
        }
        
        return $this;
    }
    /**
     * Add item to cancelledParticipations value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\ApiPersonKey $item
     * @return \Booking\StructType\BookingChangeActivityResult
     */
    public function addToCancelledParticipations(\Booking\StructType\ApiPersonKey $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\ApiPersonKey) {
            throw new InvalidArgumentException(sprintf('The cancelledParticipations property can only contain items of type \Booking\StructType\ApiPersonKey, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->cancelledParticipations[] = $item;
        
        return $this;
    }
    /**
     * Get movedParticipations value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\BookingChangeActivityParticipation[]
     */
    public function getMovedParticipations(): ?array
    {
        return $this->movedParticipations ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setMovedParticipations method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMovedParticipations method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMovedParticipationsForArrayConstraintFromSetMovedParticipations(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $bookingChangeActivityResultMovedParticipationsItem) {
            // validation for constraint: itemType
            if (!$bookingChangeActivityResultMovedParticipationsItem instanceof \Booking\StructType\BookingChangeActivityParticipation) {
                $invalidValues[] = is_object($bookingChangeActivityResultMovedParticipationsItem) ? get_class($bookingChangeActivityResultMovedParticipationsItem) : sprintf('%s(%s)', gettype($bookingChangeActivityResultMovedParticipationsItem), var_export($bookingChangeActivityResultMovedParticipationsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The movedParticipations property can only contain items of type \Booking\StructType\BookingChangeActivityParticipation, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set movedParticipations value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\BookingChangeActivityParticipation[] $movedParticipations
     * @return \Booking\StructType\BookingChangeActivityResult
     */
    public function setMovedParticipations(?array $movedParticipations = null): self
    {
        // validation for constraint: array
        if ('' !== ($movedParticipationsArrayErrorMessage = self::validateMovedParticipationsForArrayConstraintFromSetMovedParticipations($movedParticipations))) {
            throw new InvalidArgumentException($movedParticipationsArrayErrorMessage, __LINE__);
        }
        if (is_null($movedParticipations) || (is_array($movedParticipations) && empty($movedParticipations))) {
            unset($this->movedParticipations);
        } else {
            $this->movedParticipations = $movedParticipations;
        }
        
        return $this;
    }
    /**
     * Add item to movedParticipations value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\BookingChangeActivityParticipation $item
     * @return \Booking\StructType\BookingChangeActivityResult
     */
    public function addToMovedParticipations(\Booking\StructType\BookingChangeActivityParticipation $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\BookingChangeActivityParticipation) {
            throw new InvalidArgumentException(sprintf('The movedParticipations property can only contain items of type \Booking\StructType\BookingChangeActivityParticipation, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->movedParticipations[] = $item;
        
        return $this;
    }
}
