<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getAvailableAgeGroupsResult StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class GetAvailableAgeGroupsResult extends AbstractStructBase
{
    /**
     * The ageGroups
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\AgeGroups|null
     */
    protected ?\Booking\StructType\AgeGroups $ageGroups = null;
    /**
     * Constructor method for getAvailableAgeGroupsResult
     * @uses GetAvailableAgeGroupsResult::setAgeGroups()
     * @param \Booking\StructType\AgeGroups $ageGroups
     */
    public function __construct(?\Booking\StructType\AgeGroups $ageGroups = null)
    {
        $this
            ->setAgeGroups($ageGroups);
    }
    /**
     * Get ageGroups value
     * @return \Booking\StructType\AgeGroups|null
     */
    public function getAgeGroups(): ?\Booking\StructType\AgeGroups
    {
        return $this->ageGroups;
    }
    /**
     * Set ageGroups value
     * @param \Booking\StructType\AgeGroups $ageGroups
     * @return \Booking\StructType\GetAvailableAgeGroupsResult
     */
    public function setAgeGroups(?\Booking\StructType\AgeGroups $ageGroups = null): self
    {
        $this->ageGroups = $ageGroups;
        
        return $this;
    }
}
