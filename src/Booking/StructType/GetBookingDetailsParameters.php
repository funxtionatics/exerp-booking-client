<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getBookingDetailsParameters StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class GetBookingDetailsParameters extends AbstractStructBase
{
    /**
     * The bookingKey
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $bookingKey = null;
    /**
     * The includeParticipations
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $includeParticipations = null;
    /**
     * The personKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $personKey = null;
    /**
     * Constructor method for getBookingDetailsParameters
     * @uses GetBookingDetailsParameters::setBookingKey()
     * @uses GetBookingDetailsParameters::setIncludeParticipations()
     * @uses GetBookingDetailsParameters::setPersonKey()
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @param bool $includeParticipations
     * @param \Booking\StructType\ApiPersonKey $personKey
     */
    public function __construct(?\Booking\StructType\CompositeKey $bookingKey = null, ?bool $includeParticipations = null, ?\Booking\StructType\ApiPersonKey $personKey = null)
    {
        $this
            ->setBookingKey($bookingKey)
            ->setIncludeParticipations($includeParticipations)
            ->setPersonKey($personKey);
    }
    /**
     * Get bookingKey value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getBookingKey(): ?\Booking\StructType\CompositeKey
    {
        return $this->bookingKey;
    }
    /**
     * Set bookingKey value
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @return \Booking\StructType\GetBookingDetailsParameters
     */
    public function setBookingKey(?\Booking\StructType\CompositeKey $bookingKey = null): self
    {
        $this->bookingKey = $bookingKey;
        
        return $this;
    }
    /**
     * Get includeParticipations value
     * @return bool|null
     */
    public function getIncludeParticipations(): ?bool
    {
        return $this->includeParticipations;
    }
    /**
     * Set includeParticipations value
     * @param bool $includeParticipations
     * @return \Booking\StructType\GetBookingDetailsParameters
     */
    public function setIncludeParticipations(?bool $includeParticipations = null): self
    {
        // validation for constraint: boolean
        if (!is_null($includeParticipations) && !is_bool($includeParticipations)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($includeParticipations, true), gettype($includeParticipations)), __LINE__);
        }
        $this->includeParticipations = $includeParticipations;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @return \Booking\StructType\GetBookingDetailsParameters
     */
    public function setPersonKey(?\Booking\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
}
