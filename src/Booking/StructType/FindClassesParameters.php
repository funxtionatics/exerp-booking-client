<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for findClassesParameters StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class FindClassesParameters extends AbstractStructBase
{
    /**
     * The bookableViaAPI
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $bookableViaAPI = null;
    /**
     * The dateInterval
     * @var \Booking\StructType\Interval|null
     */
    protected ?\Booking\StructType\Interval $dateInterval = null;
    /**
     * The instructor
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $instructor = null;
    /**
     * The activityGroupIds
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var int[]
     */
    protected ?array $activityGroupIds = null;
    /**
     * The activityIds
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var int[]
     */
    protected ?array $activityIds = null;
    /**
     * The centers
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var int[]
     */
    protected ?array $centers = null;
    /**
     * The colorGroups
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\ColorGroup[]
     */
    protected ?array $colorGroups = null;
    /**
     * The externalIds
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var string[]
     */
    protected ?array $externalIds = null;
    /**
     * The timeIntervals
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\Interval[]
     */
    protected ?array $timeIntervals = null;
    /**
     * The weekdays
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var string[]
     */
    protected ?array $weekdays = null;
    /**
     * Constructor method for findClassesParameters
     * @uses FindClassesParameters::setBookableViaAPI()
     * @uses FindClassesParameters::setDateInterval()
     * @uses FindClassesParameters::setInstructor()
     * @uses FindClassesParameters::setActivityGroupIds()
     * @uses FindClassesParameters::setActivityIds()
     * @uses FindClassesParameters::setCenters()
     * @uses FindClassesParameters::setColorGroups()
     * @uses FindClassesParameters::setExternalIds()
     * @uses FindClassesParameters::setTimeIntervals()
     * @uses FindClassesParameters::setWeekdays()
     * @param bool $bookableViaAPI
     * @param \Booking\StructType\Interval $dateInterval
     * @param string $instructor
     * @param int[] $activityGroupIds
     * @param int[] $activityIds
     * @param int[] $centers
     * @param \Booking\StructType\ColorGroup[] $colorGroups
     * @param string[] $externalIds
     * @param \Booking\StructType\Interval[] $timeIntervals
     * @param string[] $weekdays
     */
    public function __construct(?bool $bookableViaAPI = null, ?\Booking\StructType\Interval $dateInterval = null, ?string $instructor = null, ?array $activityGroupIds = null, ?array $activityIds = null, ?array $centers = null, ?array $colorGroups = null, ?array $externalIds = null, ?array $timeIntervals = null, ?array $weekdays = null)
    {
        $this
            ->setBookableViaAPI($bookableViaAPI)
            ->setDateInterval($dateInterval)
            ->setInstructor($instructor)
            ->setActivityGroupIds($activityGroupIds)
            ->setActivityIds($activityIds)
            ->setCenters($centers)
            ->setColorGroups($colorGroups)
            ->setExternalIds($externalIds)
            ->setTimeIntervals($timeIntervals)
            ->setWeekdays($weekdays);
    }
    /**
     * Get bookableViaAPI value
     * @return bool|null
     */
    public function getBookableViaAPI(): ?bool
    {
        return $this->bookableViaAPI;
    }
    /**
     * Set bookableViaAPI value
     * @param bool $bookableViaAPI
     * @return \Booking\StructType\FindClassesParameters
     */
    public function setBookableViaAPI(?bool $bookableViaAPI = null): self
    {
        // validation for constraint: boolean
        if (!is_null($bookableViaAPI) && !is_bool($bookableViaAPI)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($bookableViaAPI, true), gettype($bookableViaAPI)), __LINE__);
        }
        $this->bookableViaAPI = $bookableViaAPI;
        
        return $this;
    }
    /**
     * Get dateInterval value
     * @return \Booking\StructType\Interval|null
     */
    public function getDateInterval(): ?\Booking\StructType\Interval
    {
        return $this->dateInterval;
    }
    /**
     * Set dateInterval value
     * @param \Booking\StructType\Interval $dateInterval
     * @return \Booking\StructType\FindClassesParameters
     */
    public function setDateInterval(?\Booking\StructType\Interval $dateInterval = null): self
    {
        $this->dateInterval = $dateInterval;
        
        return $this;
    }
    /**
     * Get instructor value
     * @return string|null
     */
    public function getInstructor(): ?string
    {
        return $this->instructor;
    }
    /**
     * Set instructor value
     * @param string $instructor
     * @return \Booking\StructType\FindClassesParameters
     */
    public function setInstructor(?string $instructor = null): self
    {
        // validation for constraint: string
        if (!is_null($instructor) && !is_string($instructor)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($instructor, true), gettype($instructor)), __LINE__);
        }
        $this->instructor = $instructor;
        
        return $this;
    }
    /**
     * Get activityGroupIds value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int[]
     */
    public function getActivityGroupIds(): ?array
    {
        return $this->activityGroupIds ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setActivityGroupIds method
     * This method is willingly generated in order to preserve the one-line inline validation within the setActivityGroupIds method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateActivityGroupIdsForArrayConstraintFromSetActivityGroupIds(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $findClassesParametersActivityGroupIdsItem) {
            // validation for constraint: itemType
            if (!(is_int($findClassesParametersActivityGroupIdsItem) || ctype_digit($findClassesParametersActivityGroupIdsItem))) {
                $invalidValues[] = is_object($findClassesParametersActivityGroupIdsItem) ? get_class($findClassesParametersActivityGroupIdsItem) : sprintf('%s(%s)', gettype($findClassesParametersActivityGroupIdsItem), var_export($findClassesParametersActivityGroupIdsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The activityGroupIds property can only contain items of type int, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set activityGroupIds value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param int[] $activityGroupIds
     * @return \Booking\StructType\FindClassesParameters
     */
    public function setActivityGroupIds(?array $activityGroupIds = null): self
    {
        // validation for constraint: array
        if ('' !== ($activityGroupIdsArrayErrorMessage = self::validateActivityGroupIdsForArrayConstraintFromSetActivityGroupIds($activityGroupIds))) {
            throw new InvalidArgumentException($activityGroupIdsArrayErrorMessage, __LINE__);
        }
        if (is_null($activityGroupIds) || (is_array($activityGroupIds) && empty($activityGroupIds))) {
            unset($this->activityGroupIds);
        } else {
            $this->activityGroupIds = $activityGroupIds;
        }
        
        return $this;
    }
    /**
     * Add item to activityGroupIds value
     * @throws InvalidArgumentException
     * @param int $item
     * @return \Booking\StructType\FindClassesParameters
     */
    public function addToActivityGroupIds(int $item): self
    {
        // validation for constraint: itemType
        if (!(is_int($item) || ctype_digit($item))) {
            throw new InvalidArgumentException(sprintf('The activityGroupIds property can only contain items of type int, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->activityGroupIds[] = $item;
        
        return $this;
    }
    /**
     * Get activityIds value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int[]
     */
    public function getActivityIds(): ?array
    {
        return $this->activityIds ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setActivityIds method
     * This method is willingly generated in order to preserve the one-line inline validation within the setActivityIds method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateActivityIdsForArrayConstraintFromSetActivityIds(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $findClassesParametersActivityIdsItem) {
            // validation for constraint: itemType
            if (!(is_int($findClassesParametersActivityIdsItem) || ctype_digit($findClassesParametersActivityIdsItem))) {
                $invalidValues[] = is_object($findClassesParametersActivityIdsItem) ? get_class($findClassesParametersActivityIdsItem) : sprintf('%s(%s)', gettype($findClassesParametersActivityIdsItem), var_export($findClassesParametersActivityIdsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The activityIds property can only contain items of type int, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set activityIds value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param int[] $activityIds
     * @return \Booking\StructType\FindClassesParameters
     */
    public function setActivityIds(?array $activityIds = null): self
    {
        // validation for constraint: array
        if ('' !== ($activityIdsArrayErrorMessage = self::validateActivityIdsForArrayConstraintFromSetActivityIds($activityIds))) {
            throw new InvalidArgumentException($activityIdsArrayErrorMessage, __LINE__);
        }
        if (is_null($activityIds) || (is_array($activityIds) && empty($activityIds))) {
            unset($this->activityIds);
        } else {
            $this->activityIds = $activityIds;
        }
        
        return $this;
    }
    /**
     * Add item to activityIds value
     * @throws InvalidArgumentException
     * @param int $item
     * @return \Booking\StructType\FindClassesParameters
     */
    public function addToActivityIds(int $item): self
    {
        // validation for constraint: itemType
        if (!(is_int($item) || ctype_digit($item))) {
            throw new InvalidArgumentException(sprintf('The activityIds property can only contain items of type int, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->activityIds[] = $item;
        
        return $this;
    }
    /**
     * Get centers value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int[]
     */
    public function getCenters(): ?array
    {
        return $this->centers ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setCenters method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCenters method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCentersForArrayConstraintFromSetCenters(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $findClassesParametersCentersItem) {
            // validation for constraint: itemType
            if (!(is_int($findClassesParametersCentersItem) || ctype_digit($findClassesParametersCentersItem))) {
                $invalidValues[] = is_object($findClassesParametersCentersItem) ? get_class($findClassesParametersCentersItem) : sprintf('%s(%s)', gettype($findClassesParametersCentersItem), var_export($findClassesParametersCentersItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The centers property can only contain items of type int, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set centers value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param int[] $centers
     * @return \Booking\StructType\FindClassesParameters
     */
    public function setCenters(?array $centers = null): self
    {
        // validation for constraint: array
        if ('' !== ($centersArrayErrorMessage = self::validateCentersForArrayConstraintFromSetCenters($centers))) {
            throw new InvalidArgumentException($centersArrayErrorMessage, __LINE__);
        }
        if (is_null($centers) || (is_array($centers) && empty($centers))) {
            unset($this->centers);
        } else {
            $this->centers = $centers;
        }
        
        return $this;
    }
    /**
     * Add item to centers value
     * @throws InvalidArgumentException
     * @param int $item
     * @return \Booking\StructType\FindClassesParameters
     */
    public function addToCenters(int $item): self
    {
        // validation for constraint: itemType
        if (!(is_int($item) || ctype_digit($item))) {
            throw new InvalidArgumentException(sprintf('The centers property can only contain items of type int, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->centers[] = $item;
        
        return $this;
    }
    /**
     * Get colorGroups value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\ColorGroup[]
     */
    public function getColorGroups(): ?array
    {
        return $this->colorGroups ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setColorGroups method
     * This method is willingly generated in order to preserve the one-line inline validation within the setColorGroups method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateColorGroupsForArrayConstraintFromSetColorGroups(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $findClassesParametersColorGroupsItem) {
            // validation for constraint: itemType
            if (!$findClassesParametersColorGroupsItem instanceof \Booking\StructType\ColorGroup) {
                $invalidValues[] = is_object($findClassesParametersColorGroupsItem) ? get_class($findClassesParametersColorGroupsItem) : sprintf('%s(%s)', gettype($findClassesParametersColorGroupsItem), var_export($findClassesParametersColorGroupsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The colorGroups property can only contain items of type \Booking\StructType\ColorGroup, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set colorGroups value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\ColorGroup[] $colorGroups
     * @return \Booking\StructType\FindClassesParameters
     */
    public function setColorGroups(?array $colorGroups = null): self
    {
        // validation for constraint: array
        if ('' !== ($colorGroupsArrayErrorMessage = self::validateColorGroupsForArrayConstraintFromSetColorGroups($colorGroups))) {
            throw new InvalidArgumentException($colorGroupsArrayErrorMessage, __LINE__);
        }
        if (is_null($colorGroups) || (is_array($colorGroups) && empty($colorGroups))) {
            unset($this->colorGroups);
        } else {
            $this->colorGroups = $colorGroups;
        }
        
        return $this;
    }
    /**
     * Add item to colorGroups value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\ColorGroup $item
     * @return \Booking\StructType\FindClassesParameters
     */
    public function addToColorGroups(\Booking\StructType\ColorGroup $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\ColorGroup) {
            throw new InvalidArgumentException(sprintf('The colorGroups property can only contain items of type \Booking\StructType\ColorGroup, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->colorGroups[] = $item;
        
        return $this;
    }
    /**
     * Get externalIds value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string[]
     */
    public function getExternalIds(): ?array
    {
        return $this->externalIds ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setExternalIds method
     * This method is willingly generated in order to preserve the one-line inline validation within the setExternalIds method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateExternalIdsForArrayConstraintFromSetExternalIds(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $findClassesParametersExternalIdsItem) {
            // validation for constraint: itemType
            if (!is_string($findClassesParametersExternalIdsItem)) {
                $invalidValues[] = is_object($findClassesParametersExternalIdsItem) ? get_class($findClassesParametersExternalIdsItem) : sprintf('%s(%s)', gettype($findClassesParametersExternalIdsItem), var_export($findClassesParametersExternalIdsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The externalIds property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set externalIds value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param string[] $externalIds
     * @return \Booking\StructType\FindClassesParameters
     */
    public function setExternalIds(?array $externalIds = null): self
    {
        // validation for constraint: array
        if ('' !== ($externalIdsArrayErrorMessage = self::validateExternalIdsForArrayConstraintFromSetExternalIds($externalIds))) {
            throw new InvalidArgumentException($externalIdsArrayErrorMessage, __LINE__);
        }
        if (is_null($externalIds) || (is_array($externalIds) && empty($externalIds))) {
            unset($this->externalIds);
        } else {
            $this->externalIds = $externalIds;
        }
        
        return $this;
    }
    /**
     * Add item to externalIds value
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Booking\StructType\FindClassesParameters
     */
    public function addToExternalIds(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf('The externalIds property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->externalIds[] = $item;
        
        return $this;
    }
    /**
     * Get timeIntervals value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\Interval[]
     */
    public function getTimeIntervals(): ?array
    {
        return $this->timeIntervals ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setTimeIntervals method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTimeIntervals method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTimeIntervalsForArrayConstraintFromSetTimeIntervals(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $findClassesParametersTimeIntervalsItem) {
            // validation for constraint: itemType
            if (!$findClassesParametersTimeIntervalsItem instanceof \Booking\StructType\Interval) {
                $invalidValues[] = is_object($findClassesParametersTimeIntervalsItem) ? get_class($findClassesParametersTimeIntervalsItem) : sprintf('%s(%s)', gettype($findClassesParametersTimeIntervalsItem), var_export($findClassesParametersTimeIntervalsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The timeIntervals property can only contain items of type \Booking\StructType\Interval, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set timeIntervals value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\Interval[] $timeIntervals
     * @return \Booking\StructType\FindClassesParameters
     */
    public function setTimeIntervals(?array $timeIntervals = null): self
    {
        // validation for constraint: array
        if ('' !== ($timeIntervalsArrayErrorMessage = self::validateTimeIntervalsForArrayConstraintFromSetTimeIntervals($timeIntervals))) {
            throw new InvalidArgumentException($timeIntervalsArrayErrorMessage, __LINE__);
        }
        if (is_null($timeIntervals) || (is_array($timeIntervals) && empty($timeIntervals))) {
            unset($this->timeIntervals);
        } else {
            $this->timeIntervals = $timeIntervals;
        }
        
        return $this;
    }
    /**
     * Add item to timeIntervals value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\Interval $item
     * @return \Booking\StructType\FindClassesParameters
     */
    public function addToTimeIntervals(\Booking\StructType\Interval $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\Interval) {
            throw new InvalidArgumentException(sprintf('The timeIntervals property can only contain items of type \Booking\StructType\Interval, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->timeIntervals[] = $item;
        
        return $this;
    }
    /**
     * Get weekdays value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string[]
     */
    public function getWeekdays(): ?array
    {
        return $this->weekdays ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setWeekdays method
     * This method is willingly generated in order to preserve the one-line inline validation within the setWeekdays method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateWeekdaysForArrayConstraintFromSetWeekdays(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $findClassesParametersWeekdaysItem) {
            // validation for constraint: enumeration
            if (!\Booking\EnumType\WeekDay::valueIsValid($findClassesParametersWeekdaysItem)) {
                $invalidValues[] = is_object($findClassesParametersWeekdaysItem) ? get_class($findClassesParametersWeekdaysItem) : sprintf('%s(%s)', gettype($findClassesParametersWeekdaysItem), var_export($findClassesParametersWeekdaysItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\WeekDay', is_array($invalidValues) ? implode(', ', $invalidValues) : var_export($invalidValues, true), implode(', ', \Booking\EnumType\WeekDay::getValidValues()));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set weekdays value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \Booking\EnumType\WeekDay::valueIsValid()
     * @uses \Booking\EnumType\WeekDay::getValidValues()
     * @throws InvalidArgumentException
     * @param string[] $weekdays
     * @return \Booking\StructType\FindClassesParameters
     */
    public function setWeekdays(?array $weekdays = null): self
    {
        // validation for constraint: array
        if ('' !== ($weekdaysArrayErrorMessage = self::validateWeekdaysForArrayConstraintFromSetWeekdays($weekdays))) {
            throw new InvalidArgumentException($weekdaysArrayErrorMessage, __LINE__);
        }
        if (is_null($weekdays) || (is_array($weekdays) && empty($weekdays))) {
            unset($this->weekdays);
        } else {
            $this->weekdays = $weekdays;
        }
        
        return $this;
    }
    /**
     * Add item to weekdays value
     * @uses \Booking\EnumType\WeekDay::valueIsValid()
     * @uses \Booking\EnumType\WeekDay::getValidValues()
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Booking\StructType\FindClassesParameters
     */
    public function addToWeekdays(string $item): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\WeekDay::valueIsValid($item)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\WeekDay', is_array($item) ? implode(', ', $item) : var_export($item, true), implode(', ', \Booking\EnumType\WeekDay::getValidValues())), __LINE__);
        }
        $this->weekdays[] = $item;
        
        return $this;
    }
}
