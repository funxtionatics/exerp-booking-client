<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for checkPrivilegeForBookingResult StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class CheckPrivilegeForBookingResult extends AbstractStructBase
{
    /**
     * The validPrivilege
     * @var bool|null
     */
    protected ?bool $validPrivilege = null;
    /**
     * Constructor method for checkPrivilegeForBookingResult
     * @uses CheckPrivilegeForBookingResult::setValidPrivilege()
     * @param bool $validPrivilege
     */
    public function __construct(?bool $validPrivilege = null)
    {
        $this
            ->setValidPrivilege($validPrivilege);
    }
    /**
     * Get validPrivilege value
     * @return bool|null
     */
    public function getValidPrivilege(): ?bool
    {
        return $this->validPrivilege;
    }
    /**
     * Set validPrivilege value
     * @param bool $validPrivilege
     * @return \Booking\StructType\CheckPrivilegeForBookingResult
     */
    public function setValidPrivilege(?bool $validPrivilege = null): self
    {
        // validation for constraint: boolean
        if (!is_null($validPrivilege) && !is_bool($validPrivilege)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($validPrivilege, true), gettype($validPrivilege)), __LINE__);
        }
        $this->validPrivilege = $validPrivilege;
        
        return $this;
    }
}
