<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ageGroupLimit StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AgeGroupLimit extends AbstractStructBase
{
    /**
     * The unit
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $unit = null;
    /**
     * The value
     * @var int|null
     */
    protected ?int $value = null;
    /**
     * Constructor method for ageGroupLimit
     * @uses AgeGroupLimit::setUnit()
     * @uses AgeGroupLimit::setValue()
     * @param string $unit
     * @param int $value
     */
    public function __construct(?string $unit = null, ?int $value = null)
    {
        $this
            ->setUnit($unit)
            ->setValue($value);
    }
    /**
     * Get unit value
     * @return string|null
     */
    public function getUnit(): ?string
    {
        return $this->unit;
    }
    /**
     * Set unit value
     * @uses \Booking\EnumType\AgeGroupTimeUnit::valueIsValid()
     * @uses \Booking\EnumType\AgeGroupTimeUnit::getValidValues()
     * @throws InvalidArgumentException
     * @param string $unit
     * @return \Booking\StructType\AgeGroupLimit
     */
    public function setUnit(?string $unit = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\AgeGroupTimeUnit::valueIsValid($unit)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\AgeGroupTimeUnit', is_array($unit) ? implode(', ', $unit) : var_export($unit, true), implode(', ', \Booking\EnumType\AgeGroupTimeUnit::getValidValues())), __LINE__);
        }
        $this->unit = $unit;
        
        return $this;
    }
    /**
     * Get value value
     * @return int|null
     */
    public function getValue(): ?int
    {
        return $this->value;
    }
    /**
     * Set value value
     * @param int $value
     * @return \Booking\StructType\AgeGroupLimit
     */
    public function setValue(?int $value = null): self
    {
        // validation for constraint: int
        if (!is_null($value) && !(is_int($value) || ctype_digit($value))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->value = $value;
        
        return $this;
    }
}
