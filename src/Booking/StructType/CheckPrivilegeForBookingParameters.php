<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for checkPrivilegeForBookingParameters StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class CheckPrivilegeForBookingParameters extends AbstractStructBase
{
    /**
     * The bookingId
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $bookingId = null;
    /**
     * The includeTentative
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $includeTentative = null;
    /**
     * The personId
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $personId = null;
    /**
     * The productKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ApiProductKey|null
     */
    protected ?\Booking\StructType\ApiProductKey $productKey = null;
    /**
     * Constructor method for checkPrivilegeForBookingParameters
     * @uses CheckPrivilegeForBookingParameters::setBookingId()
     * @uses CheckPrivilegeForBookingParameters::setIncludeTentative()
     * @uses CheckPrivilegeForBookingParameters::setPersonId()
     * @uses CheckPrivilegeForBookingParameters::setProductKey()
     * @param \Booking\StructType\CompositeKey $bookingId
     * @param bool $includeTentative
     * @param \Booking\StructType\ApiPersonKey $personId
     * @param \Booking\StructType\ApiProductKey $productKey
     */
    public function __construct(?\Booking\StructType\CompositeKey $bookingId = null, ?bool $includeTentative = null, ?\Booking\StructType\ApiPersonKey $personId = null, ?\Booking\StructType\ApiProductKey $productKey = null)
    {
        $this
            ->setBookingId($bookingId)
            ->setIncludeTentative($includeTentative)
            ->setPersonId($personId)
            ->setProductKey($productKey);
    }
    /**
     * Get bookingId value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getBookingId(): ?\Booking\StructType\CompositeKey
    {
        return $this->bookingId;
    }
    /**
     * Set bookingId value
     * @param \Booking\StructType\CompositeKey $bookingId
     * @return \Booking\StructType\CheckPrivilegeForBookingParameters
     */
    public function setBookingId(?\Booking\StructType\CompositeKey $bookingId = null): self
    {
        $this->bookingId = $bookingId;
        
        return $this;
    }
    /**
     * Get includeTentative value
     * @return bool|null
     */
    public function getIncludeTentative(): ?bool
    {
        return $this->includeTentative;
    }
    /**
     * Set includeTentative value
     * @param bool $includeTentative
     * @return \Booking\StructType\CheckPrivilegeForBookingParameters
     */
    public function setIncludeTentative(?bool $includeTentative = null): self
    {
        // validation for constraint: boolean
        if (!is_null($includeTentative) && !is_bool($includeTentative)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($includeTentative, true), gettype($includeTentative)), __LINE__);
        }
        $this->includeTentative = $includeTentative;
        
        return $this;
    }
    /**
     * Get personId value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getPersonId(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->personId;
    }
    /**
     * Set personId value
     * @param \Booking\StructType\ApiPersonKey $personId
     * @return \Booking\StructType\CheckPrivilegeForBookingParameters
     */
    public function setPersonId(?\Booking\StructType\ApiPersonKey $personId = null): self
    {
        $this->personId = $personId;
        
        return $this;
    }
    /**
     * Get productKey value
     * @return \Booking\StructType\ApiProductKey|null
     */
    public function getProductKey(): ?\Booking\StructType\ApiProductKey
    {
        return $this->productKey;
    }
    /**
     * Set productKey value
     * @param \Booking\StructType\ApiProductKey $productKey
     * @return \Booking\StructType\CheckPrivilegeForBookingParameters
     */
    public function setProductKey(?\Booking\StructType\ApiProductKey $productKey = null): self
    {
        $this->productKey = $productKey;
        
        return $this;
    }
}
