<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for participant StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Participant extends AbstractStructBase
{
    /**
     * The personKey
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $personKey = null;
    /**
     * The seatName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $seatName = null;
    /**
     * Constructor method for participant
     * @uses Participant::setPersonKey()
     * @uses Participant::setSeatName()
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @param string $seatName
     */
    public function __construct(?\Booking\StructType\ApiPersonKey $personKey = null, ?string $seatName = null)
    {
        $this
            ->setPersonKey($personKey)
            ->setSeatName($seatName);
    }
    /**
     * Get personKey value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @return \Booking\StructType\Participant
     */
    public function setPersonKey(?\Booking\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get seatName value
     * @return string|null
     */
    public function getSeatName(): ?string
    {
        return $this->seatName;
    }
    /**
     * Set seatName value
     * @param string $seatName
     * @return \Booking\StructType\Participant
     */
    public function setSeatName(?string $seatName = null): self
    {
        // validation for constraint: string
        if (!is_null($seatName) && !is_string($seatName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($seatName, true), gettype($seatName)), __LINE__);
        }
        $this->seatName = $seatName;
        
        return $this;
    }
}
