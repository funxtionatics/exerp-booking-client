<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getBookingRestrictionsResult StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class GetBookingRestrictionsResult extends AbstractStructBase
{
    /**
     * The bookingRestrictions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\BookingRestrictions|null
     */
    protected ?\Booking\StructType\BookingRestrictions $bookingRestrictions = null;
    /**
     * Constructor method for getBookingRestrictionsResult
     * @uses GetBookingRestrictionsResult::setBookingRestrictions()
     * @param \Booking\StructType\BookingRestrictions $bookingRestrictions
     */
    public function __construct(?\Booking\StructType\BookingRestrictions $bookingRestrictions = null)
    {
        $this
            ->setBookingRestrictions($bookingRestrictions);
    }
    /**
     * Get bookingRestrictions value
     * @return \Booking\StructType\BookingRestrictions|null
     */
    public function getBookingRestrictions(): ?\Booking\StructType\BookingRestrictions
    {
        return $this->bookingRestrictions;
    }
    /**
     * Set bookingRestrictions value
     * @param \Booking\StructType\BookingRestrictions $bookingRestrictions
     * @return \Booking\StructType\GetBookingRestrictionsResult
     */
    public function setBookingRestrictions(?\Booking\StructType\BookingRestrictions $bookingRestrictions = null): self
    {
        $this->bookingRestrictions = $bookingRestrictions;
        
        return $this;
    }
}
