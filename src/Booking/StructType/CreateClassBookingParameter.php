<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createClassBookingParameter StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class CreateClassBookingParameter extends AbstractStructBase
{
    /**
     * The activityKey
     * @var int|null
     */
    protected ?int $activityKey = null;
    /**
     * The centerId
     * @var int|null
     */
    protected ?int $centerId = null;
    /**
     * The color
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ColorGroup|null
     */
    protected ?\Booking\StructType\ColorGroup $color = null;
    /**
     * The date
     * @var string|null
     */
    protected ?string $date = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The duration
     * @var int|null
     */
    protected ?int $duration = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The maxAge
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $maxAge = null;
    /**
     * The maxCapacity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $maxCapacity = null;
    /**
     * The minAge
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $minAge = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The resourceKeys
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * @var \Booking\StructType\CompositeKey[]
     */
    protected ?array $resourceKeys = null;
    /**
     * The time
     * @var string|null
     */
    protected ?string $time = null;
    /**
     * The staffPersonKeys
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\ApiPersonKey[]
     */
    protected ?array $staffPersonKeys = null;
    /**
     * Constructor method for createClassBookingParameter
     * @uses CreateClassBookingParameter::setActivityKey()
     * @uses CreateClassBookingParameter::setCenterId()
     * @uses CreateClassBookingParameter::setColor()
     * @uses CreateClassBookingParameter::setDate()
     * @uses CreateClassBookingParameter::setDescription()
     * @uses CreateClassBookingParameter::setDuration()
     * @uses CreateClassBookingParameter::setExternalId()
     * @uses CreateClassBookingParameter::setMaxAge()
     * @uses CreateClassBookingParameter::setMaxCapacity()
     * @uses CreateClassBookingParameter::setMinAge()
     * @uses CreateClassBookingParameter::setName()
     * @uses CreateClassBookingParameter::setResourceKeys()
     * @uses CreateClassBookingParameter::setTime()
     * @uses CreateClassBookingParameter::setStaffPersonKeys()
     * @param int $activityKey
     * @param int $centerId
     * @param \Booking\StructType\ColorGroup $color
     * @param string $date
     * @param string $description
     * @param int $duration
     * @param string $externalId
     * @param int $maxAge
     * @param int $maxCapacity
     * @param int $minAge
     * @param string $name
     * @param \Booking\StructType\CompositeKey[] $resourceKeys
     * @param string $time
     * @param \Booking\StructType\ApiPersonKey[] $staffPersonKeys
     */
    public function __construct(?int $activityKey = null, ?int $centerId = null, ?\Booking\StructType\ColorGroup $color = null, ?string $date = null, ?string $description = null, ?int $duration = null, ?string $externalId = null, ?int $maxAge = null, ?int $maxCapacity = null, ?int $minAge = null, ?string $name = null, ?array $resourceKeys = null, ?string $time = null, ?array $staffPersonKeys = null)
    {
        $this
            ->setActivityKey($activityKey)
            ->setCenterId($centerId)
            ->setColor($color)
            ->setDate($date)
            ->setDescription($description)
            ->setDuration($duration)
            ->setExternalId($externalId)
            ->setMaxAge($maxAge)
            ->setMaxCapacity($maxCapacity)
            ->setMinAge($minAge)
            ->setName($name)
            ->setResourceKeys($resourceKeys)
            ->setTime($time)
            ->setStaffPersonKeys($staffPersonKeys);
    }
    /**
     * Get activityKey value
     * @return int|null
     */
    public function getActivityKey(): ?int
    {
        return $this->activityKey;
    }
    /**
     * Set activityKey value
     * @param int $activityKey
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setActivityKey(?int $activityKey = null): self
    {
        // validation for constraint: int
        if (!is_null($activityKey) && !(is_int($activityKey) || ctype_digit($activityKey))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($activityKey, true), gettype($activityKey)), __LINE__);
        }
        $this->activityKey = $activityKey;
        
        return $this;
    }
    /**
     * Get centerId value
     * @return int|null
     */
    public function getCenterId(): ?int
    {
        return $this->centerId;
    }
    /**
     * Set centerId value
     * @param int $centerId
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setCenterId(?int $centerId = null): self
    {
        // validation for constraint: int
        if (!is_null($centerId) && !(is_int($centerId) || ctype_digit($centerId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($centerId, true), gettype($centerId)), __LINE__);
        }
        $this->centerId = $centerId;
        
        return $this;
    }
    /**
     * Get color value
     * @return \Booking\StructType\ColorGroup|null
     */
    public function getColor(): ?\Booking\StructType\ColorGroup
    {
        return $this->color;
    }
    /**
     * Set color value
     * @param \Booking\StructType\ColorGroup $color
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setColor(?\Booking\StructType\ColorGroup $color = null): self
    {
        $this->color = $color;
        
        return $this;
    }
    /**
     * Get date value
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }
    /**
     * Set date value
     * @param string $date
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setDate(?string $date = null): self
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->date = $date;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get duration value
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }
    /**
     * Set duration value
     * @param int $duration
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setDuration(?int $duration = null): self
    {
        // validation for constraint: int
        if (!is_null($duration) && !(is_int($duration) || ctype_digit($duration))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($duration, true), gettype($duration)), __LINE__);
        }
        $this->duration = $duration;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get maxAge value
     * @return int|null
     */
    public function getMaxAge(): ?int
    {
        return $this->maxAge;
    }
    /**
     * Set maxAge value
     * @param int $maxAge
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setMaxAge(?int $maxAge = null): self
    {
        // validation for constraint: int
        if (!is_null($maxAge) && !(is_int($maxAge) || ctype_digit($maxAge))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($maxAge, true), gettype($maxAge)), __LINE__);
        }
        $this->maxAge = $maxAge;
        
        return $this;
    }
    /**
     * Get maxCapacity value
     * @return int|null
     */
    public function getMaxCapacity(): ?int
    {
        return $this->maxCapacity;
    }
    /**
     * Set maxCapacity value
     * @param int $maxCapacity
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setMaxCapacity(?int $maxCapacity = null): self
    {
        // validation for constraint: int
        if (!is_null($maxCapacity) && !(is_int($maxCapacity) || ctype_digit($maxCapacity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($maxCapacity, true), gettype($maxCapacity)), __LINE__);
        }
        $this->maxCapacity = $maxCapacity;
        
        return $this;
    }
    /**
     * Get minAge value
     * @return int|null
     */
    public function getMinAge(): ?int
    {
        return $this->minAge;
    }
    /**
     * Set minAge value
     * @param int $minAge
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setMinAge(?int $minAge = null): self
    {
        // validation for constraint: int
        if (!is_null($minAge) && !(is_int($minAge) || ctype_digit($minAge))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($minAge, true), gettype($minAge)), __LINE__);
        }
        $this->minAge = $minAge;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get resourceKeys value
     * @return \Booking\StructType\CompositeKey[]
     */
    public function getResourceKeys(): ?array
    {
        return $this->resourceKeys;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setResourceKeys method
     * This method is willingly generated in order to preserve the one-line inline validation within the setResourceKeys method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateResourceKeysForArrayConstraintFromSetResourceKeys(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $createClassBookingParameterResourceKeysItem) {
            // validation for constraint: itemType
            if (!$createClassBookingParameterResourceKeysItem instanceof \Booking\StructType\CompositeKey) {
                $invalidValues[] = is_object($createClassBookingParameterResourceKeysItem) ? get_class($createClassBookingParameterResourceKeysItem) : sprintf('%s(%s)', gettype($createClassBookingParameterResourceKeysItem), var_export($createClassBookingParameterResourceKeysItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The resourceKeys property can only contain items of type \Booking\StructType\CompositeKey, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set resourceKeys value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\CompositeKey[] $resourceKeys
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setResourceKeys(?array $resourceKeys = null): self
    {
        // validation for constraint: array
        if ('' !== ($resourceKeysArrayErrorMessage = self::validateResourceKeysForArrayConstraintFromSetResourceKeys($resourceKeys))) {
            throw new InvalidArgumentException($resourceKeysArrayErrorMessage, __LINE__);
        }
        $this->resourceKeys = $resourceKeys;
        
        return $this;
    }
    /**
     * Add item to resourceKeys value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\CompositeKey $item
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function addToResourceKeys(\Booking\StructType\CompositeKey $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\CompositeKey) {
            throw new InvalidArgumentException(sprintf('The resourceKeys property can only contain items of type \Booking\StructType\CompositeKey, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->resourceKeys[] = $item;
        
        return $this;
    }
    /**
     * Get time value
     * @return string|null
     */
    public function getTime(): ?string
    {
        return $this->time;
    }
    /**
     * Set time value
     * @param string $time
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setTime(?string $time = null): self
    {
        // validation for constraint: string
        if (!is_null($time) && !is_string($time)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($time, true), gettype($time)), __LINE__);
        }
        $this->time = $time;
        
        return $this;
    }
    /**
     * Get staffPersonKeys value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\ApiPersonKey[]
     */
    public function getStaffPersonKeys(): ?array
    {
        return $this->staffPersonKeys ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setStaffPersonKeys method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStaffPersonKeys method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStaffPersonKeysForArrayConstraintFromSetStaffPersonKeys(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $createClassBookingParameterStaffPersonKeysItem) {
            // validation for constraint: itemType
            if (!$createClassBookingParameterStaffPersonKeysItem instanceof \Booking\StructType\ApiPersonKey) {
                $invalidValues[] = is_object($createClassBookingParameterStaffPersonKeysItem) ? get_class($createClassBookingParameterStaffPersonKeysItem) : sprintf('%s(%s)', gettype($createClassBookingParameterStaffPersonKeysItem), var_export($createClassBookingParameterStaffPersonKeysItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The staffPersonKeys property can only contain items of type \Booking\StructType\ApiPersonKey, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set staffPersonKeys value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\ApiPersonKey[] $staffPersonKeys
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function setStaffPersonKeys(?array $staffPersonKeys = null): self
    {
        // validation for constraint: array
        if ('' !== ($staffPersonKeysArrayErrorMessage = self::validateStaffPersonKeysForArrayConstraintFromSetStaffPersonKeys($staffPersonKeys))) {
            throw new InvalidArgumentException($staffPersonKeysArrayErrorMessage, __LINE__);
        }
        if (is_null($staffPersonKeys) || (is_array($staffPersonKeys) && empty($staffPersonKeys))) {
            unset($this->staffPersonKeys);
        } else {
            $this->staffPersonKeys = $staffPersonKeys;
        }
        
        return $this;
    }
    /**
     * Add item to staffPersonKeys value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\ApiPersonKey $item
     * @return \Booking\StructType\CreateClassBookingParameter
     */
    public function addToStaffPersonKeys(\Booking\StructType\ApiPersonKey $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\ApiPersonKey) {
            throw new InvalidArgumentException(sprintf('The staffPersonKeys property can only contain items of type \Booking\StructType\ApiPersonKey, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->staffPersonKeys[] = $item;
        
        return $this;
    }
}
