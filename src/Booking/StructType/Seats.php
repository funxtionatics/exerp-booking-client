<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for seats StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Seats extends AbstractStructBase
{
    /**
     * The seat
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Booking\StructType\Seat[]
     */
    protected ?array $seat = null;
    /**
     * Constructor method for seats
     * @uses Seats::setSeat()
     * @param \Booking\StructType\Seat[] $seat
     */
    public function __construct(?array $seat = null)
    {
        $this
            ->setSeat($seat);
    }
    /**
     * Get seat value
     * @return \Booking\StructType\Seat[]
     */
    public function getSeat(): ?array
    {
        return $this->seat;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setSeat method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSeat method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSeatForArrayConstraintFromSetSeat(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $seatsSeatItem) {
            // validation for constraint: itemType
            if (!$seatsSeatItem instanceof \Booking\StructType\Seat) {
                $invalidValues[] = is_object($seatsSeatItem) ? get_class($seatsSeatItem) : sprintf('%s(%s)', gettype($seatsSeatItem), var_export($seatsSeatItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The seat property can only contain items of type \Booking\StructType\Seat, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set seat value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\Seat[] $seat
     * @return \Booking\StructType\Seats
     */
    public function setSeat(?array $seat = null): self
    {
        // validation for constraint: array
        if ('' !== ($seatArrayErrorMessage = self::validateSeatForArrayConstraintFromSetSeat($seat))) {
            throw new InvalidArgumentException($seatArrayErrorMessage, __LINE__);
        }
        $this->seat = $seat;
        
        return $this;
    }
    /**
     * Add item to seat value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\Seat $item
     * @return \Booking\StructType\Seats
     */
    public function addToSeat(\Booking\StructType\Seat $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\Seat) {
            throw new InvalidArgumentException(sprintf('The seat property can only contain items of type \Booking\StructType\Seat, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->seat[] = $item;
        
        return $this;
    }
}
