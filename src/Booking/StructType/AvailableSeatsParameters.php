<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for availableSeatsParameters StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AvailableSeatsParameters extends AbstractStructBase
{
    /**
     * The bookingKey
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $bookingKey = null;
    /**
     * The centerId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $centerId = null;
    /**
     * The personId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $personId = null;
    /**
     * The personType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $personType = null;
    /**
     * Constructor method for availableSeatsParameters
     * @uses AvailableSeatsParameters::setBookingKey()
     * @uses AvailableSeatsParameters::setCenterId()
     * @uses AvailableSeatsParameters::setPersonId()
     * @uses AvailableSeatsParameters::setPersonType()
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @param int $centerId
     * @param \Booking\StructType\ApiPersonKey $personId
     * @param string $personType
     */
    public function __construct(?\Booking\StructType\CompositeKey $bookingKey = null, ?int $centerId = null, ?\Booking\StructType\ApiPersonKey $personId = null, ?string $personType = null)
    {
        $this
            ->setBookingKey($bookingKey)
            ->setCenterId($centerId)
            ->setPersonId($personId)
            ->setPersonType($personType);
    }
    /**
     * Get bookingKey value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getBookingKey(): ?\Booking\StructType\CompositeKey
    {
        return $this->bookingKey;
    }
    /**
     * Set bookingKey value
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @return \Booking\StructType\AvailableSeatsParameters
     */
    public function setBookingKey(?\Booking\StructType\CompositeKey $bookingKey = null): self
    {
        $this->bookingKey = $bookingKey;
        
        return $this;
    }
    /**
     * Get centerId value
     * @return int|null
     */
    public function getCenterId(): ?int
    {
        return $this->centerId;
    }
    /**
     * Set centerId value
     * @param int $centerId
     * @return \Booking\StructType\AvailableSeatsParameters
     */
    public function setCenterId(?int $centerId = null): self
    {
        // validation for constraint: int
        if (!is_null($centerId) && !(is_int($centerId) || ctype_digit($centerId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($centerId, true), gettype($centerId)), __LINE__);
        }
        $this->centerId = $centerId;
        
        return $this;
    }
    /**
     * Get personId value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getPersonId(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->personId;
    }
    /**
     * Set personId value
     * @param \Booking\StructType\ApiPersonKey $personId
     * @return \Booking\StructType\AvailableSeatsParameters
     */
    public function setPersonId(?\Booking\StructType\ApiPersonKey $personId = null): self
    {
        $this->personId = $personId;
        
        return $this;
    }
    /**
     * Get personType value
     * @return string|null
     */
    public function getPersonType(): ?string
    {
        return $this->personType;
    }
    /**
     * Set personType value
     * @uses \Booking\EnumType\PersonType::valueIsValid()
     * @uses \Booking\EnumType\PersonType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $personType
     * @return \Booking\StructType\AvailableSeatsParameters
     */
    public function setPersonType(?string $personType = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\PersonType::valueIsValid($personType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\PersonType', is_array($personType) ? implode(', ', $personType) : var_export($personType, true), implode(', ', \Booking\EnumType\PersonType::getValidValues())), __LINE__);
        }
        $this->personType = $personType;
        
        return $this;
    }
}
