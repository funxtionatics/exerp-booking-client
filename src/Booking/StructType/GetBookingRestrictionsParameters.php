<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getBookingRestrictionsParameters StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class GetBookingRestrictionsParameters extends AbstractStructBase
{
    /**
     * The personKey
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $personKey = null;
    /**
     * The periods
     * @var \Booking\StructType\Periods|null
     */
    protected ?\Booking\StructType\Periods $periods = null;
    /**
     * Constructor method for getBookingRestrictionsParameters
     * @uses GetBookingRestrictionsParameters::setPersonKey()
     * @uses GetBookingRestrictionsParameters::setPeriods()
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @param \Booking\StructType\Periods $periods
     */
    public function __construct(?\Booking\StructType\ApiPersonKey $personKey = null, ?\Booking\StructType\Periods $periods = null)
    {
        $this
            ->setPersonKey($personKey)
            ->setPeriods($periods);
    }
    /**
     * Get personKey value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @return \Booking\StructType\GetBookingRestrictionsParameters
     */
    public function setPersonKey(?\Booking\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get periods value
     * @return \Booking\StructType\Periods|null
     */
    public function getPeriods(): ?\Booking\StructType\Periods
    {
        return $this->periods;
    }
    /**
     * Set periods value
     * @param \Booking\StructType\Periods $periods
     * @return \Booking\StructType\GetBookingRestrictionsParameters
     */
    public function setPeriods(?\Booking\StructType\Periods $periods = null): self
    {
        $this->periods = $periods;
        
        return $this;
    }
}
