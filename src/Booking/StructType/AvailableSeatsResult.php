<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for availableSeatsResult StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AvailableSeatsResult extends AbstractStructBase
{
    /**
     * The seats
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\Seats|null
     */
    protected ?\Booking\StructType\Seats $seats = null;
    /**
     * Constructor method for availableSeatsResult
     * @uses AvailableSeatsResult::setSeats()
     * @param \Booking\StructType\Seats $seats
     */
    public function __construct(?\Booking\StructType\Seats $seats = null)
    {
        $this
            ->setSeats($seats);
    }
    /**
     * Get seats value
     * @return \Booking\StructType\Seats|null
     */
    public function getSeats(): ?\Booking\StructType\Seats
    {
        return $this->seats;
    }
    /**
     * Set seats value
     * @param \Booking\StructType\Seats $seats
     * @return \Booking\StructType\AvailableSeatsResult
     */
    public function setSeats(?\Booking\StructType\Seats $seats = null): self
    {
        $this->seats = $seats;
        
        return $this;
    }
}
