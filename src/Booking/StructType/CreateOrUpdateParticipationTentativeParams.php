<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createOrUpdateParticipationTentativeParams StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class CreateOrUpdateParticipationTentativeParams extends AbstractStructBase
{
    /**
     * The bookingId
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $bookingId = null;
    /**
     * The bookingOwnerPersonId
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $bookingOwnerPersonId = null;
    /**
     * The participants
     * @var \Booking\StructType\Participants|null
     */
    protected ?\Booking\StructType\Participants $participants = null;
    /**
     * The userInterfaceType
     * @var string|null
     */
    protected ?string $userInterfaceType = null;
    /**
     * Constructor method for createOrUpdateParticipationTentativeParams
     * @uses CreateOrUpdateParticipationTentativeParams::setBookingId()
     * @uses CreateOrUpdateParticipationTentativeParams::setBookingOwnerPersonId()
     * @uses CreateOrUpdateParticipationTentativeParams::setParticipants()
     * @uses CreateOrUpdateParticipationTentativeParams::setUserInterfaceType()
     * @param \Booking\StructType\CompositeKey $bookingId
     * @param \Booking\StructType\ApiPersonKey $bookingOwnerPersonId
     * @param \Booking\StructType\Participants $participants
     * @param string $userInterfaceType
     */
    public function __construct(?\Booking\StructType\CompositeKey $bookingId = null, ?\Booking\StructType\ApiPersonKey $bookingOwnerPersonId = null, ?\Booking\StructType\Participants $participants = null, ?string $userInterfaceType = null)
    {
        $this
            ->setBookingId($bookingId)
            ->setBookingOwnerPersonId($bookingOwnerPersonId)
            ->setParticipants($participants)
            ->setUserInterfaceType($userInterfaceType);
    }
    /**
     * Get bookingId value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getBookingId(): ?\Booking\StructType\CompositeKey
    {
        return $this->bookingId;
    }
    /**
     * Set bookingId value
     * @param \Booking\StructType\CompositeKey $bookingId
     * @return \Booking\StructType\CreateOrUpdateParticipationTentativeParams
     */
    public function setBookingId(?\Booking\StructType\CompositeKey $bookingId = null): self
    {
        $this->bookingId = $bookingId;
        
        return $this;
    }
    /**
     * Get bookingOwnerPersonId value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getBookingOwnerPersonId(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->bookingOwnerPersonId;
    }
    /**
     * Set bookingOwnerPersonId value
     * @param \Booking\StructType\ApiPersonKey $bookingOwnerPersonId
     * @return \Booking\StructType\CreateOrUpdateParticipationTentativeParams
     */
    public function setBookingOwnerPersonId(?\Booking\StructType\ApiPersonKey $bookingOwnerPersonId = null): self
    {
        $this->bookingOwnerPersonId = $bookingOwnerPersonId;
        
        return $this;
    }
    /**
     * Get participants value
     * @return \Booking\StructType\Participants|null
     */
    public function getParticipants(): ?\Booking\StructType\Participants
    {
        return $this->participants;
    }
    /**
     * Set participants value
     * @param \Booking\StructType\Participants $participants
     * @return \Booking\StructType\CreateOrUpdateParticipationTentativeParams
     */
    public function setParticipants(?\Booking\StructType\Participants $participants = null): self
    {
        $this->participants = $participants;
        
        return $this;
    }
    /**
     * Get userInterfaceType value
     * @return string|null
     */
    public function getUserInterfaceType(): ?string
    {
        return $this->userInterfaceType;
    }
    /**
     * Set userInterfaceType value
     * @uses \Booking\EnumType\UserInterfaceType::valueIsValid()
     * @uses \Booking\EnumType\UserInterfaceType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $userInterfaceType
     * @return \Booking\StructType\CreateOrUpdateParticipationTentativeParams
     */
    public function setUserInterfaceType(?string $userInterfaceType = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\UserInterfaceType::valueIsValid($userInterfaceType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\UserInterfaceType', is_array($userInterfaceType) ? implode(', ', $userInterfaceType) : var_export($userInterfaceType, true), implode(', ', \Booking\EnumType\UserInterfaceType::getValidValues())), __LINE__);
        }
        $this->userInterfaceType = $userInterfaceType;
        
        return $this;
    }
}
