<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for bookingChangeActivityParameter StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class BookingChangeActivityParameter extends AbstractStructBase
{
    /**
     * The activityId
     * @var int|null
     */
    protected ?int $activityId = null;
    /**
     * The bookingKey
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $bookingKey = null;
    /**
     * The date
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $date = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The duration
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $duration = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The maxCapacity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $maxCapacity = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The sendCancelMessage
     * @var bool|null
     */
    protected ?bool $sendCancelMessage = null;
    /**
     * The sendChangeMessage
     * @var bool|null
     */
    protected ?bool $sendChangeMessage = null;
    /**
     * The time
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $time = null;
    /**
     * The resourceKeys
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\CompositeKey[]
     */
    protected ?array $resourceKeys = null;
    /**
     * The staffPersonKeys
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\ApiPersonKey[]
     */
    protected ?array $staffPersonKeys = null;
    /**
     * Constructor method for bookingChangeActivityParameter
     * @uses BookingChangeActivityParameter::setActivityId()
     * @uses BookingChangeActivityParameter::setBookingKey()
     * @uses BookingChangeActivityParameter::setDate()
     * @uses BookingChangeActivityParameter::setDescription()
     * @uses BookingChangeActivityParameter::setDuration()
     * @uses BookingChangeActivityParameter::setExternalId()
     * @uses BookingChangeActivityParameter::setMaxCapacity()
     * @uses BookingChangeActivityParameter::setName()
     * @uses BookingChangeActivityParameter::setSendCancelMessage()
     * @uses BookingChangeActivityParameter::setSendChangeMessage()
     * @uses BookingChangeActivityParameter::setTime()
     * @uses BookingChangeActivityParameter::setResourceKeys()
     * @uses BookingChangeActivityParameter::setStaffPersonKeys()
     * @param int $activityId
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @param string $date
     * @param string $description
     * @param int $duration
     * @param string $externalId
     * @param int $maxCapacity
     * @param string $name
     * @param bool $sendCancelMessage
     * @param bool $sendChangeMessage
     * @param string $time
     * @param \Booking\StructType\CompositeKey[] $resourceKeys
     * @param \Booking\StructType\ApiPersonKey[] $staffPersonKeys
     */
    public function __construct(?int $activityId = null, ?\Booking\StructType\CompositeKey $bookingKey = null, ?string $date = null, ?string $description = null, ?int $duration = null, ?string $externalId = null, ?int $maxCapacity = null, ?string $name = null, ?bool $sendCancelMessage = null, ?bool $sendChangeMessage = null, ?string $time = null, ?array $resourceKeys = null, ?array $staffPersonKeys = null)
    {
        $this
            ->setActivityId($activityId)
            ->setBookingKey($bookingKey)
            ->setDate($date)
            ->setDescription($description)
            ->setDuration($duration)
            ->setExternalId($externalId)
            ->setMaxCapacity($maxCapacity)
            ->setName($name)
            ->setSendCancelMessage($sendCancelMessage)
            ->setSendChangeMessage($sendChangeMessage)
            ->setTime($time)
            ->setResourceKeys($resourceKeys)
            ->setStaffPersonKeys($staffPersonKeys);
    }
    /**
     * Get activityId value
     * @return int|null
     */
    public function getActivityId(): ?int
    {
        return $this->activityId;
    }
    /**
     * Set activityId value
     * @param int $activityId
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setActivityId(?int $activityId = null): self
    {
        // validation for constraint: int
        if (!is_null($activityId) && !(is_int($activityId) || ctype_digit($activityId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($activityId, true), gettype($activityId)), __LINE__);
        }
        $this->activityId = $activityId;
        
        return $this;
    }
    /**
     * Get bookingKey value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getBookingKey(): ?\Booking\StructType\CompositeKey
    {
        return $this->bookingKey;
    }
    /**
     * Set bookingKey value
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setBookingKey(?\Booking\StructType\CompositeKey $bookingKey = null): self
    {
        $this->bookingKey = $bookingKey;
        
        return $this;
    }
    /**
     * Get date value
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }
    /**
     * Set date value
     * @param string $date
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setDate(?string $date = null): self
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->date = $date;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get duration value
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }
    /**
     * Set duration value
     * @param int $duration
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setDuration(?int $duration = null): self
    {
        // validation for constraint: int
        if (!is_null($duration) && !(is_int($duration) || ctype_digit($duration))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($duration, true), gettype($duration)), __LINE__);
        }
        $this->duration = $duration;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get maxCapacity value
     * @return int|null
     */
    public function getMaxCapacity(): ?int
    {
        return $this->maxCapacity;
    }
    /**
     * Set maxCapacity value
     * @param int $maxCapacity
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setMaxCapacity(?int $maxCapacity = null): self
    {
        // validation for constraint: int
        if (!is_null($maxCapacity) && !(is_int($maxCapacity) || ctype_digit($maxCapacity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($maxCapacity, true), gettype($maxCapacity)), __LINE__);
        }
        $this->maxCapacity = $maxCapacity;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get sendCancelMessage value
     * @return bool|null
     */
    public function getSendCancelMessage(): ?bool
    {
        return $this->sendCancelMessage;
    }
    /**
     * Set sendCancelMessage value
     * @param bool $sendCancelMessage
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setSendCancelMessage(?bool $sendCancelMessage = null): self
    {
        // validation for constraint: boolean
        if (!is_null($sendCancelMessage) && !is_bool($sendCancelMessage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($sendCancelMessage, true), gettype($sendCancelMessage)), __LINE__);
        }
        $this->sendCancelMessage = $sendCancelMessage;
        
        return $this;
    }
    /**
     * Get sendChangeMessage value
     * @return bool|null
     */
    public function getSendChangeMessage(): ?bool
    {
        return $this->sendChangeMessage;
    }
    /**
     * Set sendChangeMessage value
     * @param bool $sendChangeMessage
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setSendChangeMessage(?bool $sendChangeMessage = null): self
    {
        // validation for constraint: boolean
        if (!is_null($sendChangeMessage) && !is_bool($sendChangeMessage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($sendChangeMessage, true), gettype($sendChangeMessage)), __LINE__);
        }
        $this->sendChangeMessage = $sendChangeMessage;
        
        return $this;
    }
    /**
     * Get time value
     * @return string|null
     */
    public function getTime(): ?string
    {
        return $this->time;
    }
    /**
     * Set time value
     * @param string $time
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setTime(?string $time = null): self
    {
        // validation for constraint: string
        if (!is_null($time) && !is_string($time)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($time, true), gettype($time)), __LINE__);
        }
        $this->time = $time;
        
        return $this;
    }
    /**
     * Get resourceKeys value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\CompositeKey[]
     */
    public function getResourceKeys(): ?array
    {
        return $this->resourceKeys ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setResourceKeys method
     * This method is willingly generated in order to preserve the one-line inline validation within the setResourceKeys method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateResourceKeysForArrayConstraintFromSetResourceKeys(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $bookingChangeActivityParameterResourceKeysItem) {
            // validation for constraint: itemType
            if (!$bookingChangeActivityParameterResourceKeysItem instanceof \Booking\StructType\CompositeKey) {
                $invalidValues[] = is_object($bookingChangeActivityParameterResourceKeysItem) ? get_class($bookingChangeActivityParameterResourceKeysItem) : sprintf('%s(%s)', gettype($bookingChangeActivityParameterResourceKeysItem), var_export($bookingChangeActivityParameterResourceKeysItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The resourceKeys property can only contain items of type \Booking\StructType\CompositeKey, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set resourceKeys value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\CompositeKey[] $resourceKeys
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setResourceKeys(?array $resourceKeys = null): self
    {
        // validation for constraint: array
        if ('' !== ($resourceKeysArrayErrorMessage = self::validateResourceKeysForArrayConstraintFromSetResourceKeys($resourceKeys))) {
            throw new InvalidArgumentException($resourceKeysArrayErrorMessage, __LINE__);
        }
        if (is_null($resourceKeys) || (is_array($resourceKeys) && empty($resourceKeys))) {
            unset($this->resourceKeys);
        } else {
            $this->resourceKeys = $resourceKeys;
        }
        
        return $this;
    }
    /**
     * Add item to resourceKeys value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\CompositeKey $item
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function addToResourceKeys(\Booking\StructType\CompositeKey $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\CompositeKey) {
            throw new InvalidArgumentException(sprintf('The resourceKeys property can only contain items of type \Booking\StructType\CompositeKey, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->resourceKeys[] = $item;
        
        return $this;
    }
    /**
     * Get staffPersonKeys value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\ApiPersonKey[]
     */
    public function getStaffPersonKeys(): ?array
    {
        return $this->staffPersonKeys ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setStaffPersonKeys method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStaffPersonKeys method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStaffPersonKeysForArrayConstraintFromSetStaffPersonKeys(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $bookingChangeActivityParameterStaffPersonKeysItem) {
            // validation for constraint: itemType
            if (!$bookingChangeActivityParameterStaffPersonKeysItem instanceof \Booking\StructType\ApiPersonKey) {
                $invalidValues[] = is_object($bookingChangeActivityParameterStaffPersonKeysItem) ? get_class($bookingChangeActivityParameterStaffPersonKeysItem) : sprintf('%s(%s)', gettype($bookingChangeActivityParameterStaffPersonKeysItem), var_export($bookingChangeActivityParameterStaffPersonKeysItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The staffPersonKeys property can only contain items of type \Booking\StructType\ApiPersonKey, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set staffPersonKeys value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\ApiPersonKey[] $staffPersonKeys
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function setStaffPersonKeys(?array $staffPersonKeys = null): self
    {
        // validation for constraint: array
        if ('' !== ($staffPersonKeysArrayErrorMessage = self::validateStaffPersonKeysForArrayConstraintFromSetStaffPersonKeys($staffPersonKeys))) {
            throw new InvalidArgumentException($staffPersonKeysArrayErrorMessage, __LINE__);
        }
        if (is_null($staffPersonKeys) || (is_array($staffPersonKeys) && empty($staffPersonKeys))) {
            unset($this->staffPersonKeys);
        } else {
            $this->staffPersonKeys = $staffPersonKeys;
        }
        
        return $this;
    }
    /**
     * Add item to staffPersonKeys value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\ApiPersonKey $item
     * @return \Booking\StructType\BookingChangeActivityParameter
     */
    public function addToStaffPersonKeys(\Booking\StructType\ApiPersonKey $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\ApiPersonKey) {
            throw new InvalidArgumentException(sprintf('The staffPersonKeys property can only contain items of type \Booking\StructType\ApiPersonKey, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->staffPersonKeys[] = $item;
        
        return $this;
    }
}
