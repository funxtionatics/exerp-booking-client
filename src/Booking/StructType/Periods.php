<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for periods StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Periods extends AbstractStructBase
{
    /**
     * The period
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * @var string[]
     */
    protected ?array $period = null;
    /**
     * Constructor method for periods
     * @uses Periods::setPeriod()
     * @param string[] $period
     */
    public function __construct(?array $period = null)
    {
        $this
            ->setPeriod($period);
    }
    /**
     * Get period value
     * @return string[]
     */
    public function getPeriod(): ?array
    {
        return $this->period;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setPeriod method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPeriod method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePeriodForArrayConstraintFromSetPeriod(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $periodsPeriodItem) {
            // validation for constraint: enumeration
            if (!\Booking\EnumType\BookingRestrictionPeriod::valueIsValid($periodsPeriodItem)) {
                $invalidValues[] = is_object($periodsPeriodItem) ? get_class($periodsPeriodItem) : sprintf('%s(%s)', gettype($periodsPeriodItem), var_export($periodsPeriodItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\BookingRestrictionPeriod', is_array($invalidValues) ? implode(', ', $invalidValues) : var_export($invalidValues, true), implode(', ', \Booking\EnumType\BookingRestrictionPeriod::getValidValues()));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set period value
     * @uses \Booking\EnumType\BookingRestrictionPeriod::valueIsValid()
     * @uses \Booking\EnumType\BookingRestrictionPeriod::getValidValues()
     * @throws InvalidArgumentException
     * @param string[] $period
     * @return \Booking\StructType\Periods
     */
    public function setPeriod(?array $period = null): self
    {
        // validation for constraint: array
        if ('' !== ($periodArrayErrorMessage = self::validatePeriodForArrayConstraintFromSetPeriod($period))) {
            throw new InvalidArgumentException($periodArrayErrorMessage, __LINE__);
        }
        $this->period = $period;
        
        return $this;
    }
    /**
     * Add item to period value
     * @uses \Booking\EnumType\BookingRestrictionPeriod::valueIsValid()
     * @uses \Booking\EnumType\BookingRestrictionPeriod::getValidValues()
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Booking\StructType\Periods
     */
    public function addToPeriod(string $item): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\BookingRestrictionPeriod::valueIsValid($item)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\BookingRestrictionPeriod', is_array($item) ? implode(', ', $item) : var_export($item, true), implode(', ', \Booking\EnumType\BookingRestrictionPeriod::getValidValues())), __LINE__);
        }
        $this->period[] = $item;
        
        return $this;
    }
}
