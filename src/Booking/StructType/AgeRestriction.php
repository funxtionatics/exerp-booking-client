<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ageRestriction StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AgeRestriction extends AbstractStructBase
{
    /**
     * The min
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $min = null;
    /**
     * The max
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $max = null;
    /**
     * The minStrict
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $minStrict = null;
    /**
     * Constructor method for ageRestriction
     * @uses AgeRestriction::setMin()
     * @uses AgeRestriction::setMax()
     * @uses AgeRestriction::setMinStrict()
     * @param int $min
     * @param int $max
     * @param bool $minStrict
     */
    public function __construct(?int $min = null, ?int $max = null, ?bool $minStrict = null)
    {
        $this
            ->setMin($min)
            ->setMax($max)
            ->setMinStrict($minStrict);
    }
    /**
     * Get min value
     * @return int|null
     */
    public function getMin(): ?int
    {
        return $this->min;
    }
    /**
     * Set min value
     * @param int $min
     * @return \Booking\StructType\AgeRestriction
     */
    public function setMin(?int $min = null): self
    {
        // validation for constraint: int
        if (!is_null($min) && !(is_int($min) || ctype_digit($min))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($min, true), gettype($min)), __LINE__);
        }
        $this->min = $min;
        
        return $this;
    }
    /**
     * Get max value
     * @return int|null
     */
    public function getMax(): ?int
    {
        return $this->max;
    }
    /**
     * Set max value
     * @param int $max
     * @return \Booking\StructType\AgeRestriction
     */
    public function setMax(?int $max = null): self
    {
        // validation for constraint: int
        if (!is_null($max) && !(is_int($max) || ctype_digit($max))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($max, true), gettype($max)), __LINE__);
        }
        $this->max = $max;
        
        return $this;
    }
    /**
     * Get minStrict value
     * @return bool|null
     */
    public function getMinStrict(): ?bool
    {
        return $this->minStrict;
    }
    /**
     * Set minStrict value
     * @param bool $minStrict
     * @return \Booking\StructType\AgeRestriction
     */
    public function setMinStrict(?bool $minStrict = null): self
    {
        // validation for constraint: boolean
        if (!is_null($minStrict) && !is_bool($minStrict)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($minStrict, true), gettype($minStrict)), __LINE__);
        }
        $this->minStrict = $minStrict;
        
        return $this;
    }
}
