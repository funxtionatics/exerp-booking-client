<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for booking StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Booking extends AbstractStructBase
{
    /**
     * The activity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\Activity|null
     */
    protected ?\Booking\StructType\Activity $activity = null;
    /**
     * The ageRestriction
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\AgeRestriction|null
     */
    protected ?\Booking\StructType\AgeRestriction $ageRestriction = null;
    /**
     * The bookedCount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $bookedCount = null;
    /**
     * The bookingId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $bookingId = null;
    /**
     * The bookingProgramId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $bookingProgramId = null;
    /**
     * The bookingProgramName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $bookingProgramName = null;
    /**
     * The classCapacity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $classCapacity = null;
    /**
     * The date
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $date = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The endTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $endTime = null;
    /**
     * The energyConsumptionKcal
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $energyConsumptionKcal = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The hexColor
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $hexColor = null;
    /**
     * The instructors
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\Instructors|null
     */
    protected ?\Booking\StructType\Instructors $instructors = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The startTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $startTime = null;
    /**
     * The streamingId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $streamingId = null;
    /**
     * The waitingListCount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $waitingListCount = null;
    /**
     * The instructorNames
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var string[]
     */
    protected ?array $instructorNames = null;
    /**
     * The roomNames
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var string[]
     */
    protected ?array $roomNames = null;
    /**
     * The webRoomNames
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var string[]
     */
    protected ?array $webRoomNames = null;
    /**
     * Constructor method for booking
     * @uses Booking::setActivity()
     * @uses Booking::setAgeRestriction()
     * @uses Booking::setBookedCount()
     * @uses Booking::setBookingId()
     * @uses Booking::setBookingProgramId()
     * @uses Booking::setBookingProgramName()
     * @uses Booking::setClassCapacity()
     * @uses Booking::setDate()
     * @uses Booking::setDescription()
     * @uses Booking::setEndTime()
     * @uses Booking::setEnergyConsumptionKcal()
     * @uses Booking::setExternalId()
     * @uses Booking::setHexColor()
     * @uses Booking::setInstructors()
     * @uses Booking::setName()
     * @uses Booking::setStartTime()
     * @uses Booking::setStreamingId()
     * @uses Booking::setWaitingListCount()
     * @uses Booking::setInstructorNames()
     * @uses Booking::setRoomNames()
     * @uses Booking::setWebRoomNames()
     * @param \Booking\StructType\Activity $activity
     * @param \Booking\StructType\AgeRestriction $ageRestriction
     * @param int $bookedCount
     * @param \Booking\StructType\CompositeKey $bookingId
     * @param int $bookingProgramId
     * @param string $bookingProgramName
     * @param int $classCapacity
     * @param string $date
     * @param string $description
     * @param string $endTime
     * @param float $energyConsumptionKcal
     * @param string $externalId
     * @param string $hexColor
     * @param \Booking\StructType\Instructors $instructors
     * @param string $name
     * @param string $startTime
     * @param string $streamingId
     * @param int $waitingListCount
     * @param string[] $instructorNames
     * @param string[] $roomNames
     * @param string[] $webRoomNames
     */
    public function __construct(?\Booking\StructType\Activity $activity = null, ?\Booking\StructType\AgeRestriction $ageRestriction = null, ?int $bookedCount = null, ?\Booking\StructType\CompositeKey $bookingId = null, ?int $bookingProgramId = null, ?string $bookingProgramName = null, ?int $classCapacity = null, ?string $date = null, ?string $description = null, ?string $endTime = null, ?float $energyConsumptionKcal = null, ?string $externalId = null, ?string $hexColor = null, ?\Booking\StructType\Instructors $instructors = null, ?string $name = null, ?string $startTime = null, ?string $streamingId = null, ?int $waitingListCount = null, ?array $instructorNames = null, ?array $roomNames = null, ?array $webRoomNames = null)
    {
        $this
            ->setActivity($activity)
            ->setAgeRestriction($ageRestriction)
            ->setBookedCount($bookedCount)
            ->setBookingId($bookingId)
            ->setBookingProgramId($bookingProgramId)
            ->setBookingProgramName($bookingProgramName)
            ->setClassCapacity($classCapacity)
            ->setDate($date)
            ->setDescription($description)
            ->setEndTime($endTime)
            ->setEnergyConsumptionKcal($energyConsumptionKcal)
            ->setExternalId($externalId)
            ->setHexColor($hexColor)
            ->setInstructors($instructors)
            ->setName($name)
            ->setStartTime($startTime)
            ->setStreamingId($streamingId)
            ->setWaitingListCount($waitingListCount)
            ->setInstructorNames($instructorNames)
            ->setRoomNames($roomNames)
            ->setWebRoomNames($webRoomNames);
    }
    /**
     * Get activity value
     * @return \Booking\StructType\Activity|null
     */
    public function getActivity(): ?\Booking\StructType\Activity
    {
        return $this->activity;
    }
    /**
     * Set activity value
     * @param \Booking\StructType\Activity $activity
     * @return \Booking\StructType\Booking
     */
    public function setActivity(?\Booking\StructType\Activity $activity = null): self
    {
        $this->activity = $activity;
        
        return $this;
    }
    /**
     * Get ageRestriction value
     * @return \Booking\StructType\AgeRestriction|null
     */
    public function getAgeRestriction(): ?\Booking\StructType\AgeRestriction
    {
        return $this->ageRestriction;
    }
    /**
     * Set ageRestriction value
     * @param \Booking\StructType\AgeRestriction $ageRestriction
     * @return \Booking\StructType\Booking
     */
    public function setAgeRestriction(?\Booking\StructType\AgeRestriction $ageRestriction = null): self
    {
        $this->ageRestriction = $ageRestriction;
        
        return $this;
    }
    /**
     * Get bookedCount value
     * @return int|null
     */
    public function getBookedCount(): ?int
    {
        return $this->bookedCount;
    }
    /**
     * Set bookedCount value
     * @param int $bookedCount
     * @return \Booking\StructType\Booking
     */
    public function setBookedCount(?int $bookedCount = null): self
    {
        // validation for constraint: int
        if (!is_null($bookedCount) && !(is_int($bookedCount) || ctype_digit($bookedCount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($bookedCount, true), gettype($bookedCount)), __LINE__);
        }
        $this->bookedCount = $bookedCount;
        
        return $this;
    }
    /**
     * Get bookingId value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getBookingId(): ?\Booking\StructType\CompositeKey
    {
        return $this->bookingId;
    }
    /**
     * Set bookingId value
     * @param \Booking\StructType\CompositeKey $bookingId
     * @return \Booking\StructType\Booking
     */
    public function setBookingId(?\Booking\StructType\CompositeKey $bookingId = null): self
    {
        $this->bookingId = $bookingId;
        
        return $this;
    }
    /**
     * Get bookingProgramId value
     * @return int|null
     */
    public function getBookingProgramId(): ?int
    {
        return $this->bookingProgramId;
    }
    /**
     * Set bookingProgramId value
     * @param int $bookingProgramId
     * @return \Booking\StructType\Booking
     */
    public function setBookingProgramId(?int $bookingProgramId = null): self
    {
        // validation for constraint: int
        if (!is_null($bookingProgramId) && !(is_int($bookingProgramId) || ctype_digit($bookingProgramId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($bookingProgramId, true), gettype($bookingProgramId)), __LINE__);
        }
        $this->bookingProgramId = $bookingProgramId;
        
        return $this;
    }
    /**
     * Get bookingProgramName value
     * @return string|null
     */
    public function getBookingProgramName(): ?string
    {
        return $this->bookingProgramName;
    }
    /**
     * Set bookingProgramName value
     * @param string $bookingProgramName
     * @return \Booking\StructType\Booking
     */
    public function setBookingProgramName(?string $bookingProgramName = null): self
    {
        // validation for constraint: string
        if (!is_null($bookingProgramName) && !is_string($bookingProgramName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bookingProgramName, true), gettype($bookingProgramName)), __LINE__);
        }
        $this->bookingProgramName = $bookingProgramName;
        
        return $this;
    }
    /**
     * Get classCapacity value
     * @return int|null
     */
    public function getClassCapacity(): ?int
    {
        return $this->classCapacity;
    }
    /**
     * Set classCapacity value
     * @param int $classCapacity
     * @return \Booking\StructType\Booking
     */
    public function setClassCapacity(?int $classCapacity = null): self
    {
        // validation for constraint: int
        if (!is_null($classCapacity) && !(is_int($classCapacity) || ctype_digit($classCapacity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($classCapacity, true), gettype($classCapacity)), __LINE__);
        }
        $this->classCapacity = $classCapacity;
        
        return $this;
    }
    /**
     * Get date value
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }
    /**
     * Set date value
     * @param string $date
     * @return \Booking\StructType\Booking
     */
    public function setDate(?string $date = null): self
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->date = $date;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \Booking\StructType\Booking
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get endTime value
     * @return string|null
     */
    public function getEndTime(): ?string
    {
        return $this->endTime;
    }
    /**
     * Set endTime value
     * @param string $endTime
     * @return \Booking\StructType\Booking
     */
    public function setEndTime(?string $endTime = null): self
    {
        // validation for constraint: string
        if (!is_null($endTime) && !is_string($endTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endTime, true), gettype($endTime)), __LINE__);
        }
        $this->endTime = $endTime;
        
        return $this;
    }
    /**
     * Get energyConsumptionKcal value
     * @return float|null
     */
    public function getEnergyConsumptionKcal(): ?float
    {
        return $this->energyConsumptionKcal;
    }
    /**
     * Set energyConsumptionKcal value
     * @param float $energyConsumptionKcal
     * @return \Booking\StructType\Booking
     */
    public function setEnergyConsumptionKcal(?float $energyConsumptionKcal = null): self
    {
        // validation for constraint: float
        if (!is_null($energyConsumptionKcal) && !(is_float($energyConsumptionKcal) || is_numeric($energyConsumptionKcal))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($energyConsumptionKcal, true), gettype($energyConsumptionKcal)), __LINE__);
        }
        $this->energyConsumptionKcal = $energyConsumptionKcal;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Booking\StructType\Booking
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get hexColor value
     * @return string|null
     */
    public function getHexColor(): ?string
    {
        return $this->hexColor;
    }
    /**
     * Set hexColor value
     * @param string $hexColor
     * @return \Booking\StructType\Booking
     */
    public function setHexColor(?string $hexColor = null): self
    {
        // validation for constraint: string
        if (!is_null($hexColor) && !is_string($hexColor)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hexColor, true), gettype($hexColor)), __LINE__);
        }
        $this->hexColor = $hexColor;
        
        return $this;
    }
    /**
     * Get instructors value
     * @return \Booking\StructType\Instructors|null
     */
    public function getInstructors(): ?\Booking\StructType\Instructors
    {
        return $this->instructors;
    }
    /**
     * Set instructors value
     * @param \Booking\StructType\Instructors $instructors
     * @return \Booking\StructType\Booking
     */
    public function setInstructors(?\Booking\StructType\Instructors $instructors = null): self
    {
        $this->instructors = $instructors;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\Booking
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get startTime value
     * @return string|null
     */
    public function getStartTime(): ?string
    {
        return $this->startTime;
    }
    /**
     * Set startTime value
     * @param string $startTime
     * @return \Booking\StructType\Booking
     */
    public function setStartTime(?string $startTime = null): self
    {
        // validation for constraint: string
        if (!is_null($startTime) && !is_string($startTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startTime, true), gettype($startTime)), __LINE__);
        }
        $this->startTime = $startTime;
        
        return $this;
    }
    /**
     * Get streamingId value
     * @return string|null
     */
    public function getStreamingId(): ?string
    {
        return $this->streamingId;
    }
    /**
     * Set streamingId value
     * @param string $streamingId
     * @return \Booking\StructType\Booking
     */
    public function setStreamingId(?string $streamingId = null): self
    {
        // validation for constraint: string
        if (!is_null($streamingId) && !is_string($streamingId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($streamingId, true), gettype($streamingId)), __LINE__);
        }
        $this->streamingId = $streamingId;
        
        return $this;
    }
    /**
     * Get waitingListCount value
     * @return int|null
     */
    public function getWaitingListCount(): ?int
    {
        return $this->waitingListCount;
    }
    /**
     * Set waitingListCount value
     * @param int $waitingListCount
     * @return \Booking\StructType\Booking
     */
    public function setWaitingListCount(?int $waitingListCount = null): self
    {
        // validation for constraint: int
        if (!is_null($waitingListCount) && !(is_int($waitingListCount) || ctype_digit($waitingListCount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($waitingListCount, true), gettype($waitingListCount)), __LINE__);
        }
        $this->waitingListCount = $waitingListCount;
        
        return $this;
    }
    /**
     * Get instructorNames value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string[]
     */
    public function getInstructorNames(): ?array
    {
        return $this->instructorNames ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setInstructorNames method
     * This method is willingly generated in order to preserve the one-line inline validation within the setInstructorNames method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateInstructorNamesForArrayConstraintFromSetInstructorNames(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $bookingInstructorNamesItem) {
            // validation for constraint: itemType
            if (!is_string($bookingInstructorNamesItem)) {
                $invalidValues[] = is_object($bookingInstructorNamesItem) ? get_class($bookingInstructorNamesItem) : sprintf('%s(%s)', gettype($bookingInstructorNamesItem), var_export($bookingInstructorNamesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The instructorNames property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set instructorNames value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param string[] $instructorNames
     * @return \Booking\StructType\Booking
     */
    public function setInstructorNames(?array $instructorNames = null): self
    {
        // validation for constraint: array
        if ('' !== ($instructorNamesArrayErrorMessage = self::validateInstructorNamesForArrayConstraintFromSetInstructorNames($instructorNames))) {
            throw new InvalidArgumentException($instructorNamesArrayErrorMessage, __LINE__);
        }
        if (is_null($instructorNames) || (is_array($instructorNames) && empty($instructorNames))) {
            unset($this->instructorNames);
        } else {
            $this->instructorNames = $instructorNames;
        }
        
        return $this;
    }
    /**
     * Add item to instructorNames value
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Booking\StructType\Booking
     */
    public function addToInstructorNames(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf('The instructorNames property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->instructorNames[] = $item;
        
        return $this;
    }
    /**
     * Get roomNames value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string[]
     */
    public function getRoomNames(): ?array
    {
        return $this->roomNames ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setRoomNames method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRoomNames method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRoomNamesForArrayConstraintFromSetRoomNames(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $bookingRoomNamesItem) {
            // validation for constraint: itemType
            if (!is_string($bookingRoomNamesItem)) {
                $invalidValues[] = is_object($bookingRoomNamesItem) ? get_class($bookingRoomNamesItem) : sprintf('%s(%s)', gettype($bookingRoomNamesItem), var_export($bookingRoomNamesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The roomNames property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set roomNames value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param string[] $roomNames
     * @return \Booking\StructType\Booking
     */
    public function setRoomNames(?array $roomNames = null): self
    {
        // validation for constraint: array
        if ('' !== ($roomNamesArrayErrorMessage = self::validateRoomNamesForArrayConstraintFromSetRoomNames($roomNames))) {
            throw new InvalidArgumentException($roomNamesArrayErrorMessage, __LINE__);
        }
        if (is_null($roomNames) || (is_array($roomNames) && empty($roomNames))) {
            unset($this->roomNames);
        } else {
            $this->roomNames = $roomNames;
        }
        
        return $this;
    }
    /**
     * Add item to roomNames value
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Booking\StructType\Booking
     */
    public function addToRoomNames(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf('The roomNames property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->roomNames[] = $item;
        
        return $this;
    }
    /**
     * Get webRoomNames value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string[]
     */
    public function getWebRoomNames(): ?array
    {
        return $this->webRoomNames ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setWebRoomNames method
     * This method is willingly generated in order to preserve the one-line inline validation within the setWebRoomNames method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateWebRoomNamesForArrayConstraintFromSetWebRoomNames(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $bookingWebRoomNamesItem) {
            // validation for constraint: itemType
            if (!is_string($bookingWebRoomNamesItem)) {
                $invalidValues[] = is_object($bookingWebRoomNamesItem) ? get_class($bookingWebRoomNamesItem) : sprintf('%s(%s)', gettype($bookingWebRoomNamesItem), var_export($bookingWebRoomNamesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The webRoomNames property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set webRoomNames value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param string[] $webRoomNames
     * @return \Booking\StructType\Booking
     */
    public function setWebRoomNames(?array $webRoomNames = null): self
    {
        // validation for constraint: array
        if ('' !== ($webRoomNamesArrayErrorMessage = self::validateWebRoomNamesForArrayConstraintFromSetWebRoomNames($webRoomNames))) {
            throw new InvalidArgumentException($webRoomNamesArrayErrorMessage, __LINE__);
        }
        if (is_null($webRoomNames) || (is_array($webRoomNames) && empty($webRoomNames))) {
            unset($this->webRoomNames);
        } else {
            $this->webRoomNames = $webRoomNames;
        }
        
        return $this;
    }
    /**
     * Add item to webRoomNames value
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Booking\StructType\Booking
     */
    public function addToWebRoomNames(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf('The webRoomNames property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->webRoomNames[] = $item;
        
        return $this;
    }
}
