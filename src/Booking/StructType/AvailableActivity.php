<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for availableActivity StructType
 * Meta information extracted from the WSDL
 * - final: extension restriction
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AvailableActivity extends AbstractStructBase
{
    /**
     * The activityGroup
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ActivityGroup|null
     */
    protected ?\Booking\StructType\ActivityGroup $activityGroup = null;
    /**
     * The activityId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $activityId = null;
    /**
     * The activityType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $activityType = null;
    /**
     * The ageGroup
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\AgeGroup|null
     */
    protected ?\Booking\StructType\AgeGroup $ageGroup = null;
    /**
     * The colorGroup
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ColorGroup|null
     */
    protected ?\Booking\StructType\ColorGroup $colorGroup = null;
    /**
     * The courseConfiguration
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CourseConfiguration|null
     */
    protected ?\Booking\StructType\CourseConfiguration $courseConfiguration = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The allowedDurations
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var string[]
     */
    protected ?array $allowedDurations = null;
    /**
     * The resourceGroupExternalIds
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var string[]
     */
    protected ?array $resourceGroupExternalIds = null;
    /**
     * Constructor method for availableActivity
     * @uses AvailableActivity::setActivityGroup()
     * @uses AvailableActivity::setActivityId()
     * @uses AvailableActivity::setActivityType()
     * @uses AvailableActivity::setAgeGroup()
     * @uses AvailableActivity::setColorGroup()
     * @uses AvailableActivity::setCourseConfiguration()
     * @uses AvailableActivity::setDescription()
     * @uses AvailableActivity::setName()
     * @uses AvailableActivity::setAllowedDurations()
     * @uses AvailableActivity::setResourceGroupExternalIds()
     * @param \Booking\StructType\ActivityGroup $activityGroup
     * @param int $activityId
     * @param string $activityType
     * @param \Booking\StructType\AgeGroup $ageGroup
     * @param \Booking\StructType\ColorGroup $colorGroup
     * @param \Booking\StructType\CourseConfiguration $courseConfiguration
     * @param string $description
     * @param string $name
     * @param string[] $allowedDurations
     * @param string[] $resourceGroupExternalIds
     */
    public function __construct(?\Booking\StructType\ActivityGroup $activityGroup = null, ?int $activityId = null, ?string $activityType = null, ?\Booking\StructType\AgeGroup $ageGroup = null, ?\Booking\StructType\ColorGroup $colorGroup = null, ?\Booking\StructType\CourseConfiguration $courseConfiguration = null, ?string $description = null, ?string $name = null, ?array $allowedDurations = null, ?array $resourceGroupExternalIds = null)
    {
        $this
            ->setActivityGroup($activityGroup)
            ->setActivityId($activityId)
            ->setActivityType($activityType)
            ->setAgeGroup($ageGroup)
            ->setColorGroup($colorGroup)
            ->setCourseConfiguration($courseConfiguration)
            ->setDescription($description)
            ->setName($name)
            ->setAllowedDurations($allowedDurations)
            ->setResourceGroupExternalIds($resourceGroupExternalIds);
    }
    /**
     * Get activityGroup value
     * @return \Booking\StructType\ActivityGroup|null
     */
    public function getActivityGroup(): ?\Booking\StructType\ActivityGroup
    {
        return $this->activityGroup;
    }
    /**
     * Set activityGroup value
     * @param \Booking\StructType\ActivityGroup $activityGroup
     * @return \Booking\StructType\AvailableActivity
     */
    public function setActivityGroup(?\Booking\StructType\ActivityGroup $activityGroup = null): self
    {
        $this->activityGroup = $activityGroup;
        
        return $this;
    }
    /**
     * Get activityId value
     * @return int|null
     */
    public function getActivityId(): ?int
    {
        return $this->activityId;
    }
    /**
     * Set activityId value
     * @param int $activityId
     * @return \Booking\StructType\AvailableActivity
     */
    public function setActivityId(?int $activityId = null): self
    {
        // validation for constraint: int
        if (!is_null($activityId) && !(is_int($activityId) || ctype_digit($activityId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($activityId, true), gettype($activityId)), __LINE__);
        }
        $this->activityId = $activityId;
        
        return $this;
    }
    /**
     * Get activityType value
     * @return string|null
     */
    public function getActivityType(): ?string
    {
        return $this->activityType;
    }
    /**
     * Set activityType value
     * @uses \Booking\EnumType\ActivityType::valueIsValid()
     * @uses \Booking\EnumType\ActivityType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $activityType
     * @return \Booking\StructType\AvailableActivity
     */
    public function setActivityType(?string $activityType = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\ActivityType::valueIsValid($activityType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\ActivityType', is_array($activityType) ? implode(', ', $activityType) : var_export($activityType, true), implode(', ', \Booking\EnumType\ActivityType::getValidValues())), __LINE__);
        }
        $this->activityType = $activityType;
        
        return $this;
    }
    /**
     * Get ageGroup value
     * @return \Booking\StructType\AgeGroup|null
     */
    public function getAgeGroup(): ?\Booking\StructType\AgeGroup
    {
        return $this->ageGroup;
    }
    /**
     * Set ageGroup value
     * @param \Booking\StructType\AgeGroup $ageGroup
     * @return \Booking\StructType\AvailableActivity
     */
    public function setAgeGroup(?\Booking\StructType\AgeGroup $ageGroup = null): self
    {
        $this->ageGroup = $ageGroup;
        
        return $this;
    }
    /**
     * Get colorGroup value
     * @return \Booking\StructType\ColorGroup|null
     */
    public function getColorGroup(): ?\Booking\StructType\ColorGroup
    {
        return $this->colorGroup;
    }
    /**
     * Set colorGroup value
     * @param \Booking\StructType\ColorGroup $colorGroup
     * @return \Booking\StructType\AvailableActivity
     */
    public function setColorGroup(?\Booking\StructType\ColorGroup $colorGroup = null): self
    {
        $this->colorGroup = $colorGroup;
        
        return $this;
    }
    /**
     * Get courseConfiguration value
     * @return \Booking\StructType\CourseConfiguration|null
     */
    public function getCourseConfiguration(): ?\Booking\StructType\CourseConfiguration
    {
        return $this->courseConfiguration;
    }
    /**
     * Set courseConfiguration value
     * @param \Booking\StructType\CourseConfiguration $courseConfiguration
     * @return \Booking\StructType\AvailableActivity
     */
    public function setCourseConfiguration(?\Booking\StructType\CourseConfiguration $courseConfiguration = null): self
    {
        $this->courseConfiguration = $courseConfiguration;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \Booking\StructType\AvailableActivity
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\AvailableActivity
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get allowedDurations value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string[]
     */
    public function getAllowedDurations(): ?array
    {
        return $this->allowedDurations ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setAllowedDurations method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAllowedDurations method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAllowedDurationsForArrayConstraintFromSetAllowedDurations(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $availableActivityAllowedDurationsItem) {
            // validation for constraint: itemType
            if (!is_string($availableActivityAllowedDurationsItem)) {
                $invalidValues[] = is_object($availableActivityAllowedDurationsItem) ? get_class($availableActivityAllowedDurationsItem) : sprintf('%s(%s)', gettype($availableActivityAllowedDurationsItem), var_export($availableActivityAllowedDurationsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The allowedDurations property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set allowedDurations value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param string[] $allowedDurations
     * @return \Booking\StructType\AvailableActivity
     */
    public function setAllowedDurations(?array $allowedDurations = null): self
    {
        // validation for constraint: array
        if ('' !== ($allowedDurationsArrayErrorMessage = self::validateAllowedDurationsForArrayConstraintFromSetAllowedDurations($allowedDurations))) {
            throw new InvalidArgumentException($allowedDurationsArrayErrorMessage, __LINE__);
        }
        if (is_null($allowedDurations) || (is_array($allowedDurations) && empty($allowedDurations))) {
            unset($this->allowedDurations);
        } else {
            $this->allowedDurations = $allowedDurations;
        }
        
        return $this;
    }
    /**
     * Add item to allowedDurations value
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Booking\StructType\AvailableActivity
     */
    public function addToAllowedDurations(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf('The allowedDurations property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->allowedDurations[] = $item;
        
        return $this;
    }
    /**
     * Get resourceGroupExternalIds value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string[]
     */
    public function getResourceGroupExternalIds(): ?array
    {
        return $this->resourceGroupExternalIds ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setResourceGroupExternalIds method
     * This method is willingly generated in order to preserve the one-line inline validation within the setResourceGroupExternalIds method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateResourceGroupExternalIdsForArrayConstraintFromSetResourceGroupExternalIds(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $availableActivityResourceGroupExternalIdsItem) {
            // validation for constraint: itemType
            if (!is_string($availableActivityResourceGroupExternalIdsItem)) {
                $invalidValues[] = is_object($availableActivityResourceGroupExternalIdsItem) ? get_class($availableActivityResourceGroupExternalIdsItem) : sprintf('%s(%s)', gettype($availableActivityResourceGroupExternalIdsItem), var_export($availableActivityResourceGroupExternalIdsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The resourceGroupExternalIds property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set resourceGroupExternalIds value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param string[] $resourceGroupExternalIds
     * @return \Booking\StructType\AvailableActivity
     */
    public function setResourceGroupExternalIds(?array $resourceGroupExternalIds = null): self
    {
        // validation for constraint: array
        if ('' !== ($resourceGroupExternalIdsArrayErrorMessage = self::validateResourceGroupExternalIdsForArrayConstraintFromSetResourceGroupExternalIds($resourceGroupExternalIds))) {
            throw new InvalidArgumentException($resourceGroupExternalIdsArrayErrorMessage, __LINE__);
        }
        if (is_null($resourceGroupExternalIds) || (is_array($resourceGroupExternalIds) && empty($resourceGroupExternalIds))) {
            unset($this->resourceGroupExternalIds);
        } else {
            $this->resourceGroupExternalIds = $resourceGroupExternalIds;
        }
        
        return $this;
    }
    /**
     * Add item to resourceGroupExternalIds value
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Booking\StructType\AvailableActivity
     */
    public function addToResourceGroupExternalIds(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf('The resourceGroupExternalIds property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->resourceGroupExternalIds[] = $item;
        
        return $this;
    }
}
