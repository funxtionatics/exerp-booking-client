<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createOrUpdateParticipationParams StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class CreateOrUpdateParticipationParams extends AbstractStructBase
{
    /**
     * The bookingId
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $bookingId = null;
    /**
     * The bookingOwnerPersonId
     * @var \Booking\StructType\ApiPersonKey|null
     */
    protected ?\Booking\StructType\ApiPersonKey $bookingOwnerPersonId = null;
    /**
     * The participants
     * @var \Booking\StructType\Participants|null
     */
    protected ?\Booking\StructType\Participants $participants = null;
    /**
     * The paymentInfo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\PaymentInfo|null
     */
    protected ?\Booking\StructType\PaymentInfo $paymentInfo = null;
    /**
     * The productToPurchaseKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $productToPurchaseKey = null;
    /**
     * The sendConfirmationMessage
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $sendConfirmationMessage = null;
    /**
     * The userInterfaceType
     * @var string|null
     */
    protected ?string $userInterfaceType = null;
    /**
     * Constructor method for createOrUpdateParticipationParams
     * @uses CreateOrUpdateParticipationParams::setBookingId()
     * @uses CreateOrUpdateParticipationParams::setBookingOwnerPersonId()
     * @uses CreateOrUpdateParticipationParams::setParticipants()
     * @uses CreateOrUpdateParticipationParams::setPaymentInfo()
     * @uses CreateOrUpdateParticipationParams::setProductToPurchaseKey()
     * @uses CreateOrUpdateParticipationParams::setSendConfirmationMessage()
     * @uses CreateOrUpdateParticipationParams::setUserInterfaceType()
     * @param \Booking\StructType\CompositeKey $bookingId
     * @param \Booking\StructType\ApiPersonKey $bookingOwnerPersonId
     * @param \Booking\StructType\Participants $participants
     * @param \Booking\StructType\PaymentInfo $paymentInfo
     * @param \Booking\StructType\CompositeKey $productToPurchaseKey
     * @param bool $sendConfirmationMessage
     * @param string $userInterfaceType
     */
    public function __construct(?\Booking\StructType\CompositeKey $bookingId = null, ?\Booking\StructType\ApiPersonKey $bookingOwnerPersonId = null, ?\Booking\StructType\Participants $participants = null, ?\Booking\StructType\PaymentInfo $paymentInfo = null, ?\Booking\StructType\CompositeKey $productToPurchaseKey = null, ?bool $sendConfirmationMessage = null, ?string $userInterfaceType = null)
    {
        $this
            ->setBookingId($bookingId)
            ->setBookingOwnerPersonId($bookingOwnerPersonId)
            ->setParticipants($participants)
            ->setPaymentInfo($paymentInfo)
            ->setProductToPurchaseKey($productToPurchaseKey)
            ->setSendConfirmationMessage($sendConfirmationMessage)
            ->setUserInterfaceType($userInterfaceType);
    }
    /**
     * Get bookingId value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getBookingId(): ?\Booking\StructType\CompositeKey
    {
        return $this->bookingId;
    }
    /**
     * Set bookingId value
     * @param \Booking\StructType\CompositeKey $bookingId
     * @return \Booking\StructType\CreateOrUpdateParticipationParams
     */
    public function setBookingId(?\Booking\StructType\CompositeKey $bookingId = null): self
    {
        $this->bookingId = $bookingId;
        
        return $this;
    }
    /**
     * Get bookingOwnerPersonId value
     * @return \Booking\StructType\ApiPersonKey|null
     */
    public function getBookingOwnerPersonId(): ?\Booking\StructType\ApiPersonKey
    {
        return $this->bookingOwnerPersonId;
    }
    /**
     * Set bookingOwnerPersonId value
     * @param \Booking\StructType\ApiPersonKey $bookingOwnerPersonId
     * @return \Booking\StructType\CreateOrUpdateParticipationParams
     */
    public function setBookingOwnerPersonId(?\Booking\StructType\ApiPersonKey $bookingOwnerPersonId = null): self
    {
        $this->bookingOwnerPersonId = $bookingOwnerPersonId;
        
        return $this;
    }
    /**
     * Get participants value
     * @return \Booking\StructType\Participants|null
     */
    public function getParticipants(): ?\Booking\StructType\Participants
    {
        return $this->participants;
    }
    /**
     * Set participants value
     * @param \Booking\StructType\Participants $participants
     * @return \Booking\StructType\CreateOrUpdateParticipationParams
     */
    public function setParticipants(?\Booking\StructType\Participants $participants = null): self
    {
        $this->participants = $participants;
        
        return $this;
    }
    /**
     * Get paymentInfo value
     * @return \Booking\StructType\PaymentInfo|null
     */
    public function getPaymentInfo(): ?\Booking\StructType\PaymentInfo
    {
        return $this->paymentInfo;
    }
    /**
     * Set paymentInfo value
     * @param \Booking\StructType\PaymentInfo $paymentInfo
     * @return \Booking\StructType\CreateOrUpdateParticipationParams
     */
    public function setPaymentInfo(?\Booking\StructType\PaymentInfo $paymentInfo = null): self
    {
        $this->paymentInfo = $paymentInfo;
        
        return $this;
    }
    /**
     * Get productToPurchaseKey value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getProductToPurchaseKey(): ?\Booking\StructType\CompositeKey
    {
        return $this->productToPurchaseKey;
    }
    /**
     * Set productToPurchaseKey value
     * @param \Booking\StructType\CompositeKey $productToPurchaseKey
     * @return \Booking\StructType\CreateOrUpdateParticipationParams
     */
    public function setProductToPurchaseKey(?\Booking\StructType\CompositeKey $productToPurchaseKey = null): self
    {
        $this->productToPurchaseKey = $productToPurchaseKey;
        
        return $this;
    }
    /**
     * Get sendConfirmationMessage value
     * @return bool|null
     */
    public function getSendConfirmationMessage(): ?bool
    {
        return $this->sendConfirmationMessage;
    }
    /**
     * Set sendConfirmationMessage value
     * @param bool $sendConfirmationMessage
     * @return \Booking\StructType\CreateOrUpdateParticipationParams
     */
    public function setSendConfirmationMessage(?bool $sendConfirmationMessage = null): self
    {
        // validation for constraint: boolean
        if (!is_null($sendConfirmationMessage) && !is_bool($sendConfirmationMessage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($sendConfirmationMessage, true), gettype($sendConfirmationMessage)), __LINE__);
        }
        $this->sendConfirmationMessage = $sendConfirmationMessage;
        
        return $this;
    }
    /**
     * Get userInterfaceType value
     * @return string|null
     */
    public function getUserInterfaceType(): ?string
    {
        return $this->userInterfaceType;
    }
    /**
     * Set userInterfaceType value
     * @uses \Booking\EnumType\UserInterfaceType::valueIsValid()
     * @uses \Booking\EnumType\UserInterfaceType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $userInterfaceType
     * @return \Booking\StructType\CreateOrUpdateParticipationParams
     */
    public function setUserInterfaceType(?string $userInterfaceType = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\UserInterfaceType::valueIsValid($userInterfaceType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\UserInterfaceType', is_array($userInterfaceType) ? implode(', ', $userInterfaceType) : var_export($userInterfaceType, true), implode(', ', \Booking\EnumType\UserInterfaceType::getValidValues())), __LINE__);
        }
        $this->userInterfaceType = $userInterfaceType;
        
        return $this;
    }
}
