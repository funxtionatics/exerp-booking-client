<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for bookingDetails StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class BookingDetails extends AbstractStructBase
{
    /**
     * The activity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\Activity|null
     */
    protected ?\Booking\StructType\Activity $activity = null;
    /**
     * The additionalInfo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $additionalInfo = null;
    /**
     * The ageRestriction
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\AgeRestriction|null
     */
    protected ?\Booking\StructType\AgeRestriction $ageRestriction = null;
    /**
     * The bookedCount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $bookedCount = null;
    /**
     * The bookingId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $bookingId = null;
    /**
     * The bookingProgramId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $bookingProgramId = null;
    /**
     * The bookingProgramName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $bookingProgramName = null;
    /**
     * The classCapacity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $classCapacity = null;
    /**
     * The date
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $date = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The endTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $endTime = null;
    /**
     * The energyConsumptionKcal
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $energyConsumptionKcal = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The hexColor
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $hexColor = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The startTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $startTime = null;
    /**
     * The streamingId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $streamingId = null;
    /**
     * The waitingListCount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $waitingListCount = null;
    /**
     * The customAttributes
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\CustomAttribute[]
     */
    protected ?array $customAttributes = null;
    /**
     * The instructors
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\PersonSimple[]
     */
    protected ?array $instructors = null;
    /**
     * The participations
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\Participation[]
     */
    protected ?array $participations = null;
    /**
     * The resources
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\_resource[]
     */
    protected ?array $resources = null;
    /**
     * Constructor method for bookingDetails
     * @uses BookingDetails::setActivity()
     * @uses BookingDetails::setAdditionalInfo()
     * @uses BookingDetails::setAgeRestriction()
     * @uses BookingDetails::setBookedCount()
     * @uses BookingDetails::setBookingId()
     * @uses BookingDetails::setBookingProgramId()
     * @uses BookingDetails::setBookingProgramName()
     * @uses BookingDetails::setClassCapacity()
     * @uses BookingDetails::setDate()
     * @uses BookingDetails::setDescription()
     * @uses BookingDetails::setEndTime()
     * @uses BookingDetails::setEnergyConsumptionKcal()
     * @uses BookingDetails::setExternalId()
     * @uses BookingDetails::setHexColor()
     * @uses BookingDetails::setName()
     * @uses BookingDetails::setStartTime()
     * @uses BookingDetails::setStreamingId()
     * @uses BookingDetails::setWaitingListCount()
     * @uses BookingDetails::setCustomAttributes()
     * @uses BookingDetails::setInstructors()
     * @uses BookingDetails::setParticipations()
     * @uses BookingDetails::setResources()
     * @param \Booking\StructType\Activity $activity
     * @param string $additionalInfo
     * @param \Booking\StructType\AgeRestriction $ageRestriction
     * @param int $bookedCount
     * @param \Booking\StructType\CompositeKey $bookingId
     * @param int $bookingProgramId
     * @param string $bookingProgramName
     * @param int $classCapacity
     * @param string $date
     * @param string $description
     * @param string $endTime
     * @param float $energyConsumptionKcal
     * @param string $externalId
     * @param string $hexColor
     * @param string $name
     * @param string $startTime
     * @param string $streamingId
     * @param int $waitingListCount
     * @param \Booking\StructType\CustomAttribute[] $customAttributes
     * @param \Booking\StructType\PersonSimple[] $instructors
     * @param \Booking\StructType\Participation[] $participations
     * @param \Booking\StructType\_resource[] $resources
     */
    public function __construct(?\Booking\StructType\Activity $activity = null, ?string $additionalInfo = null, ?\Booking\StructType\AgeRestriction $ageRestriction = null, ?int $bookedCount = null, ?\Booking\StructType\CompositeKey $bookingId = null, ?int $bookingProgramId = null, ?string $bookingProgramName = null, ?int $classCapacity = null, ?string $date = null, ?string $description = null, ?string $endTime = null, ?float $energyConsumptionKcal = null, ?string $externalId = null, ?string $hexColor = null, ?string $name = null, ?string $startTime = null, ?string $streamingId = null, ?int $waitingListCount = null, ?array $customAttributes = null, ?array $instructors = null, ?array $participations = null, ?array $resources = null)
    {
        $this
            ->setActivity($activity)
            ->setAdditionalInfo($additionalInfo)
            ->setAgeRestriction($ageRestriction)
            ->setBookedCount($bookedCount)
            ->setBookingId($bookingId)
            ->setBookingProgramId($bookingProgramId)
            ->setBookingProgramName($bookingProgramName)
            ->setClassCapacity($classCapacity)
            ->setDate($date)
            ->setDescription($description)
            ->setEndTime($endTime)
            ->setEnergyConsumptionKcal($energyConsumptionKcal)
            ->setExternalId($externalId)
            ->setHexColor($hexColor)
            ->setName($name)
            ->setStartTime($startTime)
            ->setStreamingId($streamingId)
            ->setWaitingListCount($waitingListCount)
            ->setCustomAttributes($customAttributes)
            ->setInstructors($instructors)
            ->setParticipations($participations)
            ->setResources($resources);
    }
    /**
     * Get activity value
     * @return \Booking\StructType\Activity|null
     */
    public function getActivity(): ?\Booking\StructType\Activity
    {
        return $this->activity;
    }
    /**
     * Set activity value
     * @param \Booking\StructType\Activity $activity
     * @return \Booking\StructType\BookingDetails
     */
    public function setActivity(?\Booking\StructType\Activity $activity = null): self
    {
        $this->activity = $activity;
        
        return $this;
    }
    /**
     * Get additionalInfo value
     * @return string|null
     */
    public function getAdditionalInfo(): ?string
    {
        return $this->additionalInfo;
    }
    /**
     * Set additionalInfo value
     * @param string $additionalInfo
     * @return \Booking\StructType\BookingDetails
     */
    public function setAdditionalInfo(?string $additionalInfo = null): self
    {
        // validation for constraint: string
        if (!is_null($additionalInfo) && !is_string($additionalInfo)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalInfo, true), gettype($additionalInfo)), __LINE__);
        }
        $this->additionalInfo = $additionalInfo;
        
        return $this;
    }
    /**
     * Get ageRestriction value
     * @return \Booking\StructType\AgeRestriction|null
     */
    public function getAgeRestriction(): ?\Booking\StructType\AgeRestriction
    {
        return $this->ageRestriction;
    }
    /**
     * Set ageRestriction value
     * @param \Booking\StructType\AgeRestriction $ageRestriction
     * @return \Booking\StructType\BookingDetails
     */
    public function setAgeRestriction(?\Booking\StructType\AgeRestriction $ageRestriction = null): self
    {
        $this->ageRestriction = $ageRestriction;
        
        return $this;
    }
    /**
     * Get bookedCount value
     * @return int|null
     */
    public function getBookedCount(): ?int
    {
        return $this->bookedCount;
    }
    /**
     * Set bookedCount value
     * @param int $bookedCount
     * @return \Booking\StructType\BookingDetails
     */
    public function setBookedCount(?int $bookedCount = null): self
    {
        // validation for constraint: int
        if (!is_null($bookedCount) && !(is_int($bookedCount) || ctype_digit($bookedCount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($bookedCount, true), gettype($bookedCount)), __LINE__);
        }
        $this->bookedCount = $bookedCount;
        
        return $this;
    }
    /**
     * Get bookingId value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getBookingId(): ?\Booking\StructType\CompositeKey
    {
        return $this->bookingId;
    }
    /**
     * Set bookingId value
     * @param \Booking\StructType\CompositeKey $bookingId
     * @return \Booking\StructType\BookingDetails
     */
    public function setBookingId(?\Booking\StructType\CompositeKey $bookingId = null): self
    {
        $this->bookingId = $bookingId;
        
        return $this;
    }
    /**
     * Get bookingProgramId value
     * @return int|null
     */
    public function getBookingProgramId(): ?int
    {
        return $this->bookingProgramId;
    }
    /**
     * Set bookingProgramId value
     * @param int $bookingProgramId
     * @return \Booking\StructType\BookingDetails
     */
    public function setBookingProgramId(?int $bookingProgramId = null): self
    {
        // validation for constraint: int
        if (!is_null($bookingProgramId) && !(is_int($bookingProgramId) || ctype_digit($bookingProgramId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($bookingProgramId, true), gettype($bookingProgramId)), __LINE__);
        }
        $this->bookingProgramId = $bookingProgramId;
        
        return $this;
    }
    /**
     * Get bookingProgramName value
     * @return string|null
     */
    public function getBookingProgramName(): ?string
    {
        return $this->bookingProgramName;
    }
    /**
     * Set bookingProgramName value
     * @param string $bookingProgramName
     * @return \Booking\StructType\BookingDetails
     */
    public function setBookingProgramName(?string $bookingProgramName = null): self
    {
        // validation for constraint: string
        if (!is_null($bookingProgramName) && !is_string($bookingProgramName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bookingProgramName, true), gettype($bookingProgramName)), __LINE__);
        }
        $this->bookingProgramName = $bookingProgramName;
        
        return $this;
    }
    /**
     * Get classCapacity value
     * @return int|null
     */
    public function getClassCapacity(): ?int
    {
        return $this->classCapacity;
    }
    /**
     * Set classCapacity value
     * @param int $classCapacity
     * @return \Booking\StructType\BookingDetails
     */
    public function setClassCapacity(?int $classCapacity = null): self
    {
        // validation for constraint: int
        if (!is_null($classCapacity) && !(is_int($classCapacity) || ctype_digit($classCapacity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($classCapacity, true), gettype($classCapacity)), __LINE__);
        }
        $this->classCapacity = $classCapacity;
        
        return $this;
    }
    /**
     * Get date value
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }
    /**
     * Set date value
     * @param string $date
     * @return \Booking\StructType\BookingDetails
     */
    public function setDate(?string $date = null): self
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->date = $date;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \Booking\StructType\BookingDetails
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get endTime value
     * @return string|null
     */
    public function getEndTime(): ?string
    {
        return $this->endTime;
    }
    /**
     * Set endTime value
     * @param string $endTime
     * @return \Booking\StructType\BookingDetails
     */
    public function setEndTime(?string $endTime = null): self
    {
        // validation for constraint: string
        if (!is_null($endTime) && !is_string($endTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endTime, true), gettype($endTime)), __LINE__);
        }
        $this->endTime = $endTime;
        
        return $this;
    }
    /**
     * Get energyConsumptionKcal value
     * @return float|null
     */
    public function getEnergyConsumptionKcal(): ?float
    {
        return $this->energyConsumptionKcal;
    }
    /**
     * Set energyConsumptionKcal value
     * @param float $energyConsumptionKcal
     * @return \Booking\StructType\BookingDetails
     */
    public function setEnergyConsumptionKcal(?float $energyConsumptionKcal = null): self
    {
        // validation for constraint: float
        if (!is_null($energyConsumptionKcal) && !(is_float($energyConsumptionKcal) || is_numeric($energyConsumptionKcal))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($energyConsumptionKcal, true), gettype($energyConsumptionKcal)), __LINE__);
        }
        $this->energyConsumptionKcal = $energyConsumptionKcal;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Booking\StructType\BookingDetails
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get hexColor value
     * @return string|null
     */
    public function getHexColor(): ?string
    {
        return $this->hexColor;
    }
    /**
     * Set hexColor value
     * @param string $hexColor
     * @return \Booking\StructType\BookingDetails
     */
    public function setHexColor(?string $hexColor = null): self
    {
        // validation for constraint: string
        if (!is_null($hexColor) && !is_string($hexColor)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hexColor, true), gettype($hexColor)), __LINE__);
        }
        $this->hexColor = $hexColor;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\BookingDetails
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get startTime value
     * @return string|null
     */
    public function getStartTime(): ?string
    {
        return $this->startTime;
    }
    /**
     * Set startTime value
     * @param string $startTime
     * @return \Booking\StructType\BookingDetails
     */
    public function setStartTime(?string $startTime = null): self
    {
        // validation for constraint: string
        if (!is_null($startTime) && !is_string($startTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startTime, true), gettype($startTime)), __LINE__);
        }
        $this->startTime = $startTime;
        
        return $this;
    }
    /**
     * Get streamingId value
     * @return string|null
     */
    public function getStreamingId(): ?string
    {
        return $this->streamingId;
    }
    /**
     * Set streamingId value
     * @param string $streamingId
     * @return \Booking\StructType\BookingDetails
     */
    public function setStreamingId(?string $streamingId = null): self
    {
        // validation for constraint: string
        if (!is_null($streamingId) && !is_string($streamingId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($streamingId, true), gettype($streamingId)), __LINE__);
        }
        $this->streamingId = $streamingId;
        
        return $this;
    }
    /**
     * Get waitingListCount value
     * @return int|null
     */
    public function getWaitingListCount(): ?int
    {
        return $this->waitingListCount;
    }
    /**
     * Set waitingListCount value
     * @param int $waitingListCount
     * @return \Booking\StructType\BookingDetails
     */
    public function setWaitingListCount(?int $waitingListCount = null): self
    {
        // validation for constraint: int
        if (!is_null($waitingListCount) && !(is_int($waitingListCount) || ctype_digit($waitingListCount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($waitingListCount, true), gettype($waitingListCount)), __LINE__);
        }
        $this->waitingListCount = $waitingListCount;
        
        return $this;
    }
    /**
     * Get customAttributes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\CustomAttribute[]
     */
    public function getCustomAttributes(): ?array
    {
        return $this->customAttributes ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setCustomAttributes method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCustomAttributes method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCustomAttributesForArrayConstraintFromSetCustomAttributes(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $bookingDetailsCustomAttributesItem) {
            // validation for constraint: itemType
            if (!$bookingDetailsCustomAttributesItem instanceof \Booking\StructType\CustomAttribute) {
                $invalidValues[] = is_object($bookingDetailsCustomAttributesItem) ? get_class($bookingDetailsCustomAttributesItem) : sprintf('%s(%s)', gettype($bookingDetailsCustomAttributesItem), var_export($bookingDetailsCustomAttributesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The customAttributes property can only contain items of type \Booking\StructType\CustomAttribute, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set customAttributes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\CustomAttribute[] $customAttributes
     * @return \Booking\StructType\BookingDetails
     */
    public function setCustomAttributes(?array $customAttributes = null): self
    {
        // validation for constraint: array
        if ('' !== ($customAttributesArrayErrorMessage = self::validateCustomAttributesForArrayConstraintFromSetCustomAttributes($customAttributes))) {
            throw new InvalidArgumentException($customAttributesArrayErrorMessage, __LINE__);
        }
        if (is_null($customAttributes) || (is_array($customAttributes) && empty($customAttributes))) {
            unset($this->customAttributes);
        } else {
            $this->customAttributes = $customAttributes;
        }
        
        return $this;
    }
    /**
     * Add item to customAttributes value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\CustomAttribute $item
     * @return \Booking\StructType\BookingDetails
     */
    public function addToCustomAttributes(\Booking\StructType\CustomAttribute $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\CustomAttribute) {
            throw new InvalidArgumentException(sprintf('The customAttributes property can only contain items of type \Booking\StructType\CustomAttribute, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->customAttributes[] = $item;
        
        return $this;
    }
    /**
     * Get instructors value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\PersonSimple[]
     */
    public function getInstructors(): ?array
    {
        return $this->instructors ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setInstructors method
     * This method is willingly generated in order to preserve the one-line inline validation within the setInstructors method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateInstructorsForArrayConstraintFromSetInstructors(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $bookingDetailsInstructorsItem) {
            // validation for constraint: itemType
            if (!$bookingDetailsInstructorsItem instanceof \Booking\StructType\PersonSimple) {
                $invalidValues[] = is_object($bookingDetailsInstructorsItem) ? get_class($bookingDetailsInstructorsItem) : sprintf('%s(%s)', gettype($bookingDetailsInstructorsItem), var_export($bookingDetailsInstructorsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The instructors property can only contain items of type \Booking\StructType\PersonSimple, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set instructors value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\PersonSimple[] $instructors
     * @return \Booking\StructType\BookingDetails
     */
    public function setInstructors(?array $instructors = null): self
    {
        // validation for constraint: array
        if ('' !== ($instructorsArrayErrorMessage = self::validateInstructorsForArrayConstraintFromSetInstructors($instructors))) {
            throw new InvalidArgumentException($instructorsArrayErrorMessage, __LINE__);
        }
        if (is_null($instructors) || (is_array($instructors) && empty($instructors))) {
            unset($this->instructors);
        } else {
            $this->instructors = $instructors;
        }
        
        return $this;
    }
    /**
     * Add item to instructors value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\PersonSimple $item
     * @return \Booking\StructType\BookingDetails
     */
    public function addToInstructors(\Booking\StructType\PersonSimple $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\PersonSimple) {
            throw new InvalidArgumentException(sprintf('The instructors property can only contain items of type \Booking\StructType\PersonSimple, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->instructors[] = $item;
        
        return $this;
    }
    /**
     * Get participations value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\Participation[]
     */
    public function getParticipations(): ?array
    {
        return $this->participations ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setParticipations method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParticipations method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParticipationsForArrayConstraintFromSetParticipations(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $bookingDetailsParticipationsItem) {
            // validation for constraint: itemType
            if (!$bookingDetailsParticipationsItem instanceof \Booking\StructType\Participation) {
                $invalidValues[] = is_object($bookingDetailsParticipationsItem) ? get_class($bookingDetailsParticipationsItem) : sprintf('%s(%s)', gettype($bookingDetailsParticipationsItem), var_export($bookingDetailsParticipationsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The participations property can only contain items of type \Booking\StructType\Participation, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set participations value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\Participation[] $participations
     * @return \Booking\StructType\BookingDetails
     */
    public function setParticipations(?array $participations = null): self
    {
        // validation for constraint: array
        if ('' !== ($participationsArrayErrorMessage = self::validateParticipationsForArrayConstraintFromSetParticipations($participations))) {
            throw new InvalidArgumentException($participationsArrayErrorMessage, __LINE__);
        }
        if (is_null($participations) || (is_array($participations) && empty($participations))) {
            unset($this->participations);
        } else {
            $this->participations = $participations;
        }
        
        return $this;
    }
    /**
     * Add item to participations value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\Participation $item
     * @return \Booking\StructType\BookingDetails
     */
    public function addToParticipations(\Booking\StructType\Participation $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\Participation) {
            throw new InvalidArgumentException(sprintf('The participations property can only contain items of type \Booking\StructType\Participation, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->participations[] = $item;
        
        return $this;
    }
    /**
     * Get resources value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\_resource[]
     */
    public function getResources(): ?array
    {
        return $this->resources ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setResources method
     * This method is willingly generated in order to preserve the one-line inline validation within the setResources method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateResourcesForArrayConstraintFromSetResources(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $bookingDetailsResourcesItem) {
            // validation for constraint: itemType
            if (!$bookingDetailsResourcesItem instanceof \Booking\StructType\_resource) {
                $invalidValues[] = is_object($bookingDetailsResourcesItem) ? get_class($bookingDetailsResourcesItem) : sprintf('%s(%s)', gettype($bookingDetailsResourcesItem), var_export($bookingDetailsResourcesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The resources property can only contain items of type \Booking\StructType\_resource, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set resources value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\_resource[] $resources
     * @return \Booking\StructType\BookingDetails
     */
    public function setResources(?array $resources = null): self
    {
        // validation for constraint: array
        if ('' !== ($resourcesArrayErrorMessage = self::validateResourcesForArrayConstraintFromSetResources($resources))) {
            throw new InvalidArgumentException($resourcesArrayErrorMessage, __LINE__);
        }
        if (is_null($resources) || (is_array($resources) && empty($resources))) {
            unset($this->resources);
        } else {
            $this->resources = $resources;
        }
        
        return $this;
    }
    /**
     * Add item to resources value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\_resource $item
     * @return \Booking\StructType\BookingDetails
     */
    public function addToResources(\Booking\StructType\_resource $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\_resource) {
            throw new InvalidArgumentException(sprintf('The resources property can only contain items of type \Booking\StructType\_resource, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->resources[] = $item;
        
        return $this;
    }
}
