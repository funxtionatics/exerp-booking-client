<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for bookingProduct StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class BookingProduct extends AbstractStructBase
{
    /**
     * The key
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ApiProductKey|null
     */
    protected ?\Booking\StructType\ApiProductKey $key = null;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The price
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ProductPrice|null
     */
    protected ?\Booking\StructType\ProductPrice $price = null;
    /**
     * The creationPrice
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ProductPrice|null
     */
    protected ?\Booking\StructType\ProductPrice $creationPrice = null;
    /**
     * The periodLength
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $periodLength = null;
    /**
     * The periodUnit
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $periodUnit = null;
    /**
     * Constructor method for bookingProduct
     * @uses BookingProduct::setKey()
     * @uses BookingProduct::setType()
     * @uses BookingProduct::setName()
     * @uses BookingProduct::setPrice()
     * @uses BookingProduct::setCreationPrice()
     * @uses BookingProduct::setPeriodLength()
     * @uses BookingProduct::setPeriodUnit()
     * @param \Booking\StructType\ApiProductKey $key
     * @param string $type
     * @param string $name
     * @param \Booking\StructType\ProductPrice $price
     * @param \Booking\StructType\ProductPrice $creationPrice
     * @param int $periodLength
     * @param string $periodUnit
     */
    public function __construct(?\Booking\StructType\ApiProductKey $key = null, ?string $type = null, ?string $name = null, ?\Booking\StructType\ProductPrice $price = null, ?\Booking\StructType\ProductPrice $creationPrice = null, ?int $periodLength = null, ?string $periodUnit = null)
    {
        $this
            ->setKey($key)
            ->setType($type)
            ->setName($name)
            ->setPrice($price)
            ->setCreationPrice($creationPrice)
            ->setPeriodLength($periodLength)
            ->setPeriodUnit($periodUnit);
    }
    /**
     * Get key value
     * @return \Booking\StructType\ApiProductKey|null
     */
    public function getKey(): ?\Booking\StructType\ApiProductKey
    {
        return $this->key;
    }
    /**
     * Set key value
     * @param \Booking\StructType\ApiProductKey $key
     * @return \Booking\StructType\BookingProduct
     */
    public function setKey(?\Booking\StructType\ApiProductKey $key = null): self
    {
        $this->key = $key;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @uses \Booking\EnumType\ProductType::valueIsValid()
     * @uses \Booking\EnumType\ProductType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $type
     * @return \Booking\StructType\BookingProduct
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\ProductType::valueIsValid($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\ProductType', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \Booking\EnumType\ProductType::getValidValues())), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\BookingProduct
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get price value
     * @return \Booking\StructType\ProductPrice|null
     */
    public function getPrice(): ?\Booking\StructType\ProductPrice
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param \Booking\StructType\ProductPrice $price
     * @return \Booking\StructType\BookingProduct
     */
    public function setPrice(?\Booking\StructType\ProductPrice $price = null): self
    {
        $this->price = $price;
        
        return $this;
    }
    /**
     * Get creationPrice value
     * @return \Booking\StructType\ProductPrice|null
     */
    public function getCreationPrice(): ?\Booking\StructType\ProductPrice
    {
        return $this->creationPrice;
    }
    /**
     * Set creationPrice value
     * @param \Booking\StructType\ProductPrice $creationPrice
     * @return \Booking\StructType\BookingProduct
     */
    public function setCreationPrice(?\Booking\StructType\ProductPrice $creationPrice = null): self
    {
        $this->creationPrice = $creationPrice;
        
        return $this;
    }
    /**
     * Get periodLength value
     * @return int|null
     */
    public function getPeriodLength(): ?int
    {
        return $this->periodLength;
    }
    /**
     * Set periodLength value
     * @param int $periodLength
     * @return \Booking\StructType\BookingProduct
     */
    public function setPeriodLength(?int $periodLength = null): self
    {
        // validation for constraint: int
        if (!is_null($periodLength) && !(is_int($periodLength) || ctype_digit($periodLength))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($periodLength, true), gettype($periodLength)), __LINE__);
        }
        $this->periodLength = $periodLength;
        
        return $this;
    }
    /**
     * Get periodUnit value
     * @return string|null
     */
    public function getPeriodUnit(): ?string
    {
        return $this->periodUnit;
    }
    /**
     * Set periodUnit value
     * @uses \Booking\EnumType\TimeUnit::valueIsValid()
     * @uses \Booking\EnumType\TimeUnit::getValidValues()
     * @throws InvalidArgumentException
     * @param string $periodUnit
     * @return \Booking\StructType\BookingProduct
     */
    public function setPeriodUnit(?string $periodUnit = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\TimeUnit::valueIsValid($periodUnit)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\TimeUnit', is_array($periodUnit) ? implode(', ', $periodUnit) : var_export($periodUnit, true), implode(', ', \Booking\EnumType\TimeUnit::getValidValues())), __LINE__);
        }
        $this->periodUnit = $periodUnit;
        
        return $this;
    }
}
