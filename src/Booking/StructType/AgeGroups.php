<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ageGroups StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AgeGroups extends AbstractStructBase
{
    /**
     * The ageGroup
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Booking\StructType\AvailableAgeGroup[]
     */
    protected ?array $ageGroup = null;
    /**
     * Constructor method for ageGroups
     * @uses AgeGroups::setAgeGroup()
     * @param \Booking\StructType\AvailableAgeGroup[] $ageGroup
     */
    public function __construct(?array $ageGroup = null)
    {
        $this
            ->setAgeGroup($ageGroup);
    }
    /**
     * Get ageGroup value
     * @return \Booking\StructType\AvailableAgeGroup[]
     */
    public function getAgeGroup(): ?array
    {
        return $this->ageGroup;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setAgeGroup method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAgeGroup method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAgeGroupForArrayConstraintFromSetAgeGroup(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $ageGroupsAgeGroupItem) {
            // validation for constraint: itemType
            if (!$ageGroupsAgeGroupItem instanceof \Booking\StructType\AvailableAgeGroup) {
                $invalidValues[] = is_object($ageGroupsAgeGroupItem) ? get_class($ageGroupsAgeGroupItem) : sprintf('%s(%s)', gettype($ageGroupsAgeGroupItem), var_export($ageGroupsAgeGroupItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ageGroup property can only contain items of type \Booking\StructType\AvailableAgeGroup, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set ageGroup value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\AvailableAgeGroup[] $ageGroup
     * @return \Booking\StructType\AgeGroups
     */
    public function setAgeGroup(?array $ageGroup = null): self
    {
        // validation for constraint: array
        if ('' !== ($ageGroupArrayErrorMessage = self::validateAgeGroupForArrayConstraintFromSetAgeGroup($ageGroup))) {
            throw new InvalidArgumentException($ageGroupArrayErrorMessage, __LINE__);
        }
        $this->ageGroup = $ageGroup;
        
        return $this;
    }
    /**
     * Add item to ageGroup value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\AvailableAgeGroup $item
     * @return \Booking\StructType\AgeGroups
     */
    public function addToAgeGroup(\Booking\StructType\AvailableAgeGroup $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\AvailableAgeGroup) {
            throw new InvalidArgumentException(sprintf('The ageGroup property can only contain items of type \Booking\StructType\AvailableAgeGroup, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->ageGroup[] = $item;
        
        return $this;
    }
}
