<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for privilegeSanctionRestriction StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class PrivilegeSanctionRestriction extends AbstractStructBase
{
    /**
     * The count
     * @var int|null
     */
    protected ?int $count = null;
    /**
     * The period
     * @var int|null
     */
    protected ?int $period = null;
    /**
     * The periodUnit
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $periodUnit = null;
    /**
     * Constructor method for privilegeSanctionRestriction
     * @uses PrivilegeSanctionRestriction::setCount()
     * @uses PrivilegeSanctionRestriction::setPeriod()
     * @uses PrivilegeSanctionRestriction::setPeriodUnit()
     * @param int $count
     * @param int $period
     * @param string $periodUnit
     */
    public function __construct(?int $count = null, ?int $period = null, ?string $periodUnit = null)
    {
        $this
            ->setCount($count)
            ->setPeriod($period)
            ->setPeriodUnit($periodUnit);
    }
    /**
     * Get count value
     * @return int|null
     */
    public function getCount(): ?int
    {
        return $this->count;
    }
    /**
     * Set count value
     * @param int $count
     * @return \Booking\StructType\PrivilegeSanctionRestriction
     */
    public function setCount(?int $count = null): self
    {
        // validation for constraint: int
        if (!is_null($count) && !(is_int($count) || ctype_digit($count))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($count, true), gettype($count)), __LINE__);
        }
        $this->count = $count;
        
        return $this;
    }
    /**
     * Get period value
     * @return int|null
     */
    public function getPeriod(): ?int
    {
        return $this->period;
    }
    /**
     * Set period value
     * @param int $period
     * @return \Booking\StructType\PrivilegeSanctionRestriction
     */
    public function setPeriod(?int $period = null): self
    {
        // validation for constraint: int
        if (!is_null($period) && !(is_int($period) || ctype_digit($period))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($period, true), gettype($period)), __LINE__);
        }
        $this->period = $period;
        
        return $this;
    }
    /**
     * Get periodUnit value
     * @return string|null
     */
    public function getPeriodUnit(): ?string
    {
        return $this->periodUnit;
    }
    /**
     * Set periodUnit value
     * @uses \Booking\EnumType\TimeUnit::valueIsValid()
     * @uses \Booking\EnumType\TimeUnit::getValidValues()
     * @throws InvalidArgumentException
     * @param string $periodUnit
     * @return \Booking\StructType\PrivilegeSanctionRestriction
     */
    public function setPeriodUnit(?string $periodUnit = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\TimeUnit::valueIsValid($periodUnit)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\TimeUnit', is_array($periodUnit) ? implode(', ', $periodUnit) : var_export($periodUnit, true), implode(', ', \Booking\EnumType\TimeUnit::getValidValues())), __LINE__);
        }
        $this->periodUnit = $periodUnit;
        
        return $this;
    }
}
