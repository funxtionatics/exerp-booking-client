<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createOrUpdateParticipationTentativeResult StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class CreateOrUpdateParticipationTentativeResult extends AbstractStructBase
{
    /**
     * The participations
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\Participations|null
     */
    protected ?\Booking\StructType\Participations $participations = null;
    /**
     * Constructor method for createOrUpdateParticipationTentativeResult
     * @uses CreateOrUpdateParticipationTentativeResult::setParticipations()
     * @param \Booking\StructType\Participations $participations
     */
    public function __construct(?\Booking\StructType\Participations $participations = null)
    {
        $this
            ->setParticipations($participations);
    }
    /**
     * Get participations value
     * @return \Booking\StructType\Participations|null
     */
    public function getParticipations(): ?\Booking\StructType\Participations
    {
        return $this->participations;
    }
    /**
     * Set participations value
     * @param \Booking\StructType\Participations $participations
     * @return \Booking\StructType\CreateOrUpdateParticipationTentativeResult
     */
    public function setParticipations(?\Booking\StructType\Participations $participations = null): self
    {
        $this->participations = $participations;
        
        return $this;
    }
}
