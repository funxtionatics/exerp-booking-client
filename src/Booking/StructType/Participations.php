<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for participations StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Participations extends AbstractStructBase
{
    /**
     * The participation
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Booking\StructType\Participation[]
     */
    protected ?array $participation = null;
    /**
     * Constructor method for participations
     * @uses Participations::setParticipation()
     * @param \Booking\StructType\Participation[] $participation
     */
    public function __construct(?array $participation = null)
    {
        $this
            ->setParticipation($participation);
    }
    /**
     * Get participation value
     * @return \Booking\StructType\Participation[]
     */
    public function getParticipation(): ?array
    {
        return $this->participation;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setParticipation method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParticipation method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParticipationForArrayConstraintFromSetParticipation(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $participationsParticipationItem) {
            // validation for constraint: itemType
            if (!$participationsParticipationItem instanceof \Booking\StructType\Participation) {
                $invalidValues[] = is_object($participationsParticipationItem) ? get_class($participationsParticipationItem) : sprintf('%s(%s)', gettype($participationsParticipationItem), var_export($participationsParticipationItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The participation property can only contain items of type \Booking\StructType\Participation, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set participation value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\Participation[] $participation
     * @return \Booking\StructType\Participations
     */
    public function setParticipation(?array $participation = null): self
    {
        // validation for constraint: array
        if ('' !== ($participationArrayErrorMessage = self::validateParticipationForArrayConstraintFromSetParticipation($participation))) {
            throw new InvalidArgumentException($participationArrayErrorMessage, __LINE__);
        }
        $this->participation = $participation;
        
        return $this;
    }
    /**
     * Add item to participation value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\Participation $item
     * @return \Booking\StructType\Participations
     */
    public function addToParticipation(\Booking\StructType\Participation $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\Participation) {
            throw new InvalidArgumentException(sprintf('The participation property can only contain items of type \Booking\StructType\Participation, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->participation[] = $item;
        
        return $this;
    }
}
