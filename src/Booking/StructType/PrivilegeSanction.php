<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for privilegeSanction StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class PrivilegeSanction extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The serviceName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $serviceName = null;
    /**
     * The restriction
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\PrivilegeSanctionRestriction|null
     */
    protected ?\Booking\StructType\PrivilegeSanctionRestriction $restriction = null;
    /**
     * Constructor method for privilegeSanction
     * @uses PrivilegeSanction::setName()
     * @uses PrivilegeSanction::setServiceName()
     * @uses PrivilegeSanction::setRestriction()
     * @param string $name
     * @param string $serviceName
     * @param \Booking\StructType\PrivilegeSanctionRestriction $restriction
     */
    public function __construct(?string $name = null, ?string $serviceName = null, ?\Booking\StructType\PrivilegeSanctionRestriction $restriction = null)
    {
        $this
            ->setName($name)
            ->setServiceName($serviceName)
            ->setRestriction($restriction);
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\PrivilegeSanction
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get serviceName value
     * @return string|null
     */
    public function getServiceName(): ?string
    {
        return $this->serviceName;
    }
    /**
     * Set serviceName value
     * @param string $serviceName
     * @return \Booking\StructType\PrivilegeSanction
     */
    public function setServiceName(?string $serviceName = null): self
    {
        // validation for constraint: string
        if (!is_null($serviceName) && !is_string($serviceName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceName, true), gettype($serviceName)), __LINE__);
        }
        $this->serviceName = $serviceName;
        
        return $this;
    }
    /**
     * Get restriction value
     * @return \Booking\StructType\PrivilegeSanctionRestriction|null
     */
    public function getRestriction(): ?\Booking\StructType\PrivilegeSanctionRestriction
    {
        return $this->restriction;
    }
    /**
     * Set restriction value
     * @param \Booking\StructType\PrivilegeSanctionRestriction $restriction
     * @return \Booking\StructType\PrivilegeSanction
     */
    public function setRestriction(?\Booking\StructType\PrivilegeSanctionRestriction $restriction = null): self
    {
        $this->restriction = $restriction;
        
        return $this;
    }
}
