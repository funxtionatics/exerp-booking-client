<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for updateClassBookingParameter StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class UpdateClassBookingParameter extends AbstractStructBase
{
    /**
     * The bookingKey
     * @var \Booking\StructType\CompositeKey|null
     */
    protected ?\Booking\StructType\CompositeKey $bookingKey = null;
    /**
     * The color
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\ColorGroup|null
     */
    protected ?\Booking\StructType\ColorGroup $color = null;
    /**
     * The date
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $date = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The duration
     * @var int|null
     */
    protected ?int $duration = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The maxCapacity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $maxCapacity = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The time
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $time = null;
    /**
     * The resourceKeys
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\CompositeKey[]
     */
    protected ?array $resourceKeys = null;
    /**
     * The staffPersonKeys
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Booking\StructType\ApiPersonKey[]
     */
    protected ?array $staffPersonKeys = null;
    /**
     * Constructor method for updateClassBookingParameter
     * @uses UpdateClassBookingParameter::setBookingKey()
     * @uses UpdateClassBookingParameter::setColor()
     * @uses UpdateClassBookingParameter::setDate()
     * @uses UpdateClassBookingParameter::setDescription()
     * @uses UpdateClassBookingParameter::setDuration()
     * @uses UpdateClassBookingParameter::setExternalId()
     * @uses UpdateClassBookingParameter::setMaxCapacity()
     * @uses UpdateClassBookingParameter::setName()
     * @uses UpdateClassBookingParameter::setTime()
     * @uses UpdateClassBookingParameter::setResourceKeys()
     * @uses UpdateClassBookingParameter::setStaffPersonKeys()
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @param \Booking\StructType\ColorGroup $color
     * @param string $date
     * @param string $description
     * @param int $duration
     * @param string $externalId
     * @param int $maxCapacity
     * @param string $name
     * @param string $time
     * @param \Booking\StructType\CompositeKey[] $resourceKeys
     * @param \Booking\StructType\ApiPersonKey[] $staffPersonKeys
     */
    public function __construct(?\Booking\StructType\CompositeKey $bookingKey = null, ?\Booking\StructType\ColorGroup $color = null, ?string $date = null, ?string $description = null, ?int $duration = null, ?string $externalId = null, ?int $maxCapacity = null, ?string $name = null, ?string $time = null, ?array $resourceKeys = null, ?array $staffPersonKeys = null)
    {
        $this
            ->setBookingKey($bookingKey)
            ->setColor($color)
            ->setDate($date)
            ->setDescription($description)
            ->setDuration($duration)
            ->setExternalId($externalId)
            ->setMaxCapacity($maxCapacity)
            ->setName($name)
            ->setTime($time)
            ->setResourceKeys($resourceKeys)
            ->setStaffPersonKeys($staffPersonKeys);
    }
    /**
     * Get bookingKey value
     * @return \Booking\StructType\CompositeKey|null
     */
    public function getBookingKey(): ?\Booking\StructType\CompositeKey
    {
        return $this->bookingKey;
    }
    /**
     * Set bookingKey value
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function setBookingKey(?\Booking\StructType\CompositeKey $bookingKey = null): self
    {
        $this->bookingKey = $bookingKey;
        
        return $this;
    }
    /**
     * Get color value
     * @return \Booking\StructType\ColorGroup|null
     */
    public function getColor(): ?\Booking\StructType\ColorGroup
    {
        return $this->color;
    }
    /**
     * Set color value
     * @param \Booking\StructType\ColorGroup $color
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function setColor(?\Booking\StructType\ColorGroup $color = null): self
    {
        $this->color = $color;
        
        return $this;
    }
    /**
     * Get date value
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }
    /**
     * Set date value
     * @param string $date
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function setDate(?string $date = null): self
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->date = $date;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get duration value
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }
    /**
     * Set duration value
     * @param int $duration
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function setDuration(?int $duration = null): self
    {
        // validation for constraint: int
        if (!is_null($duration) && !(is_int($duration) || ctype_digit($duration))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($duration, true), gettype($duration)), __LINE__);
        }
        $this->duration = $duration;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get maxCapacity value
     * @return int|null
     */
    public function getMaxCapacity(): ?int
    {
        return $this->maxCapacity;
    }
    /**
     * Set maxCapacity value
     * @param int $maxCapacity
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function setMaxCapacity(?int $maxCapacity = null): self
    {
        // validation for constraint: int
        if (!is_null($maxCapacity) && !(is_int($maxCapacity) || ctype_digit($maxCapacity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($maxCapacity, true), gettype($maxCapacity)), __LINE__);
        }
        $this->maxCapacity = $maxCapacity;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get time value
     * @return string|null
     */
    public function getTime(): ?string
    {
        return $this->time;
    }
    /**
     * Set time value
     * @param string $time
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function setTime(?string $time = null): self
    {
        // validation for constraint: string
        if (!is_null($time) && !is_string($time)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($time, true), gettype($time)), __LINE__);
        }
        $this->time = $time;
        
        return $this;
    }
    /**
     * Get resourceKeys value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\CompositeKey[]
     */
    public function getResourceKeys(): ?array
    {
        return $this->resourceKeys ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setResourceKeys method
     * This method is willingly generated in order to preserve the one-line inline validation within the setResourceKeys method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateResourceKeysForArrayConstraintFromSetResourceKeys(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $updateClassBookingParameterResourceKeysItem) {
            // validation for constraint: itemType
            if (!$updateClassBookingParameterResourceKeysItem instanceof \Booking\StructType\CompositeKey) {
                $invalidValues[] = is_object($updateClassBookingParameterResourceKeysItem) ? get_class($updateClassBookingParameterResourceKeysItem) : sprintf('%s(%s)', gettype($updateClassBookingParameterResourceKeysItem), var_export($updateClassBookingParameterResourceKeysItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The resourceKeys property can only contain items of type \Booking\StructType\CompositeKey, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set resourceKeys value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\CompositeKey[] $resourceKeys
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function setResourceKeys(?array $resourceKeys = null): self
    {
        // validation for constraint: array
        if ('' !== ($resourceKeysArrayErrorMessage = self::validateResourceKeysForArrayConstraintFromSetResourceKeys($resourceKeys))) {
            throw new InvalidArgumentException($resourceKeysArrayErrorMessage, __LINE__);
        }
        if (is_null($resourceKeys) || (is_array($resourceKeys) && empty($resourceKeys))) {
            unset($this->resourceKeys);
        } else {
            $this->resourceKeys = $resourceKeys;
        }
        
        return $this;
    }
    /**
     * Add item to resourceKeys value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\CompositeKey $item
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function addToResourceKeys(\Booking\StructType\CompositeKey $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\CompositeKey) {
            throw new InvalidArgumentException(sprintf('The resourceKeys property can only contain items of type \Booking\StructType\CompositeKey, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->resourceKeys[] = $item;
        
        return $this;
    }
    /**
     * Get staffPersonKeys value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Booking\StructType\ApiPersonKey[]
     */
    public function getStaffPersonKeys(): ?array
    {
        return $this->staffPersonKeys ?? null;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setStaffPersonKeys method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStaffPersonKeys method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStaffPersonKeysForArrayConstraintFromSetStaffPersonKeys(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $updateClassBookingParameterStaffPersonKeysItem) {
            // validation for constraint: itemType
            if (!$updateClassBookingParameterStaffPersonKeysItem instanceof \Booking\StructType\ApiPersonKey) {
                $invalidValues[] = is_object($updateClassBookingParameterStaffPersonKeysItem) ? get_class($updateClassBookingParameterStaffPersonKeysItem) : sprintf('%s(%s)', gettype($updateClassBookingParameterStaffPersonKeysItem), var_export($updateClassBookingParameterStaffPersonKeysItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The staffPersonKeys property can only contain items of type \Booking\StructType\ApiPersonKey, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set staffPersonKeys value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Booking\StructType\ApiPersonKey[] $staffPersonKeys
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function setStaffPersonKeys(?array $staffPersonKeys = null): self
    {
        // validation for constraint: array
        if ('' !== ($staffPersonKeysArrayErrorMessage = self::validateStaffPersonKeysForArrayConstraintFromSetStaffPersonKeys($staffPersonKeys))) {
            throw new InvalidArgumentException($staffPersonKeysArrayErrorMessage, __LINE__);
        }
        if (is_null($staffPersonKeys) || (is_array($staffPersonKeys) && empty($staffPersonKeys))) {
            unset($this->staffPersonKeys);
        } else {
            $this->staffPersonKeys = $staffPersonKeys;
        }
        
        return $this;
    }
    /**
     * Add item to staffPersonKeys value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\ApiPersonKey $item
     * @return \Booking\StructType\UpdateClassBookingParameter
     */
    public function addToStaffPersonKeys(\Booking\StructType\ApiPersonKey $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\ApiPersonKey) {
            throw new InvalidArgumentException(sprintf('The staffPersonKeys property can only contain items of type \Booking\StructType\ApiPersonKey, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->staffPersonKeys[] = $item;
        
        return $this;
    }
}
