<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for courseConfiguration StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class CourseConfiguration extends AbstractStructBase
{
    /**
     * The courseLevel
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CourseLevel|null
     */
    protected ?\Booking\StructType\CourseLevel $courseLevel = null;
    /**
     * The courseType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\CourseType|null
     */
    protected ?\Booking\StructType\CourseType $courseType = null;
    /**
     * The courseScheduleType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $courseScheduleType = null;
    /**
     * Constructor method for courseConfiguration
     * @uses CourseConfiguration::setCourseLevel()
     * @uses CourseConfiguration::setCourseType()
     * @uses CourseConfiguration::setCourseScheduleType()
     * @param \Booking\StructType\CourseLevel $courseLevel
     * @param \Booking\StructType\CourseType $courseType
     * @param string $courseScheduleType
     */
    public function __construct(?\Booking\StructType\CourseLevel $courseLevel = null, ?\Booking\StructType\CourseType $courseType = null, ?string $courseScheduleType = null)
    {
        $this
            ->setCourseLevel($courseLevel)
            ->setCourseType($courseType)
            ->setCourseScheduleType($courseScheduleType);
    }
    /**
     * Get courseLevel value
     * @return \Booking\StructType\CourseLevel|null
     */
    public function getCourseLevel(): ?\Booking\StructType\CourseLevel
    {
        return $this->courseLevel;
    }
    /**
     * Set courseLevel value
     * @param \Booking\StructType\CourseLevel $courseLevel
     * @return \Booking\StructType\CourseConfiguration
     */
    public function setCourseLevel(?\Booking\StructType\CourseLevel $courseLevel = null): self
    {
        $this->courseLevel = $courseLevel;
        
        return $this;
    }
    /**
     * Get courseType value
     * @return \Booking\StructType\CourseType|null
     */
    public function getCourseType(): ?\Booking\StructType\CourseType
    {
        return $this->courseType;
    }
    /**
     * Set courseType value
     * @param \Booking\StructType\CourseType $courseType
     * @return \Booking\StructType\CourseConfiguration
     */
    public function setCourseType(?\Booking\StructType\CourseType $courseType = null): self
    {
        $this->courseType = $courseType;
        
        return $this;
    }
    /**
     * Get courseScheduleType value
     * @return string|null
     */
    public function getCourseScheduleType(): ?string
    {
        return $this->courseScheduleType;
    }
    /**
     * Set courseScheduleType value
     * @uses \Booking\EnumType\CourseScheduleType::valueIsValid()
     * @uses \Booking\EnumType\CourseScheduleType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $courseScheduleType
     * @return \Booking\StructType\CourseConfiguration
     */
    public function setCourseScheduleType(?string $courseScheduleType = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\CourseScheduleType::valueIsValid($courseScheduleType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\CourseScheduleType', is_array($courseScheduleType) ? implode(', ', $courseScheduleType) : var_export($courseScheduleType, true), implode(', ', \Booking\EnumType\CourseScheduleType::getValidValues())), __LINE__);
        }
        $this->courseScheduleType = $courseScheduleType;
        
        return $this;
    }
}
