<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getAvailableAgeGroupsParameters StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class GetAvailableAgeGroupsParameters extends AbstractStructBase
{
    /**
     * The scopeId
     * @var int|null
     */
    protected ?int $scopeId = null;
    /**
     * The scopeType
     * @var string|null
     */
    protected ?string $scopeType = null;
    /**
     * Constructor method for getAvailableAgeGroupsParameters
     * @uses GetAvailableAgeGroupsParameters::setScopeId()
     * @uses GetAvailableAgeGroupsParameters::setScopeType()
     * @param int $scopeId
     * @param string $scopeType
     */
    public function __construct(?int $scopeId = null, ?string $scopeType = null)
    {
        $this
            ->setScopeId($scopeId)
            ->setScopeType($scopeType);
    }
    /**
     * Get scopeId value
     * @return int|null
     */
    public function getScopeId(): ?int
    {
        return $this->scopeId;
    }
    /**
     * Set scopeId value
     * @param int $scopeId
     * @return \Booking\StructType\GetAvailableAgeGroupsParameters
     */
    public function setScopeId(?int $scopeId = null): self
    {
        // validation for constraint: int
        if (!is_null($scopeId) && !(is_int($scopeId) || ctype_digit($scopeId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($scopeId, true), gettype($scopeId)), __LINE__);
        }
        $this->scopeId = $scopeId;
        
        return $this;
    }
    /**
     * Get scopeType value
     * @return string|null
     */
    public function getScopeType(): ?string
    {
        return $this->scopeType;
    }
    /**
     * Set scopeType value
     * @uses \Booking\EnumType\ScopeType::valueIsValid()
     * @uses \Booking\EnumType\ScopeType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $scopeType
     * @return \Booking\StructType\GetAvailableAgeGroupsParameters
     */
    public function setScopeType(?string $scopeType = null): self
    {
        // validation for constraint: enumeration
        if (!\Booking\EnumType\ScopeType::valueIsValid($scopeType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Booking\EnumType\ScopeType', is_array($scopeType) ? implode(', ', $scopeType) : var_export($scopeType, true), implode(', ', \Booking\EnumType\ScopeType::getValidValues())), __LINE__);
        }
        $this->scopeType = $scopeType;
        
        return $this;
    }
}
