<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for sanctions StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Sanctions extends AbstractStructBase
{
    /**
     * The sanction
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Booking\StructType\MemberSanctionStatus[]
     */
    protected ?array $sanction = null;
    /**
     * Constructor method for sanctions
     * @uses Sanctions::setSanction()
     * @param \Booking\StructType\MemberSanctionStatus[] $sanction
     */
    public function __construct(?array $sanction = null)
    {
        $this
            ->setSanction($sanction);
    }
    /**
     * Get sanction value
     * @return \Booking\StructType\MemberSanctionStatus[]
     */
    public function getSanction(): ?array
    {
        return $this->sanction;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setSanction method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSanction method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSanctionForArrayConstraintFromSetSanction(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $sanctionsSanctionItem) {
            // validation for constraint: itemType
            if (!$sanctionsSanctionItem instanceof \Booking\StructType\MemberSanctionStatus) {
                $invalidValues[] = is_object($sanctionsSanctionItem) ? get_class($sanctionsSanctionItem) : sprintf('%s(%s)', gettype($sanctionsSanctionItem), var_export($sanctionsSanctionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The sanction property can only contain items of type \Booking\StructType\MemberSanctionStatus, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set sanction value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\MemberSanctionStatus[] $sanction
     * @return \Booking\StructType\Sanctions
     */
    public function setSanction(?array $sanction = null): self
    {
        // validation for constraint: array
        if ('' !== ($sanctionArrayErrorMessage = self::validateSanctionForArrayConstraintFromSetSanction($sanction))) {
            throw new InvalidArgumentException($sanctionArrayErrorMessage, __LINE__);
        }
        $this->sanction = $sanction;
        
        return $this;
    }
    /**
     * Add item to sanction value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\MemberSanctionStatus $item
     * @return \Booking\StructType\Sanctions
     */
    public function addToSanction(\Booking\StructType\MemberSanctionStatus $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\MemberSanctionStatus) {
            throw new InvalidArgumentException(sprintf('The sanction property can only contain items of type \Booking\StructType\MemberSanctionStatus, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->sanction[] = $item;
        
        return $this;
    }
}
