<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for bookingChangeActivitySimpleResult StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class BookingChangeActivitySimpleResult extends AbstractStructBase
{
    /**
     * The booking
     * @var \Booking\StructType\Booking|null
     */
    protected ?\Booking\StructType\Booking $booking = null;
    /**
     * Constructor method for bookingChangeActivitySimpleResult
     * @uses BookingChangeActivitySimpleResult::setBooking()
     * @param \Booking\StructType\Booking $booking
     */
    public function __construct(?\Booking\StructType\Booking $booking = null)
    {
        $this
            ->setBooking($booking);
    }
    /**
     * Get booking value
     * @return \Booking\StructType\Booking|null
     */
    public function getBooking(): ?\Booking\StructType\Booking
    {
        return $this->booking;
    }
    /**
     * Set booking value
     * @param \Booking\StructType\Booking $booking
     * @return \Booking\StructType\BookingChangeActivitySimpleResult
     */
    public function setBooking(?\Booking\StructType\Booking $booking = null): self
    {
        $this->booking = $booking;
        
        return $this;
    }
}
