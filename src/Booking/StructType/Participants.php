<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for participants StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Participants extends AbstractStructBase
{
    /**
     * The participant
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * @var \Booking\StructType\Participant[]
     */
    protected ?array $participant = null;
    /**
     * Constructor method for participants
     * @uses Participants::setParticipant()
     * @param \Booking\StructType\Participant[] $participant
     */
    public function __construct(?array $participant = null)
    {
        $this
            ->setParticipant($participant);
    }
    /**
     * Get participant value
     * @return \Booking\StructType\Participant[]
     */
    public function getParticipant(): ?array
    {
        return $this->participant;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setParticipant method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParticipant method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParticipantForArrayConstraintFromSetParticipant(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $participantsParticipantItem) {
            // validation for constraint: itemType
            if (!$participantsParticipantItem instanceof \Booking\StructType\Participant) {
                $invalidValues[] = is_object($participantsParticipantItem) ? get_class($participantsParticipantItem) : sprintf('%s(%s)', gettype($participantsParticipantItem), var_export($participantsParticipantItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The participant property can only contain items of type \Booking\StructType\Participant, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set participant value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\Participant[] $participant
     * @return \Booking\StructType\Participants
     */
    public function setParticipant(?array $participant = null): self
    {
        // validation for constraint: array
        if ('' !== ($participantArrayErrorMessage = self::validateParticipantForArrayConstraintFromSetParticipant($participant))) {
            throw new InvalidArgumentException($participantArrayErrorMessage, __LINE__);
        }
        $this->participant = $participant;
        
        return $this;
    }
    /**
     * Add item to participant value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\Participant $item
     * @return \Booking\StructType\Participants
     */
    public function addToParticipant(\Booking\StructType\Participant $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\Participant) {
            throw new InvalidArgumentException(sprintf('The participant property can only contain items of type \Booking\StructType\Participant, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->participant[] = $item;
        
        return $this;
    }
}
