<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for bookingRestrictions StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class BookingRestrictions extends AbstractStructBase
{
    /**
     * The bookingRestriction
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Booking\StructType\BookingRestriction[]
     */
    protected ?array $bookingRestriction = null;
    /**
     * Constructor method for bookingRestrictions
     * @uses BookingRestrictions::setBookingRestriction()
     * @param \Booking\StructType\BookingRestriction[] $bookingRestriction
     */
    public function __construct(?array $bookingRestriction = null)
    {
        $this
            ->setBookingRestriction($bookingRestriction);
    }
    /**
     * Get bookingRestriction value
     * @return \Booking\StructType\BookingRestriction[]
     */
    public function getBookingRestriction(): ?array
    {
        return $this->bookingRestriction;
    }
    /**
     * This method is responsible for validating the value(s) passed to the setBookingRestriction method
     * This method is willingly generated in order to preserve the one-line inline validation within the setBookingRestriction method
     * This has to validate that each item contained by the array match the itemType constraint
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateBookingRestrictionForArrayConstraintFromSetBookingRestriction(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $bookingRestrictionsBookingRestrictionItem) {
            // validation for constraint: itemType
            if (!$bookingRestrictionsBookingRestrictionItem instanceof \Booking\StructType\BookingRestriction) {
                $invalidValues[] = is_object($bookingRestrictionsBookingRestrictionItem) ? get_class($bookingRestrictionsBookingRestrictionItem) : sprintf('%s(%s)', gettype($bookingRestrictionsBookingRestrictionItem), var_export($bookingRestrictionsBookingRestrictionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The bookingRestriction property can only contain items of type \Booking\StructType\BookingRestriction, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set bookingRestriction value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\BookingRestriction[] $bookingRestriction
     * @return \Booking\StructType\BookingRestrictions
     */
    public function setBookingRestriction(?array $bookingRestriction = null): self
    {
        // validation for constraint: array
        if ('' !== ($bookingRestrictionArrayErrorMessage = self::validateBookingRestrictionForArrayConstraintFromSetBookingRestriction($bookingRestriction))) {
            throw new InvalidArgumentException($bookingRestrictionArrayErrorMessage, __LINE__);
        }
        $this->bookingRestriction = $bookingRestriction;
        
        return $this;
    }
    /**
     * Add item to bookingRestriction value
     * @throws InvalidArgumentException
     * @param \Booking\StructType\BookingRestriction $item
     * @return \Booking\StructType\BookingRestrictions
     */
    public function addToBookingRestriction(\Booking\StructType\BookingRestriction $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Booking\StructType\BookingRestriction) {
            throw new InvalidArgumentException(sprintf('The bookingRestriction property can only contain items of type \Booking\StructType\BookingRestriction, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->bookingRestriction[] = $item;
        
        return $this;
    }
}
