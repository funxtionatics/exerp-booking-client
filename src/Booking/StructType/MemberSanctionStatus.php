<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for memberSanctionStatus StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class MemberSanctionStatus extends AbstractStructBase
{
    /**
     * The booking
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\Booking|null
     */
    protected ?\Booking\StructType\Booking $booking = null;
    /**
     * The sanction
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\PrivilegeSanction|null
     */
    protected ?\Booking\StructType\PrivilegeSanction $sanction = null;
    /**
     * Constructor method for memberSanctionStatus
     * @uses MemberSanctionStatus::setBooking()
     * @uses MemberSanctionStatus::setSanction()
     * @param \Booking\StructType\Booking $booking
     * @param \Booking\StructType\PrivilegeSanction $sanction
     */
    public function __construct(?\Booking\StructType\Booking $booking = null, ?\Booking\StructType\PrivilegeSanction $sanction = null)
    {
        $this
            ->setBooking($booking)
            ->setSanction($sanction);
    }
    /**
     * Get booking value
     * @return \Booking\StructType\Booking|null
     */
    public function getBooking(): ?\Booking\StructType\Booking
    {
        return $this->booking;
    }
    /**
     * Set booking value
     * @param \Booking\StructType\Booking $booking
     * @return \Booking\StructType\MemberSanctionStatus
     */
    public function setBooking(?\Booking\StructType\Booking $booking = null): self
    {
        $this->booking = $booking;
        
        return $this;
    }
    /**
     * Get sanction value
     * @return \Booking\StructType\PrivilegeSanction|null
     */
    public function getSanction(): ?\Booking\StructType\PrivilegeSanction
    {
        return $this->sanction;
    }
    /**
     * Set sanction value
     * @param \Booking\StructType\PrivilegeSanction $sanction
     * @return \Booking\StructType\MemberSanctionStatus
     */
    public function setSanction(?\Booking\StructType\PrivilegeSanction $sanction = null): self
    {
        $this->sanction = $sanction;
        
        return $this;
    }
}
