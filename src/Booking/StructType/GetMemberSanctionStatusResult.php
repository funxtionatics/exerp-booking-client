<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getMemberSanctionStatusResult StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class GetMemberSanctionStatusResult extends AbstractStructBase
{
    /**
     * The sanctions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\Sanctions|null
     */
    protected ?\Booking\StructType\Sanctions $sanctions = null;
    /**
     * Constructor method for getMemberSanctionStatusResult
     * @uses GetMemberSanctionStatusResult::setSanctions()
     * @param \Booking\StructType\Sanctions $sanctions
     */
    public function __construct(?\Booking\StructType\Sanctions $sanctions = null)
    {
        $this
            ->setSanctions($sanctions);
    }
    /**
     * Get sanctions value
     * @return \Booking\StructType\Sanctions|null
     */
    public function getSanctions(): ?\Booking\StructType\Sanctions
    {
        return $this->sanctions;
    }
    /**
     * Set sanctions value
     * @param \Booking\StructType\Sanctions $sanctions
     * @return \Booking\StructType\GetMemberSanctionStatusResult
     */
    public function setSanctions(?\Booking\StructType\Sanctions $sanctions = null): self
    {
        $this->sanctions = $sanctions;
        
        return $this;
    }
}
