<?php

declare(strict_types=1);

namespace Booking\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for availableAgeGroup StructType
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AvailableAgeGroup extends AbstractStructBase
{
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The id
     * @var int|null
     */
    protected ?int $id = null;
    /**
     * The maxAge
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\AgeGroupLimit|null
     */
    protected ?\Booking\StructType\AgeGroupLimit $maxAge = null;
    /**
     * The minAge
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Booking\StructType\AgeGroupLimit|null
     */
    protected ?\Booking\StructType\AgeGroupLimit $minAge = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The strictMinAge
     * @var bool|null
     */
    protected ?bool $strictMinAge = null;
    /**
     * Constructor method for availableAgeGroup
     * @uses AvailableAgeGroup::setExternalId()
     * @uses AvailableAgeGroup::setId()
     * @uses AvailableAgeGroup::setMaxAge()
     * @uses AvailableAgeGroup::setMinAge()
     * @uses AvailableAgeGroup::setName()
     * @uses AvailableAgeGroup::setStrictMinAge()
     * @param string $externalId
     * @param int $id
     * @param \Booking\StructType\AgeGroupLimit $maxAge
     * @param \Booking\StructType\AgeGroupLimit $minAge
     * @param string $name
     * @param bool $strictMinAge
     */
    public function __construct(?string $externalId = null, ?int $id = null, ?\Booking\StructType\AgeGroupLimit $maxAge = null, ?\Booking\StructType\AgeGroupLimit $minAge = null, ?string $name = null, ?bool $strictMinAge = null)
    {
        $this
            ->setExternalId($externalId)
            ->setId($id)
            ->setMaxAge($maxAge)
            ->setMinAge($minAge)
            ->setName($name)
            ->setStrictMinAge($strictMinAge);
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Booking\StructType\AvailableAgeGroup
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \Booking\StructType\AvailableAgeGroup
     */
    public function setId(?int $id = null): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
    /**
     * Get maxAge value
     * @return \Booking\StructType\AgeGroupLimit|null
     */
    public function getMaxAge(): ?\Booking\StructType\AgeGroupLimit
    {
        return $this->maxAge;
    }
    /**
     * Set maxAge value
     * @param \Booking\StructType\AgeGroupLimit $maxAge
     * @return \Booking\StructType\AvailableAgeGroup
     */
    public function setMaxAge(?\Booking\StructType\AgeGroupLimit $maxAge = null): self
    {
        $this->maxAge = $maxAge;
        
        return $this;
    }
    /**
     * Get minAge value
     * @return \Booking\StructType\AgeGroupLimit|null
     */
    public function getMinAge(): ?\Booking\StructType\AgeGroupLimit
    {
        return $this->minAge;
    }
    /**
     * Set minAge value
     * @param \Booking\StructType\AgeGroupLimit $minAge
     * @return \Booking\StructType\AvailableAgeGroup
     */
    public function setMinAge(?\Booking\StructType\AgeGroupLimit $minAge = null): self
    {
        $this->minAge = $minAge;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Booking\StructType\AvailableAgeGroup
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get strictMinAge value
     * @return bool|null
     */
    public function getStrictMinAge(): ?bool
    {
        return $this->strictMinAge;
    }
    /**
     * Set strictMinAge value
     * @param bool $strictMinAge
     * @return \Booking\StructType\AvailableAgeGroup
     */
    public function setStrictMinAge(?bool $strictMinAge = null): self
    {
        // validation for constraint: boolean
        if (!is_null($strictMinAge) && !is_bool($strictMinAge)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($strictMinAge, true), gettype($strictMinAge)), __LINE__);
        }
        $this->strictMinAge = $strictMinAge;
        
        return $this;
    }
}
