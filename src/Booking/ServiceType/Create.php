<?php

declare(strict_types=1);

namespace Booking\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Create ServiceType
 * @subpackage Services
 */
class Create extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named createParticipationAndSendMessage
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @param \Booking\StructType\CompositeKey $bookingId
     * @param string $userInterfaceType
     * @return \Booking\StructType\Participation|bool
     */
    public function createParticipationAndSendMessage(\Booking\StructType\ApiPersonKey $personKey, \Booking\StructType\CompositeKey $bookingId, $userInterfaceType)
    {
        try {
            $this->setResult($resultCreateParticipationAndSendMessage = $this->getSoapClient()->__soapCall('createParticipationAndSendMessage', [
                $personKey,
                $bookingId,
                $userInterfaceType,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateParticipationAndSendMessage;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named createClass
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\CreateClassBookingParameter $parameters
     * @return \Booking\StructType\Booking|bool
     */
    public function createClass(\Booking\StructType\CreateClassBookingParameter $parameters)
    {
        try {
            $this->setResult($resultCreateClass = $this->getSoapClient()->__soapCall('createClass', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateClass;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named
     * createOrUpdateParticipationTentative
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\CreateOrUpdateParticipationTentativeParams $parameters
     * @return \Booking\StructType\CreateOrUpdateParticipationTentativeResult|bool
     */
    public function createOrUpdateParticipationTentative(\Booking\StructType\CreateOrUpdateParticipationTentativeParams $parameters)
    {
        try {
            $this->setResult($resultCreateOrUpdateParticipationTentative = $this->getSoapClient()->__soapCall('createOrUpdateParticipationTentative', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateOrUpdateParticipationTentative;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named createOrUpdateParticipation
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\CreateOrUpdateParticipationParams $parameters
     * @return \Booking\StructType\CreateOrUpdateParticipationResult|bool
     */
    public function createOrUpdateParticipation(\Booking\StructType\CreateOrUpdateParticipationParams $parameters)
    {
        try {
            $this->setResult($resultCreateOrUpdateParticipation = $this->getSoapClient()->__soapCall('createOrUpdateParticipation', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateOrUpdateParticipation;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Booking\StructType\Booking|\Booking\StructType\CreateOrUpdateParticipationResult|\Booking\StructType\CreateOrUpdateParticipationTentativeResult|\Booking\StructType\Participation
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
