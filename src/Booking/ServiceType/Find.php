<?php

declare(strict_types=1);

namespace Booking\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Find ServiceType
 * @subpackage Services
 */
class Find extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named findClasses
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\FindClassesParameters $parameters
     * @return \Booking\ArrayType\BookingArray|bool
     */
    public function findClasses(\Booking\StructType\FindClassesParameters $parameters)
    {
        try {
            $this->setResult($resultFindClasses = $this->getSoapClient()->__soapCall('findClasses', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultFindClasses;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named findPersonParticipations
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @return \Booking\ArrayType\ParticipationArray|bool
     */
    public function findPersonParticipations(\Booking\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultFindPersonParticipations = $this->getSoapClient()->__soapCall('findPersonParticipations', [
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultFindPersonParticipations;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Booking\ArrayType\BookingArray|\Booking\ArrayType\ParticipationArray
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
