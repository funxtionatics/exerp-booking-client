<?php

declare(strict_types=1);

namespace Booking\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Change ServiceType
 * @subpackage Services
 */
class Change extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named changeClassActivity
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\BookingChangeActivityParameter $parameters
     * @return \Booking\StructType\BookingChangeActivityResult|bool
     */
    public function changeClassActivity(\Booking\StructType\BookingChangeActivityParameter $parameters)
    {
        try {
            $this->setResult($resultChangeClassActivity = $this->getSoapClient()->__soapCall('changeClassActivity', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultChangeClassActivity;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named changeClassActivitySimple
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\BookingChangeActivitySimpleParameters $bookingChangeActivitySimpleParameters
     * @return \Booking\StructType\BookingChangeActivitySimpleResult|bool
     */
    public function changeClassActivitySimple(\Booking\StructType\BookingChangeActivitySimpleParameters $bookingChangeActivitySimpleParameters)
    {
        try {
            $this->setResult($resultChangeClassActivitySimple = $this->getSoapClient()->__soapCall('changeClassActivitySimple', [
                $bookingChangeActivitySimpleParameters,
            ], [], [], $this->outputHeaders));
        
            return $resultChangeClassActivitySimple;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Booking\StructType\BookingChangeActivityResult|\Booking\StructType\BookingChangeActivitySimpleResult
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
