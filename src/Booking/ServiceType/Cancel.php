<?php

declare(strict_types=1);

namespace Booking\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Cancel ServiceType
 * @subpackage Services
 */
class Cancel extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named cancelBooking
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @param string $notifyParticipants
     * @param string $notifyStaff
     * @param string $message
     * @return void|bool
     */
    public function cancelBooking(\Booking\StructType\CompositeKey $bookingKey, $notifyParticipants, $notifyStaff, $message)
    {
        try {
            $this->setResult($resultCancelBooking = $this->getSoapClient()->__soapCall('cancelBooking', [
                $bookingKey,
                $notifyParticipants,
                $notifyStaff,
                $message,
            ], [], [], $this->outputHeaders));
        
            return $resultCancelBooking;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named cancelPersonParticipation
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\CompositeKey $participationId
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @param string $userInterfaceType
     * @param string $sendCancelMessage
     * @return void|bool
     */
    public function cancelPersonParticipation(\Booking\StructType\CompositeKey $participationId, \Booking\StructType\ApiPersonKey $personKey, $userInterfaceType, $sendCancelMessage)
    {
        try {
            $this->setResult($resultCancelPersonParticipation = $this->getSoapClient()->__soapCall('cancelPersonParticipation', [
                $participationId,
                $personKey,
                $userInterfaceType,
                $sendCancelMessage,
            ], [], [], $this->outputHeaders));
        
            return $resultCancelPersonParticipation;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
