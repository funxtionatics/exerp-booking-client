<?php

declare(strict_types=1);

namespace Booking\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Check ServiceType
 * @subpackage Services
 */
class Check extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named checkPrivilegeForBooking
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\CheckPrivilegeForBookingParameters $checkPrivilegeForBookingParameters
     * @return \Booking\StructType\CheckPrivilegeForBookingResult|bool
     */
    public function checkPrivilegeForBooking(\Booking\StructType\CheckPrivilegeForBookingParameters $checkPrivilegeForBookingParameters)
    {
        try {
            $this->setResult($resultCheckPrivilegeForBooking = $this->getSoapClient()->__soapCall('checkPrivilegeForBooking', [
                $checkPrivilegeForBookingParameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCheckPrivilegeForBooking;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named checkPrivilegeForActivity
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\CheckPrivilegeForActivityParameters $parameters
     * @return \Booking\StructType\CheckPrivilegeForActivityResult|bool
     */
    public function checkPrivilegeForActivity(\Booking\StructType\CheckPrivilegeForActivityParameters $parameters)
    {
        try {
            $this->setResult($resultCheckPrivilegeForActivity = $this->getSoapClient()->__soapCall('checkPrivilegeForActivity', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCheckPrivilegeForActivity;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Booking\StructType\CheckPrivilegeForActivityResult|\Booking\StructType\CheckPrivilegeForBookingResult
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
