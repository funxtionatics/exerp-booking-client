<?php

declare(strict_types=1);

namespace Booking\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get ServiceType
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named getResources
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $center
     * @param string $activityKey
     * @return \Booking\ArrayType\ResourceArray|bool
     */
    public function getResources($center, $activityKey)
    {
        try {
            $this->setResult($resultGetResources = $this->getSoapClient()->__soapCall('getResources', [
                $center,
                $activityKey,
            ], [], [], $this->outputHeaders));
        
            return $resultGetResources;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getParticipationQRCode
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\CompositeKey $participationId
     * @return \Booking\StructType\MimeDocument|bool
     */
    public function getParticipationQRCode(\Booking\StructType\CompositeKey $participationId)
    {
        try {
            $this->setResult($resultGetParticipationQRCode = $this->getSoapClient()->__soapCall('getParticipationQRCode', [
                $participationId,
            ], [], [], $this->outputHeaders));
        
            return $resultGetParticipationQRCode;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getMemberSanctionStatus
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\GetMemberSanctionStatusParameters $parameters
     * @return \Booking\StructType\GetMemberSanctionStatusResult|bool
     */
    public function getMemberSanctionStatus(\Booking\StructType\GetMemberSanctionStatusParameters $parameters)
    {
        try {
            $this->setResult($resultGetMemberSanctionStatus = $this->getSoapClient()->__soapCall('getMemberSanctionStatus', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetMemberSanctionStatus;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getClass
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\CompositeKey $bookingKey
     * @return \Booking\StructType\Booking|bool
     */
    public function getClass(\Booking\StructType\CompositeKey $bookingKey)
    {
        try {
            $this->setResult($resultGetClass = $this->getSoapClient()->__soapCall('getClass', [
                $bookingKey,
            ], [], [], $this->outputHeaders));
        
            return $resultGetClass;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getAvailableSeats
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\AvailableSeatsParameters $parameters
     * @return \Booking\StructType\AvailableSeatsResult|bool
     */
    public function getAvailableSeats(\Booking\StructType\AvailableSeatsParameters $parameters)
    {
        try {
            $this->setResult($resultGetAvailableSeats = $this->getSoapClient()->__soapCall('getAvailableSeats', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAvailableSeats;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getBookingDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\GetBookingDetailsParameters $getBookingDetailsParameters
     * @return \Booking\StructType\BookingDetails|bool
     */
    public function getBookingDetails(\Booking\StructType\GetBookingDetailsParameters $getBookingDetailsParameters)
    {
        try {
            $this->setResult($resultGetBookingDetails = $this->getSoapClient()->__soapCall('getBookingDetails', [
                $getBookingDetailsParameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetBookingDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getBookingRestrictions
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\GetBookingRestrictionsParameters $parameters
     * @return \Booking\StructType\GetBookingRestrictionsResult|bool
     */
    public function getBookingRestrictions(\Booking\StructType\GetBookingRestrictionsParameters $parameters)
    {
        try {
            $this->setResult($resultGetBookingRestrictions = $this->getSoapClient()->__soapCall('getBookingRestrictions', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetBookingRestrictions;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getAvailableAgeGroups
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\GetAvailableAgeGroupsParameters $parameters
     * @return \Booking\StructType\GetAvailableAgeGroupsResult|bool
     */
    public function getAvailableAgeGroups(\Booking\StructType\GetAvailableAgeGroupsParameters $parameters)
    {
        try {
            $this->setResult($resultGetAvailableAgeGroups = $this->getSoapClient()->__soapCall('getAvailableAgeGroups', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAvailableAgeGroups;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getAllAvailableActivities
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\ArrayType\IntArray $centers
     * @return \Booking\ArrayType\AvailableActivityArray|bool
     */
    public function getAllAvailableActivities(\Booking\ArrayType\IntArray $centers)
    {
        try {
            $this->setResult($resultGetAllAvailableActivities = $this->getSoapClient()->__soapCall('getAllAvailableActivities', [
                $centers,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAllAvailableActivities;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getClassParticipations
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\CompositeKey $bookingId
     * @return \Booking\ArrayType\ParticipationArray|bool
     */
    public function getClassParticipations(\Booking\StructType\CompositeKey $bookingId)
    {
        try {
            $this->setResult($resultGetClassParticipations = $this->getSoapClient()->__soapCall('getClassParticipations', [
                $bookingId,
            ], [], [], $this->outputHeaders));
        
            return $resultGetClassParticipations;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getActivityGroups
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\GetActivityGroupsParameter $parameters
     * @return \Booking\ArrayType\ActivityGroupArray|bool
     */
    public function getActivityGroups(\Booking\StructType\GetActivityGroupsParameter $parameters)
    {
        try {
            $this->setResult($resultGetActivityGroups = $this->getSoapClient()->__soapCall('getActivityGroups', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetActivityGroups;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getProductsForBooking
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\GetProductsForBookingParameters $getProductsForBookingParameters
     * @return \Booking\StructType\GetProductsForBookingResult|bool
     */
    public function getProductsForBooking(\Booking\StructType\GetProductsForBookingParameters $getProductsForBookingParameters)
    {
        try {
            $this->setResult($resultGetProductsForBooking = $this->getSoapClient()->__soapCall('getProductsForBooking', [
                $getProductsForBookingParameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetProductsForBooking;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getPersonParticipations
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\ApiPersonKey $personKey
     * @param string $fromDate
     * @param string $toDate
     * @param string $personalBookingType
     * @return \Booking\ArrayType\ParticipationArray|bool
     */
    public function getPersonParticipations(\Booking\StructType\ApiPersonKey $personKey, $fromDate, $toDate, $personalBookingType)
    {
        try {
            $this->setResult($resultGetPersonParticipations = $this->getSoapClient()->__soapCall('getPersonParticipations', [
                $personKey,
                $fromDate,
                $toDate,
                $personalBookingType,
            ], [], [], $this->outputHeaders));
        
            return $resultGetPersonParticipations;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getWeekSchedulePDF
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\WeekScheduleParameter $parameters
     * @return \Booking\StructType\MimeDocument|bool
     */
    public function getWeekSchedulePDF(\Booking\StructType\WeekScheduleParameter $parameters)
    {
        try {
            $this->setResult($resultGetWeekSchedulePDF = $this->getSoapClient()->__soapCall('getWeekSchedulePDF', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetWeekSchedulePDF;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getAvailableActivities
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\ArrayType\IntArray $centers
     * @return \Booking\ArrayType\AvailableActivityArray|bool
     */
    public function getAvailableActivities(\Booking\ArrayType\IntArray $centers)
    {
        try {
            $this->setResult($resultGetAvailableActivities = $this->getSoapClient()->__soapCall('getAvailableActivities', [
                $centers,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAvailableActivities;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Booking\ArrayType\ActivityGroupArray|\Booking\ArrayType\AvailableActivityArray|\Booking\ArrayType\ParticipationArray|\Booking\ArrayType\ResourceArray|\Booking\StructType\AvailableSeatsResult|\Booking\StructType\Booking|\Booking\StructType\BookingDetails|\Booking\StructType\GetAvailableAgeGroupsResult|\Booking\StructType\GetBookingRestrictionsResult|\Booking\StructType\GetMemberSanctionStatusResult|\Booking\StructType\GetProductsForBookingResult|\Booking\StructType\MimeDocument
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
