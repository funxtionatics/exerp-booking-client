<?php

declare(strict_types=1);

namespace Booking\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Showup ServiceType
 * @subpackage Services
 */
class Showup extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named showup
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\CompositeKey $participationId
     * @param string $userInterfaceType
     * @return \Booking\StructType\Participation|bool
     */
    public function showup(\Booking\StructType\CompositeKey $participationId, $userInterfaceType)
    {
        try {
            $this->setResult($resultShowup = $this->getSoapClient()->__soapCall('showup', [
                $participationId,
                $userInterfaceType,
            ], [], [], $this->outputHeaders));
        
            return $resultShowup;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Booking\StructType\Participation
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
