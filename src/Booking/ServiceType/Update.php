<?php

declare(strict_types=1);

namespace Booking\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Update ServiceType
 * @subpackage Services
 */
class Update extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named updateClass
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\UpdateClassBookingParameter $parameters
     * @return \Booking\StructType\Booking|bool
     */
    public function updateClass(\Booking\StructType\UpdateClassBookingParameter $parameters)
    {
        try {
            $this->setResult($resultUpdateClass = $this->getSoapClient()->__soapCall('updateClass', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateClass;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Booking\StructType\Booking
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
