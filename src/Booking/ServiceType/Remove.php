<?php

declare(strict_types=1);

namespace Booking\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Remove ServiceType
 * @subpackage Services
 */
class Remove extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named removeBookingRestriction
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Booking\StructType\RemoveBookingRestrictionParameters $parameters
     * @return \Booking\StructType\RemoveBookingRestrictionResponse|bool
     */
    public function removeBookingRestriction(\Booking\StructType\RemoveBookingRestrictionParameters $parameters)
    {
        try {
            $this->setResult($resultRemoveBookingRestriction = $this->getSoapClient()->__soapCall('removeBookingRestriction', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultRemoveBookingRestriction;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Booking\StructType\RemoveBookingRestrictionResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
